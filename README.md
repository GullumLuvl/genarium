Genarium -- Phylogenetic gene tree amalgamation and reconciliation
==================================================================

# Modules

* `myriphy` -- Processing lists of tree topologies (MCMC samples)
* `grec` -- Gene Tree Reconciliation


# Installation

    git clone https://gitlab.com/GullumLuvl/Genarium.git && cd Genarium

    # Developer mode
    pip3 install --user -e .

    # Compile the Cython file (inplace)
    python3 setup.py build_ext --inplace

    # Run tests (install Pytest)
    python3 -m pytest

# Example

    cd ./examples/

## Summarizing MCMC samples of trees

Build majority rule consensus tree from a sample (discard burnin of 1000):

    MRtree -b 1000 PF00366.chain*.treelist.bz2 > PF00366.MRtree.nwk

Build the tree with highest posterior probability (maximum product of CCP):

    CCP -b 1000 PF00366.chain*.treelist.bz2 > PF00366.besttree.nwk

Map Internode Certainty supports to edges:

    internode_certainty -b 1000 PF00366.besttree.nwk PF00366.chain*.treelist.bz2 > PF00366.besttree.IC.nwk

This last command also works on a target tree that has a subset of the
sequences.

The other executables are:

* `polysplits`: generate all resolutions of a polytomic node based on the tree sample;
* `BPdiff`: compute the differences in bipartition frequencies between two chains;
* `MCMCequaltails`: produce two chains of equal length depending of a given burnin;
* `MCMCdiagnostics.R`: print MCMC convergence metrics (from the trace, not the trees);
* `pb2tracer.sh`: convert Phylobayes trace file to a format digestible by Tracer;
* `summarytree.R`: produce several kinds of consensus tree as implemented in R.

## Reconciling with a species tree

Let's use a species tree of the 196 species from the EukProt database. The topology
is taken from NCBI taxonomy: `NCBItree.EukProt196.nwk`.

To match with sequence labels, we use the same tree labelled with EukProt
species ids (`EP[0-9]+`): `NCBItree.EukProt196.spcode.nwk`. We will indicate to
the program the regex `^(EP[0-9]+)_.*` to capture the species ids from the
sequence labels.

### Fast example (6 sequences in 6 species)

    run_amalgam_rec -r '^(EP[0-9]+)_.*' OG0058322.ufboot NCBItree.EukProt196.spcode.nwk > OG0058322.rec

It outputs all optimal trees, with node annotations:

- `S`: the species
- `E`: the gene divergence event: 'P' for 'paralog' or 'X' for xenolog.
- `C`: the count of events in the subtree: duplications, losses, transfers.

It also outputs the polytomy resolutions ("resolver").

### Slower example (45 sequences in 31 species)

It takes 8 hours 54 min with a processor i7. Run it with `nice` (low priority):

    nice -19 run_amalgam_rec \
        -r '^(EP[0-9]+)_.*' \
        --dlt '3,1,6' -w 3 \
        OG0007546.ufboot NCBItree.EukProt196.spcode.nwk \
        > OG0007546.dlt3-1-6.w3.rec

Parameters:

* `--dlt`: costs of duplication, loss, transfer;
* `-w`:    weight of the amalgam penalty (how much the sequence info is accounted for).

# Disclaimer

The reconciliation algorithm is not published and not reviewed.

It uses an exhaustive listing of polytomy resolutions, then it keeps the best 2000 (local best).

It might not be usable on some input data.

The next steps are to rewrite it in an efficient language and find approximations.

# License

GPLv3
