#!/usr/bin/env python3

import pytest
from functools import partial

from myriphy.CCP import *
logger.setLevel(logging.DEBUG)


CHARS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
CHAR_IDX = {k: i for i,k in enumerate(CHARS)}
formatter = FormatDistrib()
fmt_distrib = partial(formatter.fmt_distrib, intrasep='', conv=CHARS.__getitem__)
fmt_raw_distrib = partial(formatter.fmt_distrib, intrasep='')
prettykeys = partial(formatter.pretty_distrib, intrasep='', conv=CHARS.__getitem__)


def char_setgroup(subsets):
    idx_subsets = []
    for sub in subsets:
        idx_subsets.append([CHAR_IDX[k] for k in sub])
    return bitset_pack(idx_subsets)

def frozenchar_setgroup(subsets):
    idx_subsets = []
    for sub in subsets:
        idx_subsets.append(frozenset(CHAR_IDX[k] for k in sub))
    return frozenset(idx_subsets)

def bitsetchar(chars=()):
    return BitSet(map(CHAR_IDX.__getitem__, chars))


class Test_ClusterPool_bipartitions:
    ClustPool = ClusterPool
    char_setgroup = staticmethod(char_setgroup)

    @pytest.mark.parametrize('tree_nb', [1, 2])
    @pytest.mark.parametrize('orientation', ['unrooted', 'rooted'])
    def test_polytomicroot(self, tree_nb, orientation):
        treelist = ['((a,b),(c,d),(e,f));']
        if tree_nb > 1:
            treelist *= tree_nb

        clusterpool = self.ClustPool(treelist, ids=CHAR_IDX)
        bi, _, distrib_dist = clusterpool.count_subsplits(orientation=='unrooted')
        print('ids =', clusterpool.ids)
        print('rootclust =', clusterpool.rootclust)

        print('root cluster occurences: ', bi[self.char_setgroup(('abcdef', ()))]) # should be zero
        assert set(bi.keys()) == set([self.char_setgroup(('ab', 'cdef')),
                                      self.char_setgroup(('abcd', 'ef')),
                                      self.char_setgroup(('abef', 'cd'))])
        assert all(count==tree_nb for count in bi.values())
        for clust, dists in distrib_dist.items():
            assert len(dists) == tree_nb, 'wrong number of edge lengths at clust %r' % clust

    @pytest.mark.parametrize('tree_nb', [1, 2])
    @pytest.mark.parametrize('orientation', ['unrooted', 'rooted'])
    def test_dichotomicroot(self, tree_nb, orientation):
        treelist = ['((a,b),(c,d));']
        if tree_nb > 1:
            treelist *= tree_nb

        clusterpool = self.ClustPool(treelist, ids=CHAR_IDX)
        bi, _, distrib_dist = clusterpool.count_subsplits(orientation=='unrooted')
        print('root cluster occurences: %d (should be zero)' % bi[self.char_setgroup(('abcd', ()))])
        assert set(bi.keys()) == set([self.char_setgroup(('ab', 'cd'))])
        print('bipartitions counts:', ', '.join('%s:%d' % item for item in bi.items()))
        assert bi[self.char_setgroup(('ab','cd'))] == tree_nb
        for clust, dists in distrib_dist.items():
            assert len(dists) == tree_nb, 'wrong number of edge lengths at clust %r' % clust

    @pytest.mark.parametrize('tree_nb', [1, 2])
    def test_dichotomicroot_leafclade_3leaves(self, tree_nb):
        treelist = ['((a,b),c);']
        if tree_nb > 1:
            treelist *= tree_nb

        bi, _, _ = self.ClustPool(treelist, ids=CHAR_IDX).count_subsplits()
        print('bipartitions counts:', fmt_distrib(bi))
        assert set(bi.keys()) == set() or sum(bi.values()) == 0

    def test_deeper_clades_dichotomic_root(self):
        treelist = ['(((a,b),c),(d,e));']
        bi, _, _ = self.ClustPool(treelist, ids=CHAR_IDX).count_subsplits()
        assert set(bi.keys()) == set([self.char_setgroup(('ab', 'cde')),
                                      self.char_setgroup(('abc', 'de'))])


class Test_FrozenClusterPool_bipartitions(Test_ClusterPool_bipartitions):
    ClustPool = FrozenClusterPool
    char_setgroup = staticmethod(frozenchar_setgroup)


class Test_unrooted_complement_tripartitions:
    root_level = char_setgroup('abc')
    def test_polytomic_root_node_update(self):
        # Tree: (a,b,c); -> this should define tripartitions ab|c a|bc and b|ac
        distrib_sub = Counter({self.root_level: 7})
        print('distrib_sub=%s' % distrib_sub)
        unrooted_complement_tripartitions(distrib_sub, bitsetchar('abc'), keywrap=frozenset)
        print('root_level=%s ; bitsetchar(abc)=%r ; distrib_sub=%s' % (
                self.root_level, bitsetchar('abc'), distrib_sub)) #fmt_distrib(distrib_sub)))
        assert self.root_level not in distrib_sub
        for char_subsplit in ['ab', 'bc', 'ac']:
            subsplit = char_setgroup(char_subsplit)
            assert distrib_sub[subsplit] == 7, repr(subsplit)
        assert len(distrib_sub.keys()) == 3

    def test_dichotomic_root_subsplit_raise_error(self):
        # Tree: (a,b,c); -> this should define tripartitions ab|c a|bc and b|ac
        distrib_sub = Counter({char_setgroup(('ab', 'c')): 7})
        with pytest.raises(RuntimeError):
            unrooted_complement_tripartitions(distrib_sub, bitsetchar('abc'), keywrap=frozenset)

    def test_internal_node_update(self):
        distrib_sub = Counter({char_setgroup('ab'): 7})
        unrooted_complement_tripartitions(distrib_sub, bitsetchar('abc'), keywrap=frozenset)
        for subsplit in [char_setgroup('ab'), char_setgroup('bc'), char_setgroup('ac')]:
            assert distrib_sub[subsplit] == 7
        assert len(distrib_sub.keys()) == 3

    def test_rerooting_invariant(self):
        """From 2 equivalent trees shown with different rootings, find the same tripartitions"""
        root_clust = bitsetchar('abcd')
        distrib_sub = Counter({char_setgroup(('a', 'b')): 2,
                               char_setgroup(('ab', 'c')): 1, # outgroup is d
                               char_setgroup(('ab', 'd')): 1}) # outgroup is c
        unrooted_complement_tripartitions(distrib_sub, root_clust, keywrap=frozenset)
        print('tripart. counts:', distrib_sub)
        print('tripart. counts:', fmt_raw_distrib(distrib_sub))
        print('tripart. counts:', fmt_distrib(distrib_sub))
        pretty_sub = prettykeys(distrib_sub)

        expected_subsplits = set(['a|b', 'ab|c', 'ab|d', 'a|cd', 'b|cd', 'c|d'])

        assert set(pretty_sub) == expected_subsplits
        for subsplit in expected_subsplits:
            assert pretty_sub[subsplit] == 2, 'subsplit %s' % subsplit


class Test_ClusterPool_tripartitions:

    @pytest.mark.parametrize('tree_nb', [1, 2])
    def test_polytomicroot(self, tree_nb):
        treelist = ['((a,b),(c,d),(e,f));']
        if tree_nb > 1:
            treelist *= tree_nb

        bi, tri, _ = ClusterPool(treelist, ids=CHAR_IDX).count_subsplits()

        print('%d tripartitions:' % len(tri), fmt_distrib(tri))
        assert set(prettykeys(tri).keys()) == set(['ab|cd', 'ab|ef', 'cd|ef',
                                                           'a|b', 'c|d', 'e|f',
                                                           'a|cdef', 'b|cdef', 'abef|c', 'abef|d', 'abcd|e', 'abcd|f'])
        assert all(count==tree_nb for count in tri.values())
        #for clust, dists in clusterpool.distrib_dist.items():

    @pytest.mark.parametrize('tree_nb', [1, 2])
    def test_dichotomicroot(self, tree_nb):
        treelist = ['((a,b),(c,d));']
        if tree_nb > 1:
            treelist *= tree_nb

        bi, tri, _ = ClusterPool(treelist, ids=CHAR_IDX).count_subsplits()
        print('tripartitions counts:', fmt_distrib(tri))
        assert set(prettykeys(tri).keys()) == set(['a|b', 'c|d',
                                                           'ab|c', 'ab|d', 'a|cd', 'b|cd'])
        for subsplit, count in prettykeys(tri).items():
            assert count == tree_nb, 'subsplit %s : %d' % (subsplit, count)

    @pytest.mark.parametrize('tree_nb', [1, 2])
    def test_deeper_clades_dichotomic_root(self, tree_nb):
        treelist = ['(((a,b),c),(d,e));']
        if tree_nb > 1:
            treelist *= tree_nb

        bi, tri, _ = ClusterPool(treelist, ids=CHAR_IDX).count_subsplits()
        print('tripartitions counts:', fmt_distrib(tri))
        assert set(prettykeys(tri).keys()) == set(['a|b', 'ab|c', 'd|e', # rooted
                                                           'c|de', 'a|cde', 'b|cde', # inverted
                                                           'abc|d', 'abc|e', 'ab|de'])
        for subsplit, count in prettykeys(tri).items():
            assert count == tree_nb, 'subsplit %s : %d' % (subsplit, count)

    @pytest.mark.parametrize('maketreelist', [1, 2, 'reroot', '+rerooted'])
    @pytest.mark.parametrize('partitions', ['nocomplement', 'complement'])
    def test_caterpillar_dichotomic_root(self, maketreelist, partitions):
        treelist = ['(((a,b),c),d);']
        if maketreelist == 'reroot':
            treelist = ['(((a,b),d),c);']
        elif maketreelist == '+rerooted':
            # this reproduces the bug where tripartitions do not sum up (omit one 'ab|c' and 'ab|d')
            treelist.append('(((a,b),d),c);')
        elif maketreelist > 1:
            treelist *= maketreelist

        clusterpool = ClusterPool(treelist, ids=CHAR_IDX)
        bi, tri, distrib_dist = clusterpool.count_subsplits((partitions=='complement'))
        print('tripartitions counts:', fmt_distrib(tri))

        expected_dists = set((BitSet((k,)) for k in range(4))).union((BitSet((0,1)),))
        if partitions == 'nocomplement':
            if maketreelist == 'reroot':
                expected = set(['a|b', 'ab|d'])
                expected_dists.add(BitSet((0,1,3)))
            elif maketreelist == '+rerooted':
                expected = set(['a|b', 'ab|c', 'ab|d'])
                expected_dists |= set((BitSet((0,1,2)), BitSet((0,1,3))))
            else:
                expected = set(['a|b', 'ab|c'])
                expected_dists.add(BitSet((0,1,2)))
            assert set(prettykeys(tri).keys()) == expected
            assert set(distrib_dist) == expected_dists
        else:
            expected_dists |= set((BitSet((1,2,3)), BitSet((0,2,3)), BitSet((0,1,3)), BitSet((0,1,2)), BitSet((2,3))))
            assert set(prettykeys(tri).keys()) == set(['a|b', 'ab|c', # rooted
                                                               'a|cd', 'ab|d', 'b|cd',  # inverted
                                                               'c|d'])
            assert set(distrib_dist) == expected_dists
            for subsplit, count in prettykeys(tri).items():
                assert count == len(treelist), 'subsplit %s : %d' % (subsplit, count)
        if partitions == 'complement':
            for clust, dists in distrib_dist.items():
                split = sortuple((clust, BitSet(range(4)).difference(clust)))
                print('split %s count: %d' % (split, bi[split]))
                assert len(dists) == len(treelist), '%d edge lengths in clust %r' % (len(dists), clust)

    @pytest.mark.parametrize('reverse', [False, True])
    def test_deeper_clades_dichotomic_root_alt_topo(self, reverse):
        treelist = ['(((a,b),c),(d,e));', '((a,(b,c)),(d,e));']
        if reverse:
            treelist.reverse()

        bi, tri, distrib_dist = ClusterPool(treelist, ids=CHAR_IDX).count_subsplits()
        pretty_tri = prettykeys(tri)
        print('tripartitions counts:', fmt_distrib(tri))
        assert set(pretty_tri.keys()) == set(['a|b', 'ab|c', 'd|e', # rooted
                                              'a|bc', 'b|c',
                                              'c|de', 'a|cde', 'b|cde', # inverted
                                              'abc|d', 'abc|e', 'ab|de',
                                              'bc|de', 'a|de', 'ade|c', 'ade|b'])
        for subsplit in ['d|e', 'abc|d', 'abc|e']:
            count = pretty_tri[subsplit]
            assert count == 2, 'subsplit %s : %d' % (subsplit, count)

        for subsplit in ['a|b', 'b|c', 'ab|c', 'c|de', 'a|cde', 'b|cde', 'ab|de', 'a|bc', 'bc|de', 'a|de', 'ade|c', 'ade|b']:
            count = pretty_tri[subsplit]
            assert count == 1, 'subsplit %s : %d' % (subsplit, count)

        prettydist = {''.join(CHARS[i] for i in k): v for k,v in distrib_dist.items()}
        for clust, dists in prettydist.items():
            if clust in ('ab', 'bc', 'cde', 'ade'):
                assert len(dists) == 1, '%d != 1 edge lengths in clust %r' % (len(dists), clust)
            else:
                assert len(dists) == len(treelist), '%d edge lengths in clust %r' % (len(dists), clust)


NONINT_IDS = {k:k for k in CHARS}


class Test_Amalgam:

    @pytest.mark.parametrize('reverse', [False, True])
    @pytest.mark.parametrize('ids', [None, CHAR_IDX, NONINT_IDS])
    @pytest.mark.parametrize('keybuild', ['bitset_pack', 'frozen_freeze'])
    def test_verify_freqs(self, keybuild, ids, reverse):
        treelist = ['(((a,b),c),(d,e));', '((a,(b,c)),(d,e));']
        if reverse:
            treelist.reverse()
        if keybuild == 'bitset_pack':
            ClustPool = ClusterPool
            Amalg = Amalgam
            if ids is NONINT_IDS:
                with pytest.raises(TypeError):
                    clusterpool = ClustPool(treelist, ids=ids)
                return
        else:
            ClustPool = FrozenClusterPool
            Amalg = FrozenAmalgam


        clusterpool = ClustPool(treelist, ids=ids)
        bi, tri, distrib_dist = clusterpool.count_subsplits(trivial_splits=True)
        print('bi:', fmt_raw_distrib(bi))
        print('tri:', fmt_raw_distrib(tri))
        if ids is None:
            print(clust for split, count in bi.items() for clust in split if count>0)
            expected_full = Amalg.makeset().union(*(clust for split in bi for clust in split))
        else:
            expected_full = sorted(ids.values())
        print('full:', ','.join(map(str, expected_full)))
        amalgam = Amalg(bi, tri, clusterpool.ids, clusterpool.n)
        print('amalgam.clusts.keys: %s', ' '.join(map(repr, amalgam.clusts.keys())))
        amalgam.verify_freqs()

    def test_increasing_clusters(self):
        treelist = ['(((a,b),c),(d,e));']  # , '((a,(b,c)),(d,e));'

        clusterpool = ClusterPool(treelist, ids=CHAR_IDX)
        bi, tri, _ = clusterpool.count_subsplits(trivial_splits=True)
        print('bi:', fmt_raw_distrib(bi))
        print('tri:', fmt_raw_distrib(tri))

        amalgam = Amalgam(bi, tri, clusterpool.ids, clusterpool.n)

        increasing_clusters = list(map(amalgam.translate, amalgam.increasing_clusters))
        print(increasing_clusters)

        assert set(increasing_clusters[:2]) == set([ ('a','b'), ('d','e') ])
        assert len(increasing_clusters[2]) == 3
        assert set(increasing_clusters[2:4]) == set([ ('a','b','c'), ('c','d','e') ])
        assert len(increasing_clusters[4]) == 4
        assert set(increasing_clusters[4:]) == set([ ('a','b','c','d'), ('a','b','c','e'),
                                                     ('a','b','d','e'), ('a','c','d','e'),
                                                     ('b','c','d','e') ])

#TODO
@pytest.mark.skip(reason="todo later")
class Test_Amalgam_walk_from:
    def test_get_parents_of_single_start(self):
        pass
    def test_get_parents_of_multiple_starts(self):
        pass
    def test_get_skip_second_start_if_parent_of_first(self):
        pass

#TODO
@pytest.mark.skip(reason="todo later")
class Test_combine_sorted_proba_subtrees:
    pass

#TODO
@pytest.mark.skip(reason="todo later")
class Test_MRE_from_splits:
    def test_last_clust_is_parent_to_all(self):
        #distrib = 
        pass
    def test_multiple_basal_nodes(self):
        pass
