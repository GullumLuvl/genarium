#!/usr/bin/env python3


from functools import partial
from ete3 import Tree
from collections import namedtuple, Counter
from math import nan as NaN
from myriphy.compactset import bitset_pack, BitSet
from myriphy.CCP import Amalgam, FrozenAmalgam, unrooted_complement_tripartitions, include_trivial_splits, include_trivial_subsplits, FormatDistrib, build_amalgam
from grec.amalgrec import *
logger.setLevel(logging.DEBUG)

import pytest


fmt_distrib = FormatDistrib.fmt_distrib


class ScoresNamer:
    def __init__(self, species_tree):
        self.names = []
        for node in species_tree.traverse('postorder'):
            self.names.append(node.name)
        self.namer = namedtuple('NamedScores', self.names)
    def __call__(self, scores):
        return self.namer(*scores)

class Test_multi_min:
    def test_all_finite_one_best(self):
        values = [20,10,30]
        bestkey, minima = multi_min(values)
        assert bestkey==10
        assert minima == [10]
    def test_all_finite_two_best(self):
        values = [20,10,10]
        bestkey, minima = multi_min(values)
        assert bestkey==10
        assert minima == [10,10]
    def test_ignore_first_nan(self):
        values = [NaN,10,30]
        bestkey, minima = multi_min(values)
        assert bestkey==10
        assert minima == [10]
    def test_ignore_last_nan(self):
        values = [20,10,NaN]
        bestkey, minima = multi_min(values)
        assert bestkey==10
        assert minima == [10]


class Test_Init:
    def setup_method(self):
        self.tree = Tree('((A,B)X,C)R;', format=1)
        amalgam = Amalgam({}, {})
        self.combine = AmalgDLTCombiner(self.tree, amalgam, None, 'ABXCR') 

    def test_mrca(self):
        A, B, X, C, R = 0, 1, 2, 3, 4
        mrca = self.combine.pairwise_mrca
        label_mrca = {('ABXCR'[i], 'ABXCR'[j]): 'ABXCR'[k] for (i,j),k in mrca.items()}

        assert set(label_mrca) == set(product('ABXCR', repeat=2))
        assert set(mrca) == set(product(range(5), repeat=2))
        assert mrca[A, A] == A
        assert mrca[A, B] == X
        assert mrca[A, C] == R
        assert mrca[A, X] == X
        assert mrca[A, R] == R
        assert mrca[B, A] == X
        assert mrca[B, B] == B
        assert mrca[B, C] == R
        assert mrca[B, X] == X
        assert mrca[B, R] == R
        assert mrca[C, A] == R
        assert mrca[C, B] == R
        assert mrca[C, C] == C
        assert mrca[C, X] == R
        assert mrca[C, R] == R


# data cases
species_tree = Tree('((A,B)X,(C,D,E)Y)R;', format=1)

scorename = ScoresNamer(species_tree)


combine_cases_dicho = ('dicho_spe_only',
                       'dicho_spe_indirectchild',  # an intermediate taxon is missing: one loss
                       'dicho_dup_only',
                       'dicho_dup_oneloss',
                       'dicho_transfer',
                       'dicho_transfer_indirectchild', # the vertical child is indirect: one loss
                       'dicho_dup_parallel_loss', # both paralogs loose the same taxon
                       'dicho_dup_reciprocal_loss' # each paralog loose a complementary taxon. same result as ILS
                      )
combine_cases_poly = ('poly_spe_complete',
                      'poly_spe_lostonechild',  # a child of the polytomy is missing
                      'poly_spe_indirectchild', # a child node is lacking intermediate speciation
                      'poly_dup_complete',
                      'poly_dup_onelossafter',
                      'poly_dup_onelossbefore',
                      'poly_transfer')

# D,L,T,F = 1, 0.5, 3, 1
#expected_scores = dict(
#        only_spe_dicho=
#        )


class Test_direct_event_count:

    def setup_method(self):
        r = bitset_pack(((0,1), (2,3)))
        tu = bitset_pack(((0,), (1,)))
        amalgam = Amalgam({r: 1},
                          {tu: 1},
                          ids=dict(t=0,u=1,v=2,w=3),
                          n=1)

        get_sp = dict(t='A', u='B', v='C', w='D').__getitem__  # random choice
        self.species_tree = species_tree.copy()
        self.combine = AmalgDLTCombiner(self.species_tree, amalgam, get_sp, 'ABXCDEYR')

        A,B,X,C,D,E,Y,R = 0, 1, 2, 3, 4, 5, 6, 7
        self.input_outputs = dict(
            dicho_spe_only=         [ (X, (A,B),    (None,None)), ([0,0,0], []) ],
            dicho_spe_indirectchild=[ (R, (A,Y),    (None,None)), ([0,1,0], []) ],
            dicho_dup_only=         [ (A, (A, A),   (None,None)), ([1,0,0], []) ],
            dicho_dup_oneloss=      [ (X, (X, A),   (None,None)), ([1,1,0], []) ],
            dicho_transfer=         [ (A, (A,None), (None,   C)), ([0,0,1], []) ],
            dicho_transfer_indirectchild=[(X, (A,None),(None,C)), ([0,1,1], []) ],
            dicho_dup_parallel_loss=[ (X, (A,A),  (None,None)),   (NaN, []) ],  # always suboptimal: [1,2,0]
            dicho_dup_reciprocal_loss=[(R, (A,B),  (None,None)),  (NaN, []) ]  # Note that it cannot be observed at X, (A,B). Also suboptimal: [1,4,0]
            )

    @pytest.mark.parametrize('case', combine_cases_dicho)
    def test(self, case):
        inputs, output = self.input_outputs[case]
        cost, direct_losses = self.combine.direct_event_count(*inputs)
        if np.isnan(output[0]).all():
            assert np.isnan(cost).all()
        else:
            assert cost.tolist() == output[0]
        assert direct_losses == output[1]


class Test_direct_event_count_poly:

    def setup_method(self):
        y0, y1 = BitSet((3,)), BitSet((4,5))
        y = bitset_pack((y0, y1))
        z = bitset_pack(((4,), (5,)))
        amalgam = Amalgam({y: 1},
                          {z: 1},
                          ids=dict(c=3,d=4,e=5),
                          n=1)

        dy0, dy1 = BitSet((6,)), BitSet((7,8))
        dy = bitset_pack((dy0, dy1))
        dz = bitset_pack(((7,), (8,)))
        noty1, notdy1 = BitSet((3,6,7,8)), BitSet((3,4,5,6))
        yy = bitset_pack(((3,4,5), (6,7,8)))
        amalgamdup = Amalgam({yy: 1, (noty1, y1): 1, (notdy1, dy1): 1},
                          {y: 1, dy: 1, z: 1, dz:1},
                          ids=dict(c=3,d=4,e=5,c2=6,d2=7,e2=8),
                          n=1)

        ty0, ty1 = BitSet((0,3)), BitSet((4,5))
        ty = bitset_pack((ty0, ty1))
        tz = bitset_pack(((0,), (3,)))
        amalgamtransfer = Amalgam({ty: 1},
                          {z: 1, tz: 1},
                          ids=dict(a=0,c=3,d=4,e=5),
                          n=1)

        get_sp = dict(a='A', c='C', d='D', e='E', c2='C', d2='D', e2='E').__getitem__
        self.species_tree = species_tree.copy()
        self.combine = AmalgDLTCombiner(self.species_tree, amalgam, get_sp, 'ABXCDEYR')
        self.combinedup = AmalgDLTCombiner(self.species_tree, amalgamdup, get_sp, 'ABXCDEYR')
        self.combinetransfer = AmalgDLTCombiner(self.species_tree, amalgamtransfer, get_sp, 'ABXCDEYR')

        A,B,X,C,D,E,Y,R = 0, 1, 2, 3, 4, 5, 6, 7
        cC, cD, cE = (3,), (4,), (5,)
        cCD, cCE, cDE = ((3,), (4,)), ((3,), (5,)), ((4,), (5,))
        #ctransfers = [(cC,None), (None,cC), (cD,None), (None,cD), (cE,None), (None,cE)]

        #nans = np.full(4, np.NaN)
        #zero = np.zeros(4)
        #amalgam.features[y0] = dict(
        #        partial={Y: {cC: 0, cD: NaN, cE: NaN}},
        #        partial_count={Y: {cC: zero, cD: nans, cE: nans}}
        #        )
        #amalgam.features[y1] = dict(
        #        partial={Y: {cDE: 0, cCD: NaN, cCE: NaN, **{ct: NaN for ct in ctransfers}}},
        #        partial_count={Y: {cDE: zero, cCE: nans, cCD: nans, **{ct: nans for ct in ctransfers}}}
        #        )

        # NOTE: the input 'subsplit' in these tests are tuples, whereas in the real situation they would be frozenset

        self.input_outputs = dict(
            poly_spe_complete=     [ (Y, (cC,cDE), (None,None)),            ([0,0,0], []) ],
            poly_spe_lostonechild= [ (Y, (cC,cD),  (None,None)),            ([0,0,0], [E]) ],
            #poly_spe_indirectchild=[ (Y, (cC, cF),   ()),          ([0,1,0], []) ],
            poly_dup_complete=     [ (Y, ((cC,cDE),(cC,cDE)), (None,None)), ([1,0,0], []) ],
            poly_dup_onelossafter= [ (Y, ((cC,cD), (cC,cDE)), (None,None)), ([1,1,0], []) ],
            poly_dup_onelossbefore=[ (X, ((cC,cD), (cC,cD)),  (None,None)), ([1,0,0], [E]) ],
            poly_transfer=         [ (Y, ((cC,cDE), None),    (A,None)),    ([0,0,1], []) ],
            poly_nested_transfer=  [ (Y, ((cC,None), cDE),    ((None,A),None)), ([0,0,0], []) ]  # transfer not happening at this level, but deeper.
            )

    @pytest.mark.parametrize('case', ['poly_spe_complete', 'poly_spe_lostonechild'])
    def test_spe(self, case):
        inputs, output = self.input_outputs[case]

        cost, direct_losses = self.combine.direct_event_count(*inputs)
        if np.isnan(output[0]).all():
            assert np.isnan(cost).all()
        else:
            assert cost.tolist() == output[0]
        assert direct_losses == output[1]

    @pytest.mark.parametrize('case', ['poly_dup_complete', 'poly_dup_onelossafter', 'poly_dup_oneloss_before'])
    def test_dup(self, case):
        try:
            inputs, output = self.input_outputs[case]
        except KeyError:
            return

        cost, direct_losses = self.combinedup.direct_event_count(*inputs)
        if np.isnan(output[0]).all():
            assert np.isnan(cost).all()
        else:
            assert cost.tolist() == output[0]
        assert direct_losses == output[1]

    @pytest.mark.parametrize('case', ['poly_transfer', 'poly_nested_transfer'])
    def test_transfer(self, case):
        try:
            inputs, output = self.input_outputs[case]
        except KeyError:
            return

        cost, direct_losses = self.combinetransfer.direct_event_count(*inputs)
        if np.isnan(output[0]).all():
            assert np.isnan(cost).all()
        else:
            assert cost.tolist() == output[0]
        assert direct_losses == output[1]


class Test_iter_configs:
    def setup_method(self):
        r = bitset_pack(((0,1), (2,3)))
        tu = bitset_pack(((0,), (1,)))
        vw = bitset_pack(((2,), (3,)))
        amalgam = Amalgam(bi={r: 1},
                          tri={tu: 1, vw: 1},
                          ids=dict(t=0,u=1,v=2,w=3),
                          n=1)
        self.splits = r, tu, vw
        self.amalgam = amalgam

        get_sp = dict(t='A', u='B', v='C', w='D').__getitem__  # random choice
        self.species_tree = species_tree.copy()
        self.combine = AmalgDLTCombiner(self.species_tree, amalgam, get_sp, 'ABXCDEYR')

    @pytest.mark.parametrize('terminal,expected_species', [(0, 'A') ,(1, 'B'), (2, 'C') ,(3, 'D')])
    def test_init_terminal(self, terminal, expected_species):
        """len(clust)==1 so the combine function should recognize a leaf and initialize it."""
        combine = self.combine
        features = self.amalgam.features

        clust = BitSet((terminal,))
        features[clust] = estimated = combine([], clust)
        assert estimated['species'] == expected_species and estimated['species_id'] == combine.anc_index[expected_species]
        score = estimated['score']
        species_id = estimated['species_id']
        partial = estimated['partial']
        print('features =', estimated)
        print('score =', score)
        assert score.shape == (8,)
        assert score[species_id] == 0  # log(CCP) = log(1) = 0

        anc_ids = list(range(8))
        assert all(np.isnan(score[anc_ids[:species_id]+anc_ids[species_id+1:]]))

        Y_id = combine.anc_index['Y']
        assert partial.keys() == set((Y_id,))
        # When initializing the child of a polytomy, only the direct children should be given as config
        #if combine.ancestors[species_id] in ('C', 'D'):
        #    assert set(partial[Y_id].keys()) == set([ ((species_id,), None) ])
        #else:
        assert not partial[Y_id]

    def test_cherry_X(self):
        combine = self.combine
        features = self.amalgam.features
        r, tu, _ = self.splits
        t,u = sorted(tu, key=tuple)
        r0 = min(r, key=tuple)

        A, B, X, C, D, E, Y, R = 0,1,2,3,4,5,6,7

        for terminal in range(4):
            clust = BitSet((terminal,))
            features[clust] = combine([], clust)  # len(clust)==1 so the combine function recognizes a leaf and initializes it.

        print('score(t) = %s' % features[t]['score'])
        print('score(u) = %s' % features[u]['score'])
        configs, transfers, scores, counts = list(zip(*combine.iter_configs(X, (t,u), r0)))
        print('configs:', configs)
        print('transfers:', transfers)
        assert set(configs) == set([ (A,B),  # the only Not NaN score
                                    # (B,A),
                                    # #(A,A), (B,B), # always suboptimal
                                    # (X,A), (A,X),
                                    # (X,B), (B,X), (X,X), (X,None), (None,X),
                                    # (A,None), (None,B), #(None,A), (B,None) #TODO: skip this
                                   ])
        assert (None, B) not in transfers # would not be interpreted as transfer, thus redundant
        assert (A, None) not in transfers # would not be interpreted as transfer, thus redundant
        assert configs.count((A,B)) == 1  # No duplicates
        #assert configs.count((X,None)) == 4  # transfered to either C,D,E,Y

    def test_cherry_A(self):
        combine = self.combine
        features = self.amalgam.features
        r, tu, _ = self.splits
        t,u = sorted(tu, key=tuple)
        r0 = min(r, key=tuple)

        A, B, X, C, D, E, Y, R = 0,1,2,3,4,5,6,7

        for terminal in range(4):
            clust = BitSet((terminal,))
            features[clust] = combine([], clust)

        print('score(t) = %s' % features[t]['score'])
        print('score(u) = %s' % features[u]['score'])
        configs, transfers, scores, counts = list(zip(*combine.iter_configs(A, (t,u), r0)))
        print('configs:', configs)
        print('transfers:', transfers)
        assert set(configs) == set([ (A,None) ]) # the only Not NaN score
        assert set(transfers) == set([(None, B)]) # would not be interpreted as transfer, thus redundant
        assert (A, None) not in transfers # would not be interpreted as transfer, thus redundant
        assert configs.count((A,None)) == 1  # Only transfer to B is considered

    def test_combination_of_all_descendants_of_R(self):
        combine = self.combine
        features = self.amalgam.features
        r, tu, _ = self.splits
        r0 = min(r, key=tuple)

        A, B, X, C, D, E, Y, R = 0,1,2,3,4,5,6,7

        for terminal in range(4):
            clust = BitSet((terminal,))
            features[clust] = combine([], clust)

        configs, transfers, scores, counts = list(zip(*combine.iter_configs(R, sorted(tu, key=tuple), r0)))
        print(configs)
        #FIXME: it would be better that iter_configs does not yield NaN configurations
        #NOTE that no transfers are permitted because R is the root.
        assert set(configs) == set([ (A,B) ]) # the only Not NaN score
                                    # (B,A), #(A,A), (B,B), (X,X)
                                    # (X,A), (A,X),
                                    # (X,B), (B,X),
                                    # (C,D), (D,C), (C,E), (E,C),
                                    # #(C,C), (D,D), (E,E), (Y,Y),
                                    # (D,E), (E,D), (Y,C), (C,Y),
                                    # (Y,D), (D,Y), (Y,E), (E,Y),
                                    # (R,R), (R,X), (X,R), (R,Y), (Y,R),
                                    # (R,A), (A,R), (R,B), (B,R), (R,C), (C,R),
                                    # (R,D), (D,R), (R,E), (E,R),
                                    # (X,Y), (Y,X),
                                    # (X,C), (C,X), (X,D), (D,X), (X,E), (E,X),
                                    # (Y,A), (A,Y), (Y,B), (B,Y),
                                    # (A,C), (C,A), (A,D), (D,A), (A,E), (E,A),
                                    # (B,C), (C,B), (B,D), (D,B), (B,E), (E,B)])

        assert len(configs) == len(set(configs))  # No duplicates

    def test_polytomy_Y(self):
        combine = self.combine
        features = self.amalgam.features
        r, tu, vw = self.splits
        r1 = max(r, key=tuple)

        cA, cB, cX, cC, cD, cE, cY, cR = (0,), (1,), (2,), (3,), (4,), (5,), (6,), (7,)
        Y = combine.anc_index['Y']

        for terminal in range(4):
            clust = BitSet((terminal,))
            features[clust] = combine([], clust)

        print('descendants[C]: %r' % combine.descendants[3])
        verticals, horizontals = [], []
        for vertical, transfered, _, _ in combine.iter_configs(Y, sorted(vw, key=tuple), r1):
            verticals.append(vertical)
            horizontals.append(transfered)
        print('verticals:', verticals)
        print('horizontals:', horizontals)
        # Actually, because v and w are leaves, we are limited to the only combinations involving C and D
        only_vertical = [ (cC, cD), # the only Not NaN score
                         (cD, cC), (cC, cE), (cE, cC), (cD, cE), (cE, cD),
                         (cC, cC), (cD, cD), (cE, cE), (cY, cY),  # These must be emitted for polytomy
                         (cY, cC), (cC,cY), (cY, cD), (cD, cY), (cY, cE), (cE, cY)]
        expected = set(only_vertical + [(cY,None), (None,cY)])
        # redundant: (cC,None), (None,cC), (cD,None), (None,cD), (cE,None), (None,cE) ])
        
        #assert expected <= set(verticals) # To simplify interpreting the next test, now we know it's included
        assert set(verticals) <= expected
        for vertic in only_vertical:
            assert verticals.count(vertic) <= 1, "More than one occurence of %s" % (vertic,)

    def test_deep_poly(self):
        node_E = self.species_tree&'E'
        node_E.add_child(name='F')
        node_E.add_child(name='G')
        get_sp = dict(t='F', u='G', v='C', w='D').__getitem__
        combine = AmalgDLTCombiner(self.species_tree, self.amalgam, get_sp, 'RXYABCDEFG')

        r, tu, vw = self.splits
        t,u = sorted(tu, key=tuple)
        v,w = sorted(vw, key=tuple)
        r0, r1 = sorted(r, key=tuple)

        cY, cC, cD, cE, cF, cG = (2,), (5,), (6,), (7,), (8,), (9,)
        Y = combine.anc_index['Y']

        for terminal in range(4):
            clust = BitSet((terminal,))
            self.amalgam.features[clust] = combine([], clust)

        print('partial[v] keys:', self.amalgam.features[v]['partial'][Y].keys())
        print('partial[v] scores:', self.amalgam.features[v]['partial'][Y].values())

        verticals, horizontals = [], []
        for vertical, transfered, _, _ in combine.iter_configs(Y, (v,w), r1):
            verticals.append(vertical)
            horizontals.append(transfered)
        print('verticals:', verticals)
        print('horizontals:', horizontals)
        only_vertical = [ (cC, cD) ]
        expected = set(only_vertical) # + [(cC,None), (None,cD)])  # transfers should not even be considered here
        assert set(verticals) == expected
        for vertic in only_vertical:
            assert verticals.count(vertic) <= 1, "More than one occurence of %s" % (vertic,)

        print('partial[t] keys:', self.amalgam.features[t]['partial'][Y].keys())
        print('partial[t] scores:', self.amalgam.features[t]['partial'][Y].values())

        verticals, horizontals = [], []
        for vertical, transfered, _, _ in combine.iter_configs(Y, (t,u), r0):
            verticals.append(vertical)
            horizontals.append(transfered)
        print('verticals:', verticals)
        print('horizontals:', horizontals)
        #only_vertical = [ (cE, cE) ]
        #expected = set(only_vertical) # + [(cE,None), (None,cE)])
        assert set(verticals) == set()
        for vertic in only_vertical:
            assert verticals.count(vertic) <= 1, "More than one occurence of %s" % (vertic,)

        print('amalgam.features.keys:', self.amalgam.features.keys())
        #TODO: verify that the following partial keys would actually be stored by combine()
        self.amalgam.features[r0]['score'] = np.full(10, NaN)
        self.amalgam.features[r0]['score'][[7,8,9]] = [0, 3, 3]  # E: speciation; F/G: transfer
        self.amalgam.features[r1]['score'] = np.full(10, NaN)
        self.amalgam.features[r1]['score'][[2,5,6]] = [0.5, 3, 3]  # Y: 1 loss; C/D: transfer
        self.amalgam.features[r0]['count'] = np.full(10, NaN)
        self.amalgam.features[r1]['count'] = np.full(10, NaN)
        self.amalgam.features[r0]['partial'] = {Y: {#((cE, cE), (None, None)): 0,
                                                   #((cE, None),  (None, None)): 0, # (None,None) because ignoring transfers
                                                   #((None, cE),  (None, None)): 0 #, (cE, None): 0
                                                   }}
        self.amalgam.features[r1]['partial'] = {Y: {((cC, cD), (None, None)): 0,
                                                   #((cC, None), (None, cD)): 0,
                                                   #((None, cD), (cC, None)): 0 #, (cC, None): 0, (cD, None): 0
                                                   }}
        self.amalgam.features[r0]['partial_count'] = {Y: {}}
        self.amalgam.features[r1]['partial_count'] = {Y: {((cC, cD), (None, None)): 0}}
        verticals, horizontals = [], []
        for vertical, transfered, _, _ in combine.iter_configs(Y, r, self.amalgam.full):
            verticals.append(vertical)
            horizontals.append(transfered)
        print('verticals:', verticals)
        print('horizontals:', horizontals)
        only_vertical = [ (cE,      (cC, cD)),  (cE,     cC), (cE,      cD)#,
                        # ((cE,cE), (cC, cD)), ((cE,cE), cC), ((cE,cE), cD) # this line should be unnecessary
                        ]
        expected = set(only_vertical)# + [
                              #( None    ,(cC,cD)) #, (None     ,(cC,None)), (None     ,(None,cD)), (None     ,cC), (None     ,cD),
            #(cE       ,None),                      (cE       ,(cC,None)), (cE       ,(None,cD)),
            #((cE,None),None), ((cE,None),(cC,cD)), ((cE,None),(cC,None)), ((cE,None),(None,cD)), ((cE,None),cC), ((cE,None),cD),
            #((None,cE),None), ((None,cE),(cC,cD)), ((None,cE),(cC,None)), ((None,cE),(None,cD)), ((None,cE),cC), ((None,cE),cD),
            #((cE,cE)  ,None),                      ((cE,cE)  ,(cC,None)), ((cE,cE)  ,(None,cD))  # Unncessary
           #])
        assert set(verticals) == expected
        for vertic in only_vertical:
            assert verticals.count(vertic) <= 1, "More than one occurence of %s" % (vertic,)

Dw, Lw, Tw, Fw = 1, 0.5, 3, 1  # Weights of the parsimony scoring


class TestBase_combine:
    """Initialize the combiner class for all tests"""
    def setup_method(self):
        r = bitset_pack(((0,1), (2,3)))
        tu = bitset_pack(((0,), (1,)))
        vw = bitset_pack(((2,), (3,)))
        amalgam = Amalgam(bi={r: 1},
                          tri={tu: 1, vw: 1},
                          ids=dict(t=0,u=1,v=2,w=3),
                          n=1)
        self.splits = r, tu, vw
        self.amalgam = amalgam

        get_sp = dict(t='A', u='B', v='C', w='D').__getitem__  # random choice
        self.species_tree = species_tree.copy()
        combine = AmalgDLTCombiner(self.species_tree, amalgam, get_sp, 'ABXCDEYR',
                                   dup=Dw, loss=Lw, transfer=Tw, freq=Fw)
        r, tu, vw = self.splits
        for terminal in tu | vw:
            amalgam.features[terminal] = combine([], terminal)
        self.combine = combine

        t,u = sorted(tu, key=tuple)
        print('features keys:', amalgam.features.keys())
        print('score of t: %s' % amalgam.features[t]['score'])
        print('score of u: %s' % amalgam.features[u]['score'])

    nancount = np.full(4, NaN)
    # Which ancestor mapping in 'tu' triggers transfers:
    tu_event_groups = dict(double_transfer='CDEY',
                           single_transfer='AB',
                           vertical='XR')
    tu_anc_expected_scores = [('X', 0,   np.zeros(4)),
                              ('R', NaN, nancount), # Always suboptimal: Dw + 4*Lw, np.array([1,4,0,0])), # weird situation: anc is older than MRCA of unduplicated descendants, so it requires a dup/loss (looks like ILS also).
                              ('A', Tw,  np.array([0,0,1,0])),
                              ('B', Tw,  np.array([0,0,1,0])),
                              ('C', NaN, nancount),
                              ('D', NaN, nancount),
                              ('E', NaN, nancount),
                              ('Y', NaN, nancount)]

    vw_anc_expected_scores = [('Y', Lw,  np.array([0,1,0,0])),  # one polychild is lost
                              ('R', NaN, nancount),
                              ('C', Tw,  np.array([0,0,1,0])),
                              ('D', Tw,  np.array([0,0,1,0])),
                              ('E', NaN, nancount),
                              ('A', NaN, nancount),
                              ('B', NaN, nancount),
                              ('X', NaN, nancount)]

    r_anc_expected_scores = [('R', Lw,    np.array([0,1,0,0])),  # one polychild is lost
                             ('X', Tw+0.5*Lw, np.array([0,.5,1,0])),  # polychild loss half-scored because transfer to partial Y
                             ('Y', Lw+Tw, np.array([0,1,1,0])),
                             ('A', 2*Tw+0.5*Lw,  np.array([0,.5,2,0])),
                             ('B', 2*Tw+0.5*Lw,  np.array([0,.5,2,0])),
                             ('C', 2*Tw,  np.array([0,0,2,0])),
                             ('D', 2*Tw,  np.array([0,0,2,0])),
                             ('E', NaN,   nancount)]

#
# start with equal weights for each partition (equivalent to tree reconciliation)
class Test_combine_ancsplit_score(TestBase_combine):
    @pytest.mark.parametrize('anc,expected_score,expected_count', TestBase_combine.tu_anc_expected_scores)
    def test_ancsplit_scores_best_config_tu(self, anc, expected_score, expected_count):
        r, tu, _ = self.splits
        r0 = min(r, key=tuple)
        anc_id = self.combine.anc_index[anc]
        min_score, best_count, best_config, *_ = self.combine.ancsplit_score(anc_id, tu, r0)

        # Dirty test fix: ancsplit_score input 'tu' has a random ordering. If it's (u,t), we must reorder the configs.
        t, u = sorted(tu, key=tuple)
        if tuple(tu) != (t,u):
            best_config = [(tuple(reversed(vertic)), tuple(reversed(horiz))) for vertic,horiz in best_config]

        A, B, X, C, D, E, Y, R = 0, 1, 2, 3, 4, 5, 6, 7
        print('min_score', min_score, 'best_config', best_config)
        if anc_id in (R,Y,C,D,E):
            assert not best_config
        else:
            assert len(best_config) == 1
            if anc_id in (R,X):
                assert best_config[0] == ((A,B), (None,None)), "The only mapping of t,u is A,B"
            elif anc_id == A:
                assert best_config[0] == ((A, None), (None,B))
            else:
                assert best_config[0] == ((None, B), (A,None))
        if np.isnan(expected_score):
            assert np.isnan(min_score)
            assert np.isnan(best_count).all()
        else:
            assert min_score == expected_score
            assert best_count.tolist() == expected_count.tolist()
            assert min_score == (best_count[:3] * self.combine.weights).sum() + best_count[3]*self.combine.freq

    @pytest.mark.parametrize('anc,expected_score,expected_count', TestBase_combine.vw_anc_expected_scores)
    def test_ancsplit_scores_best_config_vw(self, anc, expected_score, expected_count):
        r, _, vw = self.splits
        r1 = max(r, key=tuple)
        anc_id = self.combine.anc_index[anc]
        features_v = self.amalgam.features[min(vw, key=tuple)]
        print('count(v)[C] = %s ; partial_count(v)[((C,),None)] = %s' % (
                features_v['count'][3],features_v['partial_count'].get(((3,), None))))
        min_score, best_count, best_config, *_ = self.combine.ancsplit_score(anc_id, vw, r1)

        # Dirty test fix to recover the config ordering consistent with subsplit vw
        v, w = sorted(vw, key=tuple)
        if tuple(vw) != (v,w):
            best_config = [(tuple(reversed(vertic)), tuple(reversed(horiz))) for vertic,horiz in best_config]

        A, B, X, C, D, E, Y, R = 0, 1, 2, 3, 4, 5, 6, 7
        print('min_score', min_score, 'best_config', best_config)
        if anc_id in (R,X,A,B,E):
            assert not best_config # because it would involve a double-transfer
        else:
            assert len(best_config) == 1
            #FIXME: there is a subtle difference in the generated configs of Y and R, because Y is a polytomy:
            # from Y, the config is [((C,), (D,)), ()],
            # from R, the config is [(C,D), ()]
            # This is NOT a feature
            vertical, transfered = best_config[0]
            if anc_id in (R,Y):
                assert transfered == (None,None)
                assert flatten(vertical) == (C, D), "The only mapping of v,w is C,D; vertical=%s" % (vertical,)

            elif anc_id == C:
                assert best_config[0] == ((C, None), (None,D))
            else:
                assert best_config[0] == ((None, D), (C,None))
        if np.isnan(expected_score):
            assert np.isnan(min_score)
            assert np.isnan(best_count).all()
        else:
            assert min_score == expected_score
            assert best_count.tolist() == expected_count.tolist()
            assert min_score == (best_count[:3] * self.combine.weights).sum() + best_count[3]*self.combine.freq

    def test_trim_combinations_above_limit(self):
        combine = self.combine
        features = self.amalgam.features
        r, tu, vw = self.splits
        r1 = max(r, key=tuple)
        v,w = sorted(vw, key=tuple)
        A, B, X, C, D, E, Y, R = 0, 1, 2, 3, 4, 5, 6, 7

        combine.max_ancsplit_configs = 10
        
        for terminal in vw:
            features[terminal] = combine([], terminal)

        #features[v]['partial'] = {Y: {}}
        #features[w]['partial'] = {Y: {}}
        #features[v]['partial_count'] = {Y: {}}
        #features[w]['partial_count'] = {Y: {}}
        # The taxon_id are arbitrary here, it doesn't matter if they don't correspond
        for taxon_id, partial in zip(range(8,13), range(5)):
            combine.parents[taxon_id] = [C,Y,R]
            features[v]['partial'][Y][(taxon_id,),None] = partial
            features[v]['partial_count'][Y][(taxon_id,),None] = np.array([0,partial,0,0])
        for taxon_id, partial in zip(range(13,16), range(3)):
            combine.parents[taxon_id] = [D,Y,R]
            features[w]['partial'][Y][(taxon_id,),None] = partial
            features[w]['partial_count'][Y][(taxon_id,),None] = np.array([0,partial,0,0])

        min_score, best_count, best_config, ancsplit_partial, \
        ancsplit_count, ancsplit_direct_losses, ancsplit_configs = self.combine.ancsplit_score(Y, vw, r1)

        # Dirty test fix of the config ordering to match input subsplit 'vw':
        if tuple(vw) != (v,w):
            ancsplit_configs = [(tuple(reversed(vertic)), tuple(reversed(horiz))) for vertic,horiz in ancsplit_configs]
        assert (((C,),(D,)), (None, None)) in ancsplit_configs
        assert len(ancsplit_count) == 10



class Test_combine_single_tree_splits(TestBase_combine):
    def test_best_score(self):
        features = self.amalgam.features
        combine = self.combine
        r, tu, vw = self.splits
        r0, r1 = sorted(r, key=tuple)
        features[r0] = combine([tu], r0)
        features[r1] = combine([vw], r1)
        rootfeatures = combine([r], self.amalgam.full)

        all_estimated = (features[r0], features[r1], rootfeatures)
        all_expected  = (self.tu_anc_expected_scores, self.vw_anc_expected_scores, self.r_anc_expected_scores)

        for split, estimated, expected in zip((tu, vw, r), all_estimated, all_expected):
            print('split', split)
            score = estimated['score']
            count = estimated['count']
            assert score.shape == (8,)
            for anc, expected_score, expected_count in expected:
                anc_id = combine.anc_index[anc]
                print('anc %d %s expected score: %s' % (anc_id, anc, expected_score))
                if np.isnan(expected_score):
                    assert np.isnan(score[anc_id])
                    assert np.isnan(count[anc_id]).all()
                else:
                    assert score[anc_id] == expected_score
                    assert count[anc_id].tolist() == expected_count.tolist()


class TestBase_multi_subsplits:
    """Test that the combiner correctly score each alternative gene subsplit"""
    def setup_method(self):
        # Trees:
        #
        # 0    2,3    0    3,4    0    2,4
        #  \__/        \__/        \__/
        #  /  \        /  \        /  \
        # 1    4      1    2      1    3
        #                                   
        #  0.7         0.25        0.05      
        f1, f2, f3 = self.subsplit_y_CCPs
        print('f1 f2 f3:', f1, f2, f3)
        xy = bitset_pack(((0,1), (2,3,4)))

        x1 = bitset_pack(((0,), (1,)))

        y1 = bitset_pack(((2,3), (4,)))
        y2 = bitset_pack(((3,4), (2,)))
        y3 = bitset_pack(((2,4), (3,)))

        suby1 = bitset_pack(((2,), (3,)))
        suby2 = bitset_pack(((3,), (4,)))
        suby3 = bitset_pack(((2,), (4,)))
        
        # For each subsplit in (y1, y2, y3), build the corresponding parent split
        bi_y1 = bitset_pack(((2,3), (0,1,4)))  # 0.7
        bi_y2 = bitset_pack(((3,4), (0,1,2)))  # 0.25
        bi_y3 = bitset_pack(((2,4), (0,1,3)))  # 0.05

        bi = {xy: 1, bi_y1: f1, bi_y2: f2, bi_y3: f3}
        tri = Counter({y1: f1, y2: f2, y3: f3})

        # for each subsplit in (y1, y2, y3), add the corresponding complementary unrooted subsplits 
        tri[bitset_pack(((2,3), (0,1)))] = f1
        tri[bitset_pack(((0,1), (4,)))]  = f1

        tri[bitset_pack(((3,4), (0,1)))] = f2
        tri[bitset_pack(((0,1), (2,)))]  = f2

        tri[bitset_pack(((2,4), (0,1)))] = f3
        tri[bitset_pack(((0,1), (3,)))]  = f3

        # And subsplits of trivial splits: (remember: these subsplits are rooted on the parent split point)
        # subsplits of (0,1,2,3)
        tri[bitset_pack(((3,), (0,1,2)))] = f2
        tri[bitset_pack(((2,), (0,1,3)))] = f3
        # subsplits of (0,1,2,4)
        tri[bitset_pack(((2,), (0,1,4)))] = f1
        tri[bitset_pack(((4,), (0,1,2)))] = f2
        # subsplits of (0,1,3,4)
        tri[bitset_pack(((0,1,4), (3,)))] = f1
        tri[bitset_pack(((0,1,3), (4,)))] = f3
        
        #subsplits of (0,2,3,4)
        tri[bitset_pack(((0,), (2,3,4)))] = 1
        
        # subsplits of (1,2,3,4)
        tri[bitset_pack(((1,), (2,3,4)))] = 1

        # trivial splits (single leaf) have freq=1
        include_trivial_splits(bi, set((0,1,2,3,4)), n=1)

        self.leaves = tuple(BitSet((g,)) for g in range(5))

        #unrooted_complement_tripartitions(tri, BitSet((0,1,2,3,4)))
        print('bi:', fmt_distrib(bi, ''))
        print('tri:', fmt_distrib(tri, ''))
        include_trivial_subsplits(tri, bi, unrooted=False)
        #print('tri complemented:', fmt_distrib(tri, ''))

        amalgam = Amalgam(bi, tri, n=1)

        print('increasing clusters:', amalgam.increasing_clusters)
        print('subsplits:\n', '\n   '.join('%r: %s' % (parent,sub) for parent,sub in amalgam.subsplits.items()))
        amalgam.verify_freqs()
        self.splits = xy, [x1, y1, y2, y3], [suby1, suby2, suby3]
        self.amalgam = amalgam

        get_sp = dict(zip(range(5), 'ABCDE')).__getitem__
        self.species_tree = species_tree.copy()
        self.combine = AmalgDLTCombiner(self.species_tree, amalgam, get_sp, 'ABXCDEYR',
                                   dup=Dw, loss=Lw, transfer=Tw, freq=Fw)
        for leaf in self.leaves:
            amalgam.features[leaf] = self.combine([], leaf)


class Test_multi_subsplits_under_polytomy(TestBase_multi_subsplits): # Should probably first test it under the dichotomic case
    subsplit_y_CCPs = 0.7, 0.25, 0.05

    def test_poly_resolve_from_CCP(self):
        A,B,X,C,D,E,Y,R = 0,1,2,3,4,5,6,7

        features = self.amalgam.features
        combine = self.combine
        xy, (x1, y1, y2, y3), (suby1, suby2, suby3) = self.splits
        xy0, xy1 = sorted(xy, key=tuple)
        clust_y1, clust_y2, clust_y3 = [max(y, key=len) for y in (y1, y2, y3)]
        print('CCP:', self.amalgam.CCP)

        features[xy0] = combine([x1], xy0)
        features[clust_y1] = combine([suby1], clust_y1)
        features[clust_y2] = combine([suby2], clust_y2)
        features[clust_y3] = combine([suby3], clust_y3)
        assert features[clust_y1]['count'][Y,:3].sum() == 1, "1 loss (at stem). %s" % features[clust_y1]['count'][Y]
        print('partial_count:', features[clust_y1]['partial_count'])
        # Dirty fix:
        configkey = (((C,), (D,)) if list(y1) == sorted(y1, key=tuple) else ((D,), (C,))),\
                    (None,None)
        assert features[clust_y1]['partial_count'][Y][configkey][:3].sum() == 0, "%s" % features[clust_y1]['partial_count'][Y]

        estimated = combine([y1, y2, y3], xy1)
        score = estimated['score']
        best_splits_configs = estimated['best_split_config']
        best_count = estimated['count']
        print('score', score)
        print('best_splits_configs', best_splits_configs)
        print('best_count', best_count)

        print('best freq retained:', np.exp((score[Y]) / -Fw))
        assert score[Y] == - Fw * np.log(0.7)

        print('amalgam.subsplits[xy[1]]', self.amalgam.subsplits[xy1])
        best_subsplits_Y = set(b for b,c in best_splits_configs[Y])
        assert len(best_subsplits_Y) == 1
        best_subsplit = self.amalgam.subsplits[xy1][list(best_subsplits_Y)[0]]
        assert best_subsplit == y1, "Y best_subsplit=%s VS y1=%s" % (best_subsplit, y1)  # highest CCP, equivalent reconciliation

        assert not best_count[R,:3].sum() == 0, "Expect zero D,L,T"

class Test_exaequo_subsplits_under_polytomy(TestBase_multi_subsplits):
    subsplit_y_CCPs = 0.4, 0.4, 0.2

    def test_poly_resolve_from_CCP(self):
        A,B,X,C,D,E,Y,R = 0,1,2,3,4,5,6,7

        features = self.amalgam.features
        combine = self.combine
        xy, (x1, y1, y2, y3), (suby1, suby2, suby3) = self.splits
        xy0, xy1 = sorted(xy, key=tuple)
        clust_y1, clust_y2, clust_y3 = [max(y, key=len) for y in (y1, y2, y3)]
        print('CCP:', self.amalgam.CCP)

        features[xy0] = combine([x1], xy0)
        features[clust_y1] = combine([suby1], clust_y1)
        features[clust_y2] = combine([suby2], clust_y2)
        features[clust_y3] = combine([suby3], clust_y3)
        assert features[clust_y1]['count'][Y,:3].sum() == 1, "1 loss (at stem). %s" % features[clust_y1]['count'][Y]
        print('partial_count:', features[clust_y1]['partial_count'])
        # Dirty fix:
        configkey = (((C,), (D,)) if list(y1) == sorted(y1, key=tuple) else ((D,), (C,))),\
                    (None,None)
        assert features[clust_y1]['partial_count'][Y][configkey][:3].sum() == 0, "%s" % features[clust_y1]['partial_count'][Y]

        estimated = combine([y1, y2, y3], xy1)
        score = estimated['score']
        best_splits_configs = estimated['best_split_config']
        best_count = estimated['count']
        print('score', score)
        print('best_splits_configs', best_splits_configs)
        print('best_count', best_count)

        print('best freq retained:', np.exp((score[Y]) / -Fw))
        assert score[Y] == - Fw * np.log(self.subsplit_y_CCPs[0])  # FIXME: the two best frequencies should be summed

        print('amalgam.subsplits[xy[1]]', self.amalgam.subsplits[xy1])
        best_subsplits_Y = set(b for b,c in best_splits_configs[Y])
        assert len(best_subsplits_Y) == 2
        best_subsplits_Y = [self.amalgam.subsplits[xy1][b] for b in best_subsplits_Y]
        assert set(best_subsplits_Y) == set((y1,y2)), "best_subsplits_Y=%s VS y1=%s y2=%s" % (best_subsplits_Y, y1, y2)  # highest CCP, equivalent reconciliation

        assert not best_count[R,:3].sum() == 0, "Expect zero D,L,T"


genetree_newicks = dict(
    dicho_spe_only=         '((a,b)x,(c,(d,e))y)r;',
    dicho_spe_indirectchild='(a,(c,(d,e))y)r;',
    dicho_dup_only=         '(((a1,a2)a,b)x,(c,(d,e))y)r;',
    dicho_dup_oneloss=      '(((a1,b)x1,a2)x,(c,(d,e))y)r;',
    dicho_transfer=         '(((a,c2)ta,b)x,((c,d),e)y)r;',
    #dicho_transfer_indirectchild='(((a,c2)tx,(c,(d,e))y)r' # not interpreted as transfer with costs [1, 0.5, 3]; but with costs [2,1,3]
    poly_spe_lostonechild= '((a,b)x,(c,d)y)r;',
    poly_dup_complete=     '((a,b)x,((c,(d,e))y1,(c2,(d2,e2))y2)y)r;',
    poly_dup_onelossafter= '((a,b)x,((c,(d,e))y1,(c2,d2)y2)y)r;',
    poly_dup_onelossbefore='((a,b)x,((c,d)y1,(c2,d2)y2)y)r;',
    poly_basal_transfer='((a,b)x,((a2,(c,d))ty,e)y)r;',
    poly_transfer='((a,b)x,((a2,c)tc,(d,e))y)r;', # nested transfer (deep)
    poly_spe_indirectchild='((a,b)x,(c,(d,f))y)r;',
    poly_spe_indirectchild2='((a,b)x,(c,(d,h))y)r;',
    poly_spe_indirectchild_notleaf='((a,b)x,(c,(d,(h,i)g))y)r;'
    )

genetree_newicks['dicho_dup_oneloss_or_transfer'] = genetree_newicks['dicho_dup_oneloss']

best_rootsplits = dict(
    dicho_spe_only=         ['R', [(('a','b'),         ('c','d','e'))]],
    dicho_spe_indirectchild=['R', [(('a',),            ('c','d','e'))]],
    dicho_dup_only=         ['R', [(('a1', 'a2', 'b'), ('c','d','e'))]],
    dicho_dup_oneloss=      ['R', [(('a1', 'b', 'a2'), ('c','d','e'))]],
    dicho_transfer=         ['R', [(('a','c2','b'),    ('c','d','e'))]]
    #dicho_transfer_indirectchild='(((a,c2)tx,(c,(d,e))y)r' # not interpreted as transfer with costs [1, 0.5, 3]; but with costs [2,1,3]
    )

class Test_Amalgam_walk:
    @pytest.mark.parametrize('case,rootstate', best_rootsplits.items())
    #FIXME: redundant with the test below, because I designed the cases for rootsplit 'r'
    def test_best_rootstate(self, case, rootstate):

        newick = genetree_newicks[case]
        tree = Tree(newick, format=1)

        amalgam = build_amalgam([newick])
        print('CCP:', amalgam.CCP)

        combine = AmalgDLTCombiner(species_tree, amalgam, get_sp, ancestors='ABXCDEYR',
                                   dup=Dw, loss=Lw, transfer=Tw, freq=Fw)
        if 'transfer' in case:
            combine.set_weights(2,1,3)
        amalgam.init_leaves(partial(combine, []))
        amalgam.walk(combine)
        features = amalgam.features

        anc_id = combine.anc_index[rootstate[0]]
        best_rootsplits = [tuple(amalgam.translate(clust) for clust in amalgam.subsplits[amalgam.full][b])
                            for b,_ in features[amalgam.full]['best_split_config'][anc_id]]
        # Dirty fix: ordering
        best_rootsplits = [tuple(reversed(split)) if list(split) != sorted(split, key=tuple) else split
                            for split in best_rootsplits]

        print('tmp_score:',         features[amalgam.full]['tmp_score'])
        print('partial score:',     features[amalgam.full]['partial']) 
        print('best count:',        features[amalgam.full]['count'])
        print('best_split_config:', features[amalgam.full]['best_split_config'])
        #for b, (vertical, transfered) in features[amalgam.full['best_split_config']:

        print('best_rootsplits:', best_rootsplits)
        assert best_rootsplits == rootstate[1]


class Test_fill_missing_partials_adding_losses:
    """TODO"""

# input_tree, root split, total event count, node paralogy test (is it a dup?)
backtrace_outputs = dict(
    dicho_spe_only=         [[0,0,0,0], {'a': 0}],
    dicho_spe_indirectchild=[[0,1,0,0], {'a': 0}],
    dicho_dup_only=         [[1,0,0,0], {'a1^a2': 1.}],
    dicho_dup_oneloss=      [[1,1,0,0], {'a2': 1.}],
    dicho_dup_oneloss_or_transfer=[[1./3, 1./3, 2./3, 0], {'a1': 1./3, 'a2': 1./3}], # 2 scenarios involve transfer
    dicho_transfer=         [[0,0,1,0], # with the default DTL costs, it is better than the DL rec [2,4,0,0].
                             {'ta': 0, 'c2': 1.}],
    poly_spe_lostonechild= [[0,1,0,0], {'c':0}],
    poly_dup_complete=     [[1,0,0,0], {'y1^y2': 1.}],  # This is arbitrary, because y1 and y2 have the same size
    poly_dup_onelossafter= [[1,1,0,0], {'y2': 1.}],
    poly_dup_onelossbefore=[[1,1,0,0], {'y1^y2': 1.}],
    poly_basal_transfer=   [[0,0,1,0], {'a2': 1., 'ty': 0}],
    poly_transfer=         [[0,0,1,0], {'a2': 1., 'tc': 0}],
    poly_spe_indirectchild=[[0,1,0,0], {'y': 0, 'f': 0, 'd': 0}],
    poly_spe_indirectchild2=[[0,2,0,0], {'y': 0, 'h': 0, 'd': 0}],
    poly_spe_indirectchild_notleaf=[[0,1,0,0], {'y': 0, 'h': 0, 'd': 0}]
    )


def get_split(node, ids):
    """Return the split object corresponding to a divergence"""
    if len(node.children) != 2:
        raise ValueError('A dichotomic node is required')
    child1, child2 = node.children
    clade1 = [ids[leafname] for leafname in child1.iter_leaf_names()]
    clade2 = [ids[leafname] for leafname in child2.iter_leaf_names()]
    return bitset_pack((clade1, clade2))

def get_clust(node, ids):
    clade = [ids[leafname] for leafname in node.iter_leaf_names()]
    return BitSet(clade)


def get_sp(leaflabel):
    return leaflabel[0].upper()


class Backtrack_setup:
    species_tree = species_tree
    def compute_backtrace(self, case):
        self.newick = genetree_newicks[case]
        if case.startswith('poly_spe_indirectchild'):
            species_tree = self.species_tree.copy()
            node_E = species_tree&'E'
            node_E.add_child(name='F')
            node_G = node_E.add_child(name='G')
            if case.endswith('2') or case.endswith('notleaf'):
                node_G.add_child(name='H')
                node_G.add_child(name='I')
        else:
            species_tree = self.species_tree

        amalgam = build_amalgam([self.newick])
        print('CCP:', amalgam.CCP)
        print('ids:', ' '.join('%s:%s' % item for item in sorted(amalgam.ids.items(), key=lambda item: item[1])))
        combine = AmalgDLTCombiner(species_tree, amalgam, get_sp)
        if 'transfer' in case:
            # Give more chance for transfers to occur, otherwise it will favor more dup/loss, and
            # this is not what the test case intend to do
            combine.set_weights(2,1,3)

        amalgam.init_leaves(partial(combine, []))
        amalgam.walk(combine)
        print('root best taxon', np.nanargmin(amalgam.features[amalgam.full]['score']))
        print('root best splits (R)', amalgam.features[amalgam.full]['best_split_config'][7])
        print('root score (R):', amalgam.features[amalgam.full]['score'][7])
        print('root count (R):', amalgam.features[amalgam.full]['count'][7])
        self.amalgam = amalgam
        self.combine = combine
        return backtrack_best_rec(combine)


class Test_backtrack_best_rec_events(Backtrack_setup):
    @pytest.mark.parametrize('case', backtrace_outputs.keys()) 
    def test_expected_best_rec(self, case):
        expected_count, expected_nodetype = backtrace_outputs[case]

        backtrace, data, dup_splits, para_clades, xeno_clades \
                = self.compute_backtrace(case)

        amalgam = self.amalgam
        tree = Tree(self.newick, format=1)
        expected_rootsplit = get_split(tree, amalgam.ids)

        print('backtrace:', backtrace)
        node0, (child1, child2) = backtrace[0]
        rootclust, anc, resolver, count, _ = data[node0]
        rootsplit = bitset_pack((data[child1][0], data[child2][0]))

        assert amalgam.features[rootclust]['count'][anc].tolist() == expected_count
        assert count.tolist() == expected_count
        assert rootclust == amalgam.full

        uniq_rec_nodes = set(node_id for node_id, _ in backtrace)
        if 'dup_oneloss_or_transfer' in case:
            assert len(backtrace) > len(uniq_rec_nodes), "expect >1 solution"
        else:
            assert rootsplit == expected_rootsplit
            assert len(backtrace) == len(uniq_rec_nodes), "expect a single solution"

        print('dup_splits:', dup_splits)
        print('para_clades:', para_clades)
        print('xeno_clades:', xeno_clades)
        for name, expected_freq in expected_nodetype.items():
            alt_names = name.split('^')  # mutually exclusive alternatives (XOR)
            clusts = [get_clust(tree&alt, amalgam.ids) for alt in alt_names]
            if 'transfer' in case:
                typefreqs = [xeno_clades[clust] for clust in clusts]
            else:
                typefreqs = [para_clades[clust] for clust in clusts]
            if len(alt_names) == 1:
                f = typefreqs[0]
                assert f == expected_freq, 'wrong freq=%g of %s:%r (expected %g)' % (f, name, clusts[0], expected_freq)
            else:
                assert sum(f==expected_freq for f in typefreqs) == 1, 'Exactly one alternative freq(%s)=%s must match expected %g' % (name, typefreqs, expected_freq)
            print('Correct freq %s of %s:%r' % (typefreqs, name, clusts))

#                      number of optimal trees
rectree_outputs = dict(
    dicho_spe_only=         1,
    dicho_spe_indirectchild=1,
    dicho_dup_only=         1,
    dicho_dup_oneloss=      1,
    dicho_dup_oneloss_or_transfer=3, # 2 scenarios involve transfer
    dicho_transfer=         1, # with the default DTL costs, it is better than the DL rec [2,4,0,0].
    poly_spe_lostonechild=  1,
    poly_dup_complete=      1,
    poly_dup_onelossafter=  1,
    poly_dup_onelossbefore= 1,
    poly_basal_transfer=    1,
    poly_transfer=          1,
    poly_spe_indirectchild= 1,
    poly_spe_indirectchild2=1,
    poly_spe_indirectchild_notleaf=1
    )


class Test_rectrees_from_backtrace(Backtrack_setup):
    @pytest.mark.parametrize('case', rectree_outputs.keys()) 
    def test_expected_rectree_numbers(self, case):
        expected_number = rectree_outputs[case]

        backtrace_args = self.compute_backtrace(case)
        print('data.keys:', backtrace_args[1].keys())
        rectrees = rectrees_from_backtrace(self.combine, *backtrace_args)
        print('%d trees from backtrace = %s' % (len(rectrees), backtrace_args[0]))

        assert len(rectrees) == expected_number
        assert len(rectrees[0]) == len(self.amalgam.ids)


class Test_backtrack_best_rec_mapping:
    """Test that ancestors are mapped to the correct nodes"""
    def setup_method(self):
        full = BitSet((0,1,2,3))
        x, y = BitSet((0,1)), BitSet((2,3))
        sx = (BitSet((0,)), BitSet((1,)))
        sy = (BitSet((2,)), BitSet((3,)))
        nsx0 = BitSet((0,)), BitSet((1,2,3))
        nsx1 = BitSet((0,2,3)), BitSet((1,))
        t,u,v,w = [BitSet((k,)) for k in range(4)]
        self.amalgam = Amalgam({(x,y): 1}, {sx: 1, sy: 1})
        print('subsplits:', self.amalgam.subsplits)
        self.splits = (x,y), sx, sy

        features = self.amalgam.features
        # Let's represent an optimal reconciliation:
        # t,u,v,w mapped to C D E E. -> best root taxon is Y.
        #                            -> root taxon R is suboptimal (implies one loss)

        A,B,X,C,D,E,Y,R = 0,1,2,3,4,5,6,7
        for leaf, sp_id in zip((t,u,v,w), (C,D,E,E)):
            score = np.full(8, NaN)
            leafcount = np.full((8,4), NaN)
            score[sp_id] = 0
            leafcount[sp_id] = 0
            features[leaf] = dict(taxon_id=sp_id, score=score, count=leafcount)

        score_x = np.full(8, NaN)
        count_x = np.full((8,4), NaN)
        score_x[Y] = 0.5    # 1 loss
        score_x[[C,D]] = 3  # 1 transfer
        count_x[Y] = [0,1,0,0]
        count_x[[C,D]] = [[0,0,1,0]]*2
        print('count(x=%r) = %s shape %s count_x[5]=%s' % (x, count_x, count_x.shape, count_x[5]))
        features[x] = dict(score=score_x, count=count_x,
                           best_split_config={Y: [(0, ((C,D), (None,None)))],
                                              C: [(0, ((C,None), (None,D)))],
                                              D: [(0, ((None,D), (C,None)))]},
                           partial={})

        score_y = np.full(8, NaN)
        count_y = np.full((8,4), NaN)
        score_y[E] = 1      # 1 duplication
        count_y[E] = [1,0,0,0]
        print('count(y=%r) = %s shape %s count_y[5]=%s' % (y, count_y, count_y.shape, count_y[5]))
        features[y] = dict(score=score_y, count=count_y,
                           best_split_config={E: [(0, ((E,E), (None,None)))]},
                           partial={})

        score_r = np.full(8, NaN)
        count_r = np.full((8,4), NaN)
        score_r[Y] = 1        # 1 duplication
        score_r[[C,D,E]] = 7  # 1 dup + 2T
        count_r[Y] = [1,0,0,0]
        count_r[[C,D,E]] = [[1,0,2,0]]*3
        features[full] = dict(score=score_r, count=count_r,
                              best_split_config={Y: [(0, ((Y,E),    (None,None)))#,
                                                     #(0, ((C,E),    (None,None))), # suboptimal
                                                     #(0, ((D,E),    (None,None)))
                                                     ],
                                                 C: [(0, ((C,None), (None,E)))],
                                                 D: [(0, ((D,None), (None,E)))],
                                                 E: [(0, ((None,E), (C,None))),
                                                     (0, ((None,E), (D,None)))]
                                                 #, A: [], B: [], X: [], R: []
                                                 },
                              partial={})

    def test_best_taxa_or_resolver_auto(self):
        rootfeatures = self.amalgam.features[self.amalgam.full]
        best_score, best_root_resolvers = best_taxa_or_resolvers(rootfeatures, range(8))
        assert best_score == 1, "expect to find score_r[Y] but found %g" % best_score
        assert len(best_root_resolvers) == 1
        best_anc_id, best_resolver = best_root_resolvers[0]
        assert best_anc_id == 6  # Y
        assert best_resolver is None  # The test data treated without partial scores

    def test_best_taxa_or_resolver_constraint(self):
        rootfeatures = self.amalgam.features[self.amalgam.full]
        best_score, best_root_resolvers = best_taxa_or_resolvers(rootfeatures, [3])
        assert best_score == 7, "expect to find score_r[C] but found %g" % best_score
        assert len(best_root_resolvers) == 1
        best_anc_id, best_resolver = best_root_resolvers[0]
        assert best_anc_id == 3  # C
        assert best_resolver is None  # The test data treated without partial scores

    def test_suboptimal_root_taxon(self):
        """Obtain the reconciliation rooted on the selected taxon (even if suboptimal)."""
        # Fill the features manually:
        (x,y), sx, sy = self.splits
        A,B,X,C,D,E,Y,R = 0,1,2,3,4,5,6,7
        CombineData = namedtuple('CombineData', ['amalgam', 'anc_range', 'ancestors'])
        backtrace, data, *_ = backtrack_best_rec(
                                CombineData(self.amalgam, range(8), list('ABXCDEYR')),
                                root_anc_id=C)
        assert len(backtrace) == 3, "Expect 1 tree with 3 internal nodes"
        print('data:', data)
        node0, (child1, child2) = backtrace[0]
        clust0, anc0, *_ = data[node0]
        clust1, anc1, *_ = data[child1]
        clust2, anc2, *_ = data[child2]
        assert clust0 == self.amalgam.full
        assert clust1 == x
        assert clust2 == y
        assert anc0 == C
        assert anc1 == C
        assert anc2 == E

    def test_auto_root_taxon(self):
        """Obtain the reconciliation rooted on the optimal taxon."""
        # Fill the features manually:
        (x,y), sx, sy = self.splits
        A,B,X,C,D,E,Y,R = 0,1,2,3,4,5,6,7
        CombineData = namedtuple('CombineData', ['amalgam', 'anc_range', 'ancestors'])
        backtrace, data, *_ = backtrack_best_rec(CombineData(self.amalgam, range(8), list('ABXCDEYR')))
        print('backtrace (%d nodes): %s' % (len(backtrace), backtrace))
        assert len(backtrace) == 3, "Expect 1 tree with 3 internal nodes"
        print('data:', data)
        node0, (child1, child2) = backtrace[0]
        clust0, anc0, *_ = data[node0]
        clust1, anc1, *_ = data[child1]
        clust2, anc2, *_ = data[child2]
        assert clust0 == self.amalgam.full
        assert clust1 == x
        assert clust2 == y
        assert anc0 == Y
        assert anc1 == Y
        assert anc2 == E

        node1, (child3, child4) = backtrace[1]
        assert node0 != node1

    def test_optimal_root_explicit_constraint(self):
        (x,y), sx, sy = self.splits
        A,B,X,C,D,E,Y,R = 0,1,2,3,4,5,6,7
        CombineData = namedtuple('CombineData', ['amalgam', 'anc_range', 'ancestors'])
        backtrace, data, *_ = backtrack_best_rec(
                                CombineData(self.amalgam, range(8), list('ABXCDEYR')),
                                root_anc_id=Y)
        assert len(backtrace) == 3, "Expect 1 tree with 3 internal nodes"
        print('data:', data)
        node0, (child1, child2) = backtrace[0]
        clust0, anc0, *_ = data[node0]
        clust1, anc1, *_ = data[child1]
        clust2, anc2, *_ = data[child2]
        assert clust0 == self.amalgam.full
        assert clust1 == x
        assert clust2 == y
        assert anc0 == Y
        assert anc1 == Y
        assert anc2 == E


