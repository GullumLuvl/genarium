#!/usr/bin/env python3


from myriphy.compactset import *
import pytest


union_in_out = [( [(0,1), (2,)], (0,1,2) ),
                ( [(4,5), (6,)], (4,5,6) ),
                ( [(4,5), (5,6)], (4,5,6) )]


@pytest.mark.parametrize('in_out', union_in_out)
def test_BitSet_union(in_out):
    (first, *others), out = in_out
    uni = tuple(BitSet(first).union(*map(BitSet, others)))

    print('in: %s %s -> uni: %s' % (first, others, uni))
    assert uni == out

diff_in_out = [( [(1,2),(3,)], (1,2) ),
               ( [(1,2),(2,)], (1,) ),
               ( [(1,2),(0,)], (1,2) ),
               ( [(1,2),(1,2)], () )]

@pytest.mark.parametrize('in_out', diff_in_out)
def test_BitSet_diff(in_out):
    (first, *others), out = in_out
    diff = tuple(BitSet(first).difference(*map(BitSet, others)))

    print('in: %s %s -> diff: %s' % (first, others, diff))
    assert diff == out


complem_in_out = [( ((),      3), (0,1,2) ),
                  ( ((0,1,2), 3), () ),
                  ( ((0,1,2), 5), (3,4) ),
                  ( ((2,3,4), 5), (0,1) )]


@pytest.mark.parametrize('in_out', complem_in_out)
def test_BitSet_complement(in_out):
    (val, N), out = in_out
    compl = tuple(BitSet(val).complement(N))

    print('in: %s %s -> complement: %s' % (val, N, compl))
    assert compl == out
