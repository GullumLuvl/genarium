#!/usr/bin/env python3

import pytest
import re

from myriphy.newick import DELIM_REGEX, decompose_newick, decompose_newick_withdist, NewickDecomposer,\
                           IterNewickClusters, IterNewickClustersPartial
import logging
try:
    from myriphy.newick import logger as newicklogger
    newicklogger.setLevel(logging.DEBUG)
except ImportError:
    pass

def get_nwk_as_is(txt):
    return txt

def get_nwk_branchlength(txt):
    return re.sub(r'([a-z]+)', r'\1:0.77', txt)


def clusters_from_newick(newick, ids=None, include_full=False):
    iterclusters = IterNewickClusters(newick, ids)
    for clust, subclust in iterclusters:
        print('yield %r %s' % (clust, subclust))
        yield clust, subclust
    #yield from iterclusters
    if include_full:
        print('yield %r %s (root)' % iterclusters.root)
        yield iterclusters.root

def list_clusters_from_newick(newick):
    return list(tuple(clust) for clust, _ in clusters_from_newick(newick, include_full=True))


class Test_decompose_nodist:
    @pytest.mark.parametrize('end', ['', ';'])
    @pytest.mark.parametrize('get_nwk', [get_nwk_as_is, get_nwk_branchlength])
    def test_without_branchlengths(self, get_nwk, end):
        nwk = get_nwk('((a,b),c)' + end)
        print([match.group() for match in DELIM_REGEX.finditer(nwk)])
        decomposed = tuple([elem for pos, elem in decompose_newick(nwk)])
        print(decomposed)
        assert decomposed == ('(', '(', 'a', ',', 'b', ')', ',', 'c', ')', ';')


DECOMPOSERS = {'py': decompose_newick_withdist, 'cy': NewickDecomposer}


class Test_decompose_withdist:
    @pytest.mark.parametrize('decomposer', ['py', 'cy'])
    @pytest.mark.parametrize('end', ['', ';'])
    def test_without_branchlengths(self, end, decomposer):
        nwk = '((a,b),c)' + end
        decomposing = DECOMPOSERS[decomposer](nwk)
        decomposed = [elem[1:] for elem in decomposing]
        print(decomposed)
        assert decomposed == [('(', '', ''),
                              ('(', '', ''),
                              ('a', '', ''),
                              (',', '', ''),
                              ('b', '', ''),
                              (')', '', ''),
                              (',', '', ''),
                              ('c', '', ''),
                              (')', '', ''),
                              (';', '', '')]
    @pytest.mark.parametrize('decomposer', ['py', 'cy'])
    def test_with_branchlengths(self, decomposer):
        nwk = '((a:0.3,b:0.5):0.1,c:0.7);'
        decomposing = DECOMPOSERS[decomposer](nwk)
        decomposed = [elem[1:] for elem in decomposing]
        print(decomposed)
        assert decomposed == [('(', '', ''),
                              ('(', '', ''),
                              ('a', '', '0.3'),
                              (',', '', ''),
                              ('b', '', '0.5'),
                              (')', '', '0.1'),
                              (',', '', ''),
                              ('c', '', '0.7'),
                              (')', '', ''),
                              (';', '', '')]
    @pytest.mark.parametrize('decomposer', ['py', 'cy'])
    @pytest.mark.parametrize('end', ['', ';'])
    def test_missing_englobing_parentheses(self, end, decomposer):
        nwk = '(a,b),c' + end
        decomposing = DECOMPOSERS[decomposer](nwk)
        decomposed = [elem[1:] for elem in decomposing]
        print(decomposed)
        assert decomposed == [('(', '', ''),
                              ('a', '', ''),
                              (',', '', ''),
                              ('b', '', ''),
                              (')', '', ''),
                              (',', '', ''),
                              ('c', '', ''),
                              (';', '', '')]



class Test_list_clusters_from_newick:
    @pytest.mark.parametrize('get_newick', [get_nwk_as_is, get_nwk_branchlength])
    def test_one_clust_sized_two(self, get_newick):
        newick = get_newick('(a,b)')
        clusters = list_clusters_from_newick('(a,b)')
        print(clusters)
        assert len(clusters) == 1
        assert clusters[0] == ('a', 'b')

    @pytest.mark.parametrize('get_newick', [get_nwk_as_is, get_nwk_branchlength])
    def test_one_nested_part(self, get_newick):
        clusters = list_clusters_from_newick(get_newick('((a,b),c)'))
        print(clusters)
        assert len(clusters) == 2
        assert clusters[0] == ('a', 'b')
        assert clusters[1] == ('a', 'b', 'c')

    @pytest.mark.parametrize('get_newick', [get_nwk_as_is, get_nwk_branchlength])
    def test_no_parenthesis_means_one_clust(self, get_newick):
        clusters = list_clusters_from_newick(get_newick('a,b'))
        assert len(clusters) == 1
        assert clusters[0] == ('a','b')

    @pytest.mark.parametrize('get_newick', [get_nwk_as_is, get_nwk_branchlength])
    def test_missing_englobing_parentheses(self, get_newick):
        clusters = list_clusters_from_newick(get_newick('(a,b),(c,d)'))
        print('clusters =', clusters)
        assert len(clusters) == 3
        assert clusters[0] == ('a','b')
        assert clusters[1] == ('c','d')
        assert clusters[2] == ('a', 'b', 'c','d')

    @pytest.mark.parametrize('get_newick', [get_nwk_as_is, get_nwk_branchlength])
    def test_fail_on_extra_closing(self, get_newick):
        with pytest.raises(ValueError):
            list_clusters_from_newick(get_newick('(a,b))'))

    @pytest.mark.parametrize('get_newick', [get_nwk_as_is, get_nwk_branchlength])
    def test_fail_on_unclosed(self, get_newick):
        with pytest.raises(ValueError):
            list_clusters_from_newick(get_newick('((a,b)'))

    def test_one_nested_part_indexed(self):
        ids = dict(a=0, b=1, c=2)
        clusters = list(tuple(clust) for clust, subclust in clusters_from_newick('((a,b),c)', ids, include_full=True))
        print(clusters)
        assert len(clusters) == 2
        assert clusters[0] == (0, 1)
        assert clusters[1] == (0, 1, 2)


class Test_list_subclusters:
    def test_one_nested_part(self):
        clusters, subclustdists = list(zip(*clusters_from_newick('((a,b),c)', include_full=True)))
        print(subclustdists)
        subclusters = [[item[0] for item in subclustdist] for subclustdist in subclustdists]
        assert subclusters[0] == [('a',), ('b',)]
        assert subclusters[1] == [('a', 'b'), ('c',)]
    def test_two_nested_parts(self):
        clusters, subclustdists = list(zip(*clusters_from_newick('((a,b),(c,d))', include_full=True)))
        subclusters = [[item[0] for item in subclustdist] for subclustdist in subclustdists]
        assert subclusters[0] == [('a',), ('b',)]
        assert subclusters[1] == [('c',), ('d',)]
        assert subclusters[2] == [('a', 'b'), ('c','d')]
    def test_depth_2(self):
        clusters, subclustdists = list(zip(*clusters_from_newick('(((a,b),c),d)', include_full=True)))
        subclusters = [[item[0] for item in subclustdist] for subclustdist in subclustdists]
        assert subclusters[0] == [('a',), ('b',)]
        assert subclusters[1] == [('a', 'b'), ('c',)]
        assert subclusters[2] == [('a', 'b', 'c'), ('d',)]


class Test_list_clusters_notfull:
    """Do not yield the root level (full)"""
    def test_one_nested_part(self):
        clusters, subclustdists = list(zip(*clusters_from_newick('((a,b),c)', include_full=False)))
        subclusters = [[item[0] for item in subclustdist] for subclustdist in subclustdists]
        print('clusters =', clusters)
        assert len(clusters) == 1
        assert clusters[0] == ('a', 'b')
        assert len(subclusters) == 1

    def test_depth_2(self):
        clusters, subclustdists = list(zip(*clusters_from_newick('(((a,b),c),d)', include_full=False)))
        subclusters = [[item[0] for item in subclustdist] for subclustdist in subclustdists]
        assert len(clusters) == 2
        assert clusters[0] == ('a', 'b')
        assert clusters[1] == ('a', 'b', 'c')


class Test_IterNewickClustersPartial:
    @pytest.mark.parametrize('englobing_parentheses', [True, False])
    def test_remove_one_inner_leaf(self, englobing_parentheses):
        newick = '(((a,b),c),d),e;'
        if englobing_parentheses:
            newick = '(' + newick.rstrip(';') + ');'
        iterclusters = IterNewickClustersPartial(newick, drop_ids=set('a'))
        clusters, subclustdists = list(zip(*iterclusters))
        print(clusters)
        print(subclustdists)
        assert set(clusters) == set([('b','c'), ('b','c','d')])
        rootclust, rootlevel = iterclusters.root
        assert rootclust == tuple('bcde')
        if not englobing_parentheses:
            assert iterclusters.has_full_clust is False

    @pytest.mark.parametrize('englobing_parentheses', [True, False])
    def test_remove_one_basal_leaf(self, englobing_parentheses):
        newick = '(((a,b),c),d),e;'
        if englobing_parentheses:
            newick = '(' + newick.rstrip(';') + ');'
        iterclusters = IterNewickClustersPartial(newick, drop_ids=set('e'))
        clusters, subclustdists = list(zip(*iterclusters))
        print(clusters)
        print(subclustdists)
        assert set(clusters) == set([('a','b'), ('a', 'b','c')])
        rootclust, rootlevel = iterclusters.root
        assert rootclust == tuple('abcd')
        assert iterclusters.has_full_clust is True

    @pytest.mark.parametrize('englobing_parentheses', [True, False])
    def test_remove_two_distant_leaves(self, englobing_parentheses):
        newick = '(((a,b),c),(d,e)),f;'
        if englobing_parentheses:
            newick = '(' + newick.rstrip(';') + ');'
        iterclusters = IterNewickClustersPartial(newick, drop_ids=set('ae'))
        clusters, subclustdists = list(zip(*iterclusters))
        print(clusters)
        print(subclustdists)
        assert set(clusters) == set([('b','c'), ('b','c','d')])
        rootclust, rootlevel = iterclusters.root
        assert rootclust == tuple('bcdf')

    def test_propagate_branchlength(self):
        newick = '((((a:1,b:2):3,c:6):4,d:6):0.5,e:7);'
        iterclusters = IterNewickClustersPartial(newick, drop_ids=set('a'))
        clusters, subclustdists = list(zip(*iterclusters))
        print(clusters)
        print(subclustdists)
        subsplit0 = subclustdists[0]
        subclust0, dist0 = subsplit0[0]
        assert subclust0 == ('b',)
        assert dist0 == 2+3
        # Check the subcluster composition
        subclust1, _ = subsplit0[1]
        assert subclust1 == ('c',)

    def test_nothing_retained(self):
        newick = '((((a,b),c),d),e);'
        iterclusters = IterNewickClustersPartial(newick, drop_ids=set('abcde'))
        iterated = list(iterclusters)
        print(iterated)
        assert not iterated
