#!/usr/bin/env python3

import pytest
from grec.seq2clust import *
from ete3 import Tree


def default_get_sp(nodename):
    return nodename.split('_')[0]


class Test_read_sp_cluster_hierarchy:
    def setup_method(self):
        self.lines_direct = ['A	sp1	sp2\n', 'B	sp3	sp4\n']
        self.lines_nested = ['AB=	A	B\n']
        self.lines_mixed = ['C	*A	sp5\n']
        self.lines_undefined = ['X=	y']
        self.lines_undefined_mixed = ['X	*y']
    def test_direct_clusters(self):
        hierarchy = dict(read_sp_cluster_hierarchy(self.lines_direct))
        assert set(hierarchy.keys()) == set(('A', 'B'))
        assert set(hierarchy['A']) == set(('sp1', 'sp2'))
        assert set(hierarchy['B']) == set(('sp3', 'sp4'))
    def test_nested_cluster(self):
        hierarchy = dict(read_sp_cluster_hierarchy(self.lines_direct + self.lines_nested))
        assert set(hierarchy.keys()) == set(('A', 'B', 'AB='))
        assert set(hierarchy['AB=']) == set(('A', 'B'))
    def test_nested_cluster_mixed(self):
        hierarchy = dict(read_sp_cluster_hierarchy(self.lines_direct + self.lines_mixed))
        assert set(hierarchy.keys()) == set(('A', 'B', 'C'))
        assert set(hierarchy['C']) == set(('*A', 'sp5'))


class Test_read_sp_clusters:
    def setup_method(self):
        self.lines_direct = ['A	sp1	sp2\n', 'B	sp3	sp4\n']
        self.lines_nested = ['AB=	A	B\n']
        self.lines_mixed = ['C	*A	sp5\n']
        self.lines_undefined = ['X=	y']
        self.lines_undefined_mixed = ['X	*y']
    def test_direct_clusters(self):
        clusters, species_clusters = read_sp_clusters(self.lines_direct)
        assert set(clusters.keys()) == set(('A', 'B'))
        assert clusters['A'] == set(('sp1', 'sp2'))
        assert clusters['B'] == set(('sp3', 'sp4'))
        assert species_clusters['sp1'] == ['A']
        assert species_clusters['sp2'] == ['A']
        assert species_clusters['sp3'] == ['B']
        assert species_clusters['sp4'] == ['B']
    def test_nested_cluster(self):
        clusters, species_clusters = read_sp_clusters(self.lines_direct + self.lines_nested)
        assert set(clusters.keys()) == set(('A', 'B', 'AB='))
        assert clusters['AB='] == set(('sp1', 'sp2', 'sp3', 'sp4'))
        assert species_clusters['sp1'] == ['A', 'AB=']
        assert species_clusters['sp2'] == ['A', 'AB=']
    def test_nested_cluster_mixed(self):
        clusters, species_clusters = read_sp_clusters(self.lines_direct + self.lines_mixed)
        assert set(clusters.keys()) == set(('A', 'B', 'C'))
        assert clusters['C'] == set(('sp1', 'sp2', 'sp5'))
        assert species_clusters['sp1'] == ['A', 'C']
        assert species_clusters['sp2'] == ['A', 'C']
        assert species_clusters['sp5'] == ['C']
    def test_undefined_reference_raises_KeyError(self):
        with pytest.raises(KeyError):
            _ = read_sp_clusters(self.lines_undefined)
        with pytest.raises(KeyError):
            _ = read_sp_clusters(self.lines_undefined_mixed)



all_species_tags =     {'A': 'species_A_tag', 'B': 'species_B_tag'}
species_tags_default = {'A': 'species_A_tag', '*': 'species_B_tag'}
species_tags_missing = {'A': 'species_A_tag'}


class Test_apply_leaf_features():

    def setup_method(self):
        self.tree = Tree('((A_g1, A_g2)x, B_g1)r;', format=1)

    @pytest.mark.parametrize('mytag', [all_species_tags, species_tags_default])
    def test_tag_A_and_B(self, mytag):

        tree = self.tree
        apply_leaf_features(tree, default_get_sp, mytag=mytag)

        assert 'mytag' in (tree&'A_g1').features
        assert 'mytag' in (tree&'A_g2').features
        assert 'mytag' in (tree&'B_g1').features
        assert (tree&'A_g1').mytag == 'species_A_tag'
        assert (tree&'A_g2').mytag == 'species_A_tag'
        assert (tree&'B_g1').mytag == 'species_B_tag'

    def test_missing_tag_without_default_must_fail(self):
        tree = self.tree
        with pytest.raises(KeyError):
            apply_leaf_features(tree, default_get_sp, mytag=species_tags_missing)
