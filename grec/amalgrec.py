#!/usr/bin/env python3

# (C) Copyright Guillaume Louvel 2023
#
# This file is licensed under the GNU General Public License GPL version 3


"""
Reconciliation of amalgamated gene trees
(trees that can be built from a sample of tripartitions).

Similar to TERA (Scornavacca et al. 2015) and ecceTERA (Jacox et al. 2016).

This aims to extend the algorithm to allow polytomic species trees.


This only contains the recursion function, built with the AmalgDLTCombiner class.

The Amalgam class to store the tripartitions is defined in myriphy.CCP.


Basic usage:

    >>> amalgam = myriphy.CCP.build_amalgam(treefiles)
    >>> combiner = AmalgDLTCombiner(amalgam, species_tree, get_species_func)
    >>> amalgam.walk(combiner)  # recurse over all tripartitions (postorder)


Recursion:

At each tripartition, the array of scores is computed using the children
tripartitions scores.

An array of score S contains the (parsimony) score associated to each ancestor.

At polytomic ancestors, an array of partial scores associates each polytomy
resolution to a score that ignores basal losses (because such losses can be
cancelled in a parent polytomy resolution).

"""


from itertools import combinations, product
from collections import defaultdict
from queue import deque
import numpy as np
import logging
logger = logging.getLogger(__name__)


def flatten(nested_set):
    flat = ()
    unflat=list(nested_set)
    while unflat:
        x = unflat.pop(0)
        if isinstance(x, int):
            flat += (x,)
        elif x is not None:
            unflat = list(x) + unflat
    return flat


def flatten_2d(setgroups):
    """Create a collection of sets from a collection of setgroups
    (flatten the inner level).

    example:  ((a,b),(c,d)), ((e,f),(g,)) -> (a,b,c,d), (e,f,g) 

    input config: tuple of tuples of tuples
    output:       tuple of tuples
    """
    flatgroups = []
    for unflat in setgroups:
        flatgroups.append(None if unflat is None else flatten(unflat))
    return tuple(flatgroups)


def multi_min(values, *_, key=None):  #, ignore_nan=True):
    """Return the lowest key value, and all minimal values"""
    minima = []
    values = iter(values)
    if key is None:
        def key(e): return e
    try:
        minima = [next(values)]
        best = key(minima[0])
    except StopIteration:
        raise ValueError('Empty sequence')
    try:
        while np.isnan(best):
            try:
                minima = [next(values)]
                best = key(minima[0])
            except StopIteration:
                raise ValueError('All NaNs')
    except TypeError as err:
        err.args = (err.args[0] + ' using best=%r' % (best,),)
        raise

    for elem in values:
        key_elem = key(elem)
        if key_elem < best:
            best = key_elem
            minima = [elem]
        elif key_elem == best:
            minima.append(elem)
    return best, minima


class AmalgDLTCombiner:
    def __init__(self, species_tree, amalgam, get_sp, ancestors=None, time_compatible=None, dup=1, loss=0.5, transfer=3, freq=1):
        self.set_weights(dup, loss, transfer, freq)
        self.max_ancsplit_configs = 2000

        #amalgam.verify_freqs()  # complexifies unit tests
        self.amalgam = amalgam
        self.get_sp = get_sp
        self.species_tree = species_tree
        if ancestors is None:
            ancestors = [n.name for n in species_tree.traverse()]
        if len(ancestors) > len(set(ancestors)):
            raise ValueError('Species tree nodes must have a unique label')
        self.ancestors = ancestors

        # Cache all tree node relations (indexed nodes)
        self.n_anc = len(ancestors)
        self.anc_range = range(self.n_anc)
        self.anc_index = {anc: i for i,anc in enumerate(ancestors)}
        self.root_id = self.anc_index[species_tree.name]
        self.anc_nodes = {}
        self.children = {}
        self.parents = {}
        self.descendants = {}
        self.pairwise_mrca = {}
        self.polytomies = set()
        for n in species_tree.traverse('postorder'):
            anc_i = self.anc_index[n.name]
            self.anc_nodes[n.name] = n
            self.pairwise_mrca[anc_i, anc_i] = anc_i
            if n.is_leaf():
                self.descendants[anc_i] = frozenset((anc_i,))
                self.children[anc_i] = []
            else:
                children_i = self.children[anc_i] = [self.anc_index[c.name] for c in n.children]
                self.descendants[anc_i] = frozenset((anc_i,)).union(*(self.descendants[c_i] for c_i in children_i))
                for children_pair in combinations(children_i, 2):
                    for desc1, desc2 in product(*(self.descendants[c_i] for c_i in children_pair)):
                        self.pairwise_mrca[anc_i, desc1] = self.pairwise_mrca[desc1, anc_i] = anc_i
                        self.pairwise_mrca[anc_i, desc2] = self.pairwise_mrca[desc2, anc_i] = anc_i
                        self.pairwise_mrca[desc1, desc2] = self.pairwise_mrca[desc2, desc1] = anc_i
                if len(children_i) > 2:
                    self.polytomies.add(anc_i)
            self.parents[anc_i] = [self.anc_index[p.name] for p in n.iter_ancestors()]
        logger.info('Loaded species tree relationships')

        if time_compatible is None:
            time_compatible = {anc_i: set(self.anc_range).difference(self.parents[anc_i]) for anc_i in self.anc_range}
        else:
            # Type check:
            for a, t_compat_a in time_compatible.items():
                if not isinstance(a, int) or not all(isinstance(t, int) for t in t_compat_a):
                    raise ValueError('time_compatible type must be: dict( int : iterable of int )')
        self.time_compat = time_compatible
        #TODO: horiz_time_compat (for faster iter_configs)
        self.horiz_time_compat = {a: t_compat_a.difference(self.descendants[a])
                                  for a, t_compat_a in time_compatible.items()}

        self._event_cost = {}


    def mrca(self, taxalist):
        """Most Recent Common Ancestor of a set of taxa"""
        taxa = list(set(taxalist))
        try:
            mrca = self.pairwise_mrca[taxa[0], taxa[1]]
        except IndexError:
            # Only one taxon was given, or zero
            return taxa[0]
        for taxon in taxa[2:]:
            mrca = self.pairwise_mrca[mrca, taxon]
        return mrca

    def set_weights(self, dup=1, loss=0.5, transfer=3, freq=1):
        if dup < 0 or loss < 0 or transfer < 0 or freq < 0:
            raise ValueError('dup,loss,transfer,freq must be positive')
        self.dup, self.loss, self.transfer, self.freq = dup, loss, transfer, freq
        self.weights = np.array([dup, loss, transfer])

    def fill_missing_partials_adding_losses(self, a, ch, y):
        features = self.amalgam.features
        descendants = sorted(self.descendants[ch])
        desc_scores = features[y]['score'][descendants]
        if np.isfinite(desc_scores).any():
            desc_losses = np.array([0 if ch==d else self.parents[d].index(ch)+1
                                    for d in descendants])  #TODO: Optimize
            best = np.nanargmin(desc_scores + desc_losses * self.loss) #FIXME: multiple minima
            best_desc = descendants[best]
            subscore = desc_scores[best] + desc_losses[best] * self.loss
            subcount = features[y]['count'][best_desc] + np.array([0,1,0,0]) * desc_losses[best]
            #assert not np.isnan(subcount).any(), "NaN counts at descendant %d (alternative descendants = %s)" % (best_desc, (desc_scores+desc_losses*self.loss == subscore) )
            if np.isnan(subcount).any():
                logger.error("NaN counts at descendant %d (alternative descendants = %s)", best_desc, (desc_scores+desc_losses*self.loss == subscore))
            return best_desc, subscore, subcount
        return None, None, None

    def iter_configs(self, a, subsplit, clust, maxdepth=6):
        """Given an ancestor 'a' mapped to a gene cluster (~gene tree node),
        and a descendant subsplit, list all possible combinations of descendant
        configurations of taxa."""
        if len(subsplit) != 2:
            raise NotImplementedError('Non dichotomic subsplit')
        #subsplit = tuple(subsplit)  # Make it explicit that we iterate in a constant order
        #subsplit = sorted(subsplit)
        features = self.amalgam.features
        subconfigs = [[] for _ in subsplit]
        subresolvers = [[] for _ in subsplit]
        if a in self.polytomies:
            a_is_poly = True
            for b, y in enumerate(subsplit):
                for subresolver, subscore in features[y]['partial'][a].items():
                    subresolvers[b].append(subresolver + (subscore, features[y]['partial_count'][a][subresolver]))
                for ch in self.children[a]:
                    best_desc, subscore, subcount = self.fill_missing_partials_adding_losses(a, ch, y)
                    if subscore is not None:
                        subconfigs[b].append(((ch,), None, subscore, subcount))  # or include (best_desc,) ?
                        if ((ch,), None) in features[y]['partial'][a]:
                            raise RuntimeError('Config ((%s,),None) for %r already in partial at anc %s! seen count %s VS new %s' % (ch, y, a, features[y]['partial_count'][a][(ch,),None], subcount))
                #logger.debug('subconfig (%r) = %s + %s', y, subconfigs[b], subresolvers[b])
        else:
            a_is_poly = False
            for b, y in enumerate(subsplit):
                for d in self.descendants[a]:
                    subscore = features[y]['score'][d]
                    if np.isfinite(subscore):
                        subconfigs[b].append((d, None, subscore, features[y]['count'][d]))
                #logger.debug('subconfig (%r) = %s + %s', y, subconfigs[b], subresolvers[b])

        #prod_size = len(subconfigs[0]) + len(subresolvers[0])
        #msg = '(%d+%d)' % (len(subconfigs[0]), len(subresolvers[0]))
        #for i in range(1, len(subconfigs)):
        #    prod_size *= (len(subconfigs[i]) + len(subresolvers[i]))
        #    msg += '*(%d+%d)' % (len(subconfigs[i]), len(subresolvers[i]))
        #if prod_size:
        #    logger.info('a=%s %s split=%r ' + msg + ' = %d', a, self.ancestors[a], subsplit, prod_size)

        for (v1, t1, s1, c1), (v2, t2, s2, c2) in product(*(subconf+subres for subconf, subres in zip(subconfigs, subresolvers))):
            if ((a_is_poly and len(v1) == 1 and v1 == v2)  # This is a duplication of the poly child (redundant)
                or (not a_is_poly and v1 == v2 != a)): # Always suboptimal Dup + parallel losses
                continue
            yield (v1, v2), (t1, t2), s1+s2, c1+c2
        if a_is_poly:
            # the only interesting configs to join with one transfer are partial resolutions
            subconfigs = [[(v,t,s,c) for v,t,s,c in subres if len(v)>1]
                          for subres in subresolvers]
        for h in self.horiz_time_compat[a]:
        #TODO: to allow transfers between resolved subnodes of a same polytomy,
        # I should use horiz_time_compat[a] - set(v1) (respectively,v2)
            if h in self.polytomies:
                # if a transfer arrives in a polytomy: take partial score!
                # Ignoring all the losses is too permissive. Score half of them.
                # (Ideally we should score the expected number of losses. TODO)
                #h_losses = [len(features[y]['direct_losses'][h]) for y in subsplit]  # Not currently stored.
                # Find the minimum partial score, and resolutions of ti...
                h_scores = [(features[y]['best_partial'][h] + features[y]['score'][h])/2.
                            for y in subsplit]
                h_counts = [(features[y]['best_partial_count'][h] + features[y]['count'][h])/2.
                            for y in subsplit]
            else:
                h_scores = [features[y]['score'][h] for y in subsplit]
                h_counts = [features[y]['count'][h] for y in subsplit]
            for v1, t1, s1, c1 in subconfigs[0]:
                if np.isfinite(h_scores[1]):
                    yield (v1, None), (t1, h), s1 + h_scores[1], c1 + h_counts[1]
            for v2, t2, s2, c2 in subconfigs[1]:
                if np.isfinite(h_scores[0]):
                    yield (None, v2), (h, t2), s2 + h_scores[0], c2 + h_counts[0]
        #if not n_configs and len(clust)>1:
        #    logger.warning('  Zero config of children taxa! at subsplit %s', subsplit) #FIXME: is this a problem?
        return


    def direct_event_count(self, a, vertical: tuple, transfered: tuple):  # tuple or list
        """Cost of a gene tree dichotomy from parent 'a' to children 'config'.
        Missing children of a polytomy are not penalized.

        return D'(a,config), direct losses

        details:
        using d1: direct children of 'a' with descendants c1

        D'(a,c1) = D'(a,d1) + L(d0,c0)
        """
        try:
            return self._event_cost[(a,vertical,transfered)]
        except KeyError:
            pass  # compute new value, see below.

        count = np.zeros(3, dtype=int)  # dup, loss, transfer, freq
        direct_losses = []

        ntransfers = sum(vi is None for vi in vertical)
        if ntransfers >= 2:
            raise ValueError('double transfers are not considered (%s, %s)' % (a, transfered))
        #TODO: consider this situation: transfer to mrca(transfered), and no
        # vertical descendants. This does not appear necessary, given that time
        # constraints are very rough, because there is a less costly
        # alternative of transferring from the parent split directly, avoiding
        # counting the losses to a:
        #
        # 1. costly:                      2. fewer losses
        #
        # i  j      LOST: X  X            i  j
        #  \/              \/              \/
        # mrca(i,j) <----- a  sister(a)   mrca(i,j)     LOST:a   sister(a)
        #                   \/                    <---        \/
        #                   parent(a)                 `------parent(a)

        elif ntransfers:
            count[2] += ntransfers  # as trees are dichotomic, ntransfers=1 here.

        vertical_2d = flatten_2d(vertical) if a in self.polytomies else tuple((k,) for k in vertical if k is not None)
        direct_vertical = []
        for set_i in vertical_2d:
            if set_i is None:
                continue
            direct_set_i = []
            for child_taxon in set_i:
                while child_taxon not in [a] + self.children[a]:
                    child_taxon = self.parents[child_taxon][0]
                    #count[1] += len(self.children[child_taxon]) - 1
                    # Actually: if it's a polytomy, there exist one resolution that implies a single loss
                    count[1] += 1
                direct_set_i.append(child_taxon)
            direct_vertical.append(tuple(direct_set_i))

        if ntransfers:
            # In principle, len(direct_vertical) == 1 in this case
            assert len(direct_vertical) == 1, 'len(direct_vertical) + ntransfers = %d + %d != 2' % (len(direct_vertical), ntransfers)
            d1, = direct_vertical
            if a in self.polytomies:
                direct_losses += [ch for ch in self.children[a] if ch not in d1]  # include?
                logger.debug('    parent of transfer: %s, polytomy' % a)
            elif (a,) != d1:
                count[1] += 1
                logger.debug('    parent of transfer: %s, dichotomy with children %s', a, d1)
            return count, direct_losses

        try:
            d1, d2 = direct_vertical
        except ValueError as err:
            if 'values to unpack (expected 2' in err.args[0]:
                raise ValueError('Expect two vertical child taxa but not: %s' % direct_vertical)
            raise

        union = set(d1).union(d2)
        mrca1, mrca2 = self.mrca(d1), self.mrca(d2)

        # Polytomy VS Dichotomic speciation must be treated differently,
        # because the subconfig d1 or d2 can never contain a at polytomies.
        if a in self.polytomies:
            if set(d1) & set(d2):
                #logger.debug('Duplication in polytomy')
                # This dichotomy implies a duplication.
                # NOTE: no need to count duplications occuring deeper (if d1 or d2
                # contain nested subconfigs) because this has already been counted in partial score
                count[0] += 1
                # count lost taxa on each side, relative to the union:
                #count[1] += len(union.difference(d1)) + len(union.difference(d2))
                count[1] += bool(union.difference(d1)) + bool(union.difference(d2))   # more precise alternative: the average loss count of all possible resolutions...
                # single dup event = the children taxa equal the parent taxon.
                # otherwise, it implies missing speciation nodes (losses)
                # - dichotomic species node a: (a,) == d1 == d2
                # - polytomic: the dup occurs in an unobserved taxon that resolves the polytomy.
                #              the children map to that taxon.
                #              we can't tell if there are intermediate speciations, and losses. no cost.
                # the dup-loss case: a is older than d1 or d2.
                # - dichotomic: add the direct_losses
                # - polytomic: a is older than mrca(d1) or mrca(d2): we add the losses between a and mrcas
            #else:
                #logger.debug('Speciation in polytomy')
            # In any case, dup or spe,
            # count the taxa lost prior to the gene divergence (i.e. missing from union)
            direct_losses += list(set(self.children[a]) - union)
        else:
            # At a dichotomic speciation, d1 and d2 are singletons, thus d1==mrca1
            if a in union or mrca1 == mrca2:
                # Duplication
                #logger.debug('Duplication in dichotomy')
                count[0] += 1
                # Missing intermediate speciations (only if dup, because otherwise already counted):
                # We add to the count, because we don't need to delay 'direct losses' for later
                if mrca1 == mrca2 != a:
                #TODO: evaluate if we can return NaN in this case (check that no HGT can make this more optimal)
                # In this situation, the HGT would occur on one side, in taxon a. Thus we catch it at the child subsplit, mapped to a.
                    return np.full(3, np.NaN), direct_losses
                if mrca1 != a:
                    count[1] += bool(set(self.children[a]).difference(d1))
                if mrca2 != a:
                    count[1] += bool(set(self.children[a]).difference(d2))
        #TODO: determine if useful to cache results: self._event_cost[a,vertical,transfered] = count, direct_losses
        return count, direct_losses


    def __call__(self, subsplits: list, clust):
        """Recursion formula to score cluster x from its direct subsplits
    
        Let:
        - x: a cluster of genes
        - b: a split of x (bipartition)
        - splits(x): all splits of x

        - R(x): the reconciliation of the gene node represented by x, 
                i.e. the mapping of x to an ancestral taxon k.
        - {R(x) = k} the event where x is mapped to taxon k
        - Sx(k) := S(R(x) = k) := S({R(x) = k}) the corresponding score. Value to minimize.
        
        - Ŝ(R(x)) = min_k Sx(k)   
        - ^R(x) = argmin_k Sx(k) the optimal taxon to reconcile x with.

        - c: a list of taxa mapped by b (config).
             They are time-compatible with (more recent than) R(x)
        - configs(b,k): the set of mappings c to split b, time-compatible with k, with a special tag on transfered taxa.

        - we extend the notation R() to a set of clusters and a set of taxa:
          { R(b) = c } := Intersection_i { R(b[i]) = c[i] }
          and thus:
          Sb(c) := S(R(b) = c) = Sum_i S(R(b[i]) = c[i])

        - D(k,c) : the cost of the evolutionary event where x is inherited
          from taxon k to taxa c in one split. This includes:
            + speciation,
            + speciation and losses,
            + duplication,
            + duplication and losses,
            + transfer

        The recursion formula relates {R(x)=k} to {R(b)=c} by minimizing over
        splits and child taxa configs:

            Sx(k) = min_{b in splits(x)} [ min_{c in configs(b,k)} Sb(c) + D(k,c) ]

        However, k being polytomic requires an adaptation;
        each hypothetical config c that also contains k can represent a possible
        polytomy resolution. In this case, we ignore some losses.
        For each config c containing k, we further iterate over the inner configs:
        * p := configs(b[0], k) scored with Sb[0](k) = min_{bb in splits(b[0]} S'bb(subc0))
        * q := configs(b[1], k) scored with Sb[1](k) _

        where min_{bb in splits(b[0])} S'bb(subc0) is the partial score (crown
        of polytomy, no losses counted).

        To rewrite the recursion without nesting more terms, we will
        just redefine the configs as representing all possible polytomy resolutions

        c' : list of taxa mapped by b and its children resolutions.

        A nested recursion is therefore done at polytomies, to score each c'

        Let
        - c0(y,z) := c := (p0, q0)_{y,z} , the initial pair of possible child taxa.
        - c1[i] := (c0(split(y)), c0(split(z)))_{y,z,split(y),split(z)}
                   where c0(split(y)) := (p1,q1) is a further resolution of p0 if p0==k
                   The indexing [i] emphasizes that this is one of the possible resolutions (p1, q1) of p0=k
        - d0 := the direct children of k having p0,q0 as descendants
        - d1 := by recursion, preserving the nesting parentheses:
                for example, if p0==k, d1 := ((p1,q1), q0)
        - S'x,y,z(k,i,j) : the score, but omitting losses of direct children
        - D'(k,i,j) : the cost, omitting lost direct children
        - L(u,v) : the number of losses along the single path from taxon u to v

        Then, if p0==k:

            S'x,y,z(k, c1[i]) = D'(k,d1) + L(d0,c0) + S'y,split(y)(k,p1,q1) + S'z(p0)
            ^                                         |
            |________________recurse__________________|

        using dynamic programming, we store previous values of

            S'y,split(y)(k,p1,q1) = minimum for all splittings of y.
        """
        #TODO: if len(clust) == 1: initialize
        amalgam = self.amalgam
        features = amalgam.features
        n_subsplits = len(subsplits)

        # returned dict; then walk: features['clust'].update(estimated)
        clustfeatures = {}
        nancount = np.full(4, np.NaN)
        if len(clust) == 1:
            # Leaf cluster: initialize features and quit
            clustfeatures['species'] = species = self.get_sp(amalgam.labels[list(clust)[0]])
            clustfeatures['species_id'] = species_id = self.anc_index[species]
            assert n_subsplits <= 1, "Requirement for Amalgam to suit initialization"
            clustfeatures['score'] = np.full(self.n_anc, np.NaN)
            clustfeatures['score'][species_id] = 0  #TODO: double-check if I need to score ancestors with loss penalty (should not)
            clustfeatures['count'] = np.full((self.n_anc, 4), np.NaN)
            clustfeatures['count'][species_id] = 0
            # Does not make sense, but is convenient for the recursion.
            clustfeatures['partial'] = {a: {} for a in self.polytomies}
            clustfeatures['partial_count'] =  {a: {} for a in self.polytomies}
            #for nloss, anc_id in enumerate(self.parents[species_id][:-1], start=1):
            #    #NOTE: this initialization is required for iter_configs
            #    #      we skip species_id in the enumeration because it is already in 'score'
            #    parent = self.parents[anc_id][0]
            #    if parent in self.polytomies:
            #        clustfeatures['partial'][parent][((anc_id,),None)] = nloss
            #        clustfeatures['partial_count'][parent][((anc_id,),None)] = nloss * np.array([0,1,0,0])
            clustfeatures['best_partial'] = {a: np.NaN for a in self.polytomies}
            clustfeatures['best_partial_count'] = {a: nancount for a in self.polytomies}
            #clustfeatures['n_best'] = {a: 0 for a in self.anc_range}  # Number of optimal subtrees (sum over configs, subsplits)
            #clustfeatures['n_best'][species_id] = 1
            clustfeatures['n_best'] = {a: 1 for a in [species_id] + self.parents[species_id]}  # Number of optimal subtrees (sum over configs, subsplits)
            #clustfeatures['n_best'] = {species_id: 1}
            return clustfeatures
        #elif clust == amalgam.full:
        #    # At the root cluster, skip partial scorings (not needed):
        #    backup_polytomies = self.polytomies
        #    self.polytomies = set()
        score = np.full((self.n_anc, n_subsplits), np.NaN)
        counts = {}
        best_configs = {}

        tmp_poly_partial  = {a: {}    for a in self.polytomies}  # partial scores at the crown of a polytomy
                      # (ignoring the loss of polytomic children)
        tmp_poly_counts       = {a: {}    for a in self.polytomies}
        poly_child_losses = {a: {}    for a in self.polytomies}
        poly_configs      = {a: set() for a in self.polytomies}  # We use it at the end to minimize partial scores over all subsplits

        for (a, anc), (b,subsplit) in product(enumerate(self.ancestors), enumerate(subsplits)):
            score[a,b], counts[a,b], best_configs[a,b], ancsplit_partial, \
            ancsplit_count, ancsplit_direct_losses, ancsplit_configs \
                = self.ancsplit_score(a, subsplit, clust)

            if a in self.polytomies:
                # Store temporary variables at polytomies
                tmp_poly_partial[a][subsplit] = ancsplit_partial
                tmp_poly_counts[a][subsplit] = ancsplit_count
                poly_child_losses[a][subsplit] = ancsplit_direct_losses
                poly_configs[a] |= ancsplit_configs

        # Cleanup partial scores:
        # At dichotomy, we don't need them;
        # At polytomy, we can take the minimum value of all subsplits, it is optimal
        clustfeatures['n_best'] = {}
        poly_partial = {}
        poly_counts = {}
        poly_splits = {}  # optimal subsplits for each (anc, config)
        poly_best_partial = {}
        poly_best_count = {}
        for a in self.polytomies:
            poly_partial[a] = {}
            poly_counts[a] = {}
            poly_splits[a] = {}
            min_partial = np.NaN
            best_partial_counts = []
            # 1. Minimize over subsplits
            for config in poly_configs[a]:
                #TODO: idea to discard some of the configs:
                # Estimate the difference between the stem config VS the crown (partial) config:
                # Discard if the partial score is much higher than the best stem score
                ## Alternatively, keeping the best partial to compute the best score might work
                poly_partial[a][config], best_poly_splits_a = multi_min(range(len(subsplits)),
                                                    key=lambda b: tmp_poly_partial[a][subsplits[b]].get(config,np.NaN))
                poly_splits[a][config] = best_poly_splits_a
                logger.debug('Aggregate partial at poly %s clust %r config %s: best subsplit score: %s; best subsplits: %s ; partial scores: %s',
                             a, clust, config, poly_partial[a][config],
                             best_poly_splits_a,
                             [tmp_poly_partial[a][s].get(config, np.NaN)
                              for s in subsplits])
                best_poly_counts_a = [tmp_poly_counts[a][subsplits[b]][config]
                                      for b in best_poly_splits_a
                                      if config in tmp_poly_counts[a][subsplits[b]]]
                clustfeatures['n_best'][config[0]] = len(best_poly_splits_a) #FIXME
                #clustfeatures['n_best'][config[0]] = 0
                #for b in best_poly_splits_a:
                #    for sub, vi, ti in zip(amalgam.subsplits[clust][b], *config):
                #        ci = ti if vi is None else vi if isinstance(vi, int) or len(vi)>1 else vi[0]
                #        clustfeatures['n_best'][config[0]] += features[sub]['n_best'][ci]
                poly_counts[a][config] = np.nanmean(best_poly_counts_a, axis=0) if best_poly_counts_a else nancount
                #if best_poly_counts_a:
                #    logger.info('best_poly_splits_a=%s has valid configs with counts: %s', best_poly_splits_a, best_poly_counts_a)
                #else:
                if not best_poly_counts_a:
                    logger.error('NaN partial counts at a=%d; config=%s; best_poly_splits_a=%s', a, config, best_poly_splits_a)
                    continue

                if np.isnan(min_partial) or poly_partial[a][config] < min_partial:
                    min_partial = poly_partial[a][config]
                    best_partial_counts = list(best_poly_counts_a)
                elif min_partial == poly_partial[a][config]:
                    best_partial_counts += best_poly_counts_a
            poly_best_partial[a] = min_partial
            if best_partial_counts:
                poly_best_count[a] = np.nanmean(best_partial_counts, axis=0)
            else:
            #    logger.warning('No valid partial counts obtained at polytomy %s', a)
                poly_best_count[a] = np.full(4, np.NaN)

        clustfeatures['partial'] = poly_partial
        clustfeatures['partial_count'] = poly_counts
        clustfeatures['best_partial'] = poly_best_partial
        clustfeatures['best_partial_count'] = poly_best_count
        clustfeatures['resolver_best_subsplit'] = poly_splits
        clustfeatures['tmp_score'] = score # debugging
        clustfeatures['score'] = best_score = np.nanmin(score, axis=1)  # minimum over subsplits
        #TODO: I have not verified that minimizing now always leads to the global minimum
        clustfeatures['best_split_config'] = {}
        clustfeatures['count'] = np.full((self.n_anc, 4), np.NaN)
        for a in self.anc_range:
            best_subsplit_a = np.flatnonzero(score[a]==best_score[a])
            clustfeatures['best_split_config'][a] = []
            child_n_best = []
            best_counts_a = []
            for b in best_subsplit_a:
                clustfeatures['best_split_config'][a].extend((b, config) for config in best_configs[a,b])
                if not np.isnan(counts[a,b]).all():
                    best_counts_a.append(counts[a,b])
                    child_n_best.append(0)
                    for vertic,transfered in best_configs[a,b]:
                        config_n_best = 1
                        #actualvertic = ()
                        for y,v,t in zip(amalgam.subsplits[clust][b], vertic, transfered):
                            try:
                                config_n_best *= features[y]['n_best'][t if v is None else v if (isinstance(v,int) or len(v)>1) else v[0]]
                                #actualvertic += (v,)
                            
                            except KeyError as err:
                                # Unittest 'poly_spe_indirectchild_notleaf' reaches this code
                                if isinstance(v, tuple) and len(v)==1:
                                    desc = self.descendants[v[0]].intersection(features[y]['n_best'])
                                    if desc:
                                        closest = min(desc, key=lambda d: self.parents[d].index(v[0]))
                                        config_n_best *= features[y]['n_best'][closest]
                                        #actualvertic += (closest,)
                                        assert not features[y]['best_split_config'][v[0]], "%r -> %s (from %r -> %s): %s replaced by %s" % (y,v[0],clust,a,features[y]['best_split_config'][v[0]], features[y]['best_split_config'][closest])  
                                        # This complement is required in backtrace
                                        features[y]['best_split_config'][v[0]] = features[y]['best_split_config'][closest]
                                        features[y]['n_best'][v[0]] = features[y]['n_best'][closest]
                                else:
                                    err.args = ('%s at %r n_best(y)=%s a=%s clust %r best_configs %s' % (err.args[0], y, ', '.join('%s:%d' % (ci,k) for ci,k in features[y]['n_best'].items() if k), a, clust, best_configs[a,b]),)
                                    raise
                        child_n_best[-1] += config_n_best
                        #clustfeatures['best_split_config'][a].append((b, (actualvertic, transfered)))
                    assert child_n_best[-1] != 0, 'averaged counts of %s weighted %s ; best_split_config=%s' % (best_counts_a, child_n_best, clustfeatures['best_split_config'][a])

            if best_counts_a:
                logger.debug('clust %r a=%s averaged counts of %s weighted %s ; best_split_config=%s', clust, a, best_counts_a, child_n_best, clustfeatures['best_split_config'][a])
                # weight possibilities by the number of descendant best_counts?
                clustfeatures['count'][a] = np.average(best_counts_a, axis=0, weights=child_n_best)
                clustfeatures['n_best'][a] = sum(child_n_best)
                # Complement with the ancestors of a?
            #else:
            #    logger.warning('NaN counts at %r -> %s ancestor %d', clust, subsplit, a)
                #assert np.isnan(best_score[a]), 'anc %s score[a]=%s best_score[a]=%s; test=%s' % (a, score[a], best_score[a], score[a] == best_score[a])
            assert clustfeatures['count'][a].shape == (4,), "shape=%s; type=%s, counts[a,0] = %s" % (clustfeatures['count'][a].shape, type(counts[a,0]), counts[a,0])
        assert not np.isnan(clustfeatures['count']).all()
        #try:
        #    self.polytomies = backup_polytomies
        #except NameError:
        #    pass
        return clustfeatures


    def ancsplit_score(self, a, subsplit, clust):
        """Examine all configs (combination of descendant taxa).
        Return the minimum score (stem score),
        and also the complete list of "partial scores" (one for each config, ignoring poly child losses)
        TODO: a version that skips partial scoring. The partial scores are not needed at the root cluster (except for unit-tests...).
        
        Arrays 'best_event_count' and 'partial_count' have shape (4,)

        We should assume that 'subsplit' has no fixed ordering (is a set or frozenset) for generality.
        """
        if len(subsplit) != 2:
            raise ValueError('subsplits must be dichotomic')

        min_score, best_event_count, best_config = np.NaN, np.NaN, []

        #min_partial = None
        #best_resolver_count, best_resolver_config = [np.full(4, np.NaN)], []
        ancsplit_configs = set()
        ancsplit_partial = {}        # in case of polytomy, we need to store sub resolutions
        ancsplit_count = {}          #FIXME: unused?
        ancsplit_direct_losses = {}  # one list for each children config.

        #only iterate realisable combinations of subancs
        n_configs = 0
        for vertical, transfered, alt_partial, partial_count in self.iter_configs(a, subsplit, clust):
            if np.isnan(alt_partial):
                # Non realisable combination of descendant taxa, skip
                continue
            n_configs += 1

            logger.debug('  - vertical %s transfered %s', vertical, transfered)

            direct_events, direct_losses = self.direct_event_count(a, vertical, transfered)

            debug_msg = '    initial partial=%s CCP=%s; direct events=%s' % (alt_partial, self.amalgam.CCP[subsplit], direct_events)

            partial_count[3] += np.log(self.amalgam.CCP[subsplit])
            partial_count[:3] += direct_events

            #FIXME: compute directly from event_count
            #alt_partial = (event_count * self.weights).sum() # where weights = [d,t,l,f]
            alt_partial -= self.freq * np.log(self.amalgam.CCP[subsplit])
            alt_partial += (direct_events * self.weights).sum()
            alt_score   = alt_partial + len(direct_losses) * self.loss
            debug_msg += '; completed: partial=%g; score=%g; events=%s + %d direct losses' % (alt_partial, alt_score, partial_count, len(direct_losses))
            logger.debug(debug_msg)

            #if min_partial is None or alt_partial < min_partial:
            #    min_partial = alt_partial
            #    best_resolver_config = [(vertical, transfered)]
            #    best_resolver_count = [partial_count]
            #elif alt_partial == min_partial:
            #    best_resolver_config.append((vertical, transfered))
            #    best_resolver_count.append(partial_count)
            if np.isnan(min_score) and not np.isnan(alt_score) or alt_score < min_score:
                min_score = alt_score
                best_config = [(vertical, transfered)]
                event_count = partial_count.copy()
                event_count[1] += len(direct_losses)
                best_event_count = [event_count]
            elif alt_score == min_score:
                if (vertical, transfered) in best_config:
                    logger.warning('%s %s : duplicate best config: %s', a, subsplit, (vertical, transfered))
                best_config.append((vertical, transfered))
                event_count = partial_count.copy()
                event_count[1] += len(direct_losses)
                best_event_count.append(event_count)

            #TODO: when vertical involves a dup, can we summarize the subconfigs? i.e. discard the partial score and subconfig, only remember the best scoring one.
            #if direct_events[0]:
            #    vertical, transfered = (a,a), (None, None)  # Wrong
            if not np.isnan(alt_partial):
                ancsplit_configs.add((vertical, transfered))
                ancsplit_partial[(vertical, transfered)] = alt_partial
                ancsplit_count[(vertical, transfered)] = partial_count
                ancsplit_direct_losses[vertical] = direct_losses

        best_event_count = np.full(4, np.NaN) if np.isnan(best_event_count).any() else np.nanmean(best_event_count, axis=0)
        if np.isnan(min_score):
            # No descendant config can be a realisable scenario
            #logger.debug('  NaN score')
            assert not best_config, 'best_config=%s partial scores: %s' % (best_config, '; '.join('%g' % ancsplit_partial[c] for c in best_config))
            assert not ancsplit_partial
        else:
            logger.debug('* anc %d %s ; subsplit %s', a, self.ancestors[a], subsplit)
            logger.debug('  MIN score = %s; best_config = %s; best_count = %s', min_score, best_config, best_event_count)

        if n_configs > self.max_ancsplit_configs:
            sorted_partial = sorted(ancsplit_partial.keys(), key=ancsplit_partial.__getitem__)
            min_partial = ancsplit_partial[sorted_partial[0]]
            max_partial = ancsplit_partial[sorted_partial[-1]]
            logger.info('%d > %d configs at anc %d with %d children, subsplit %s: trim! min=%g max=%g',
                        n_configs, self.max_ancsplit_configs, a, len(self.children[a]), subsplit, min_partial, max_partial)
            for i in range(n_configs - self.max_ancsplit_configs):
                vertical, transfered = sorted_partial.pop()
                #if len(vertical)==1 and vertical[0] is not None: keep? sorted_partial.insert(0, (vertical, transfered))
                if (vertical,transfered) in best_config:
                    logger.warning('Best config (with direct losses) %s was removed from the top partial configs!' % ((vertical, transfered),))
                ancsplit_configs.remove((vertical, transfered))
                del ancsplit_partial[(vertical, transfered)]
                del ancsplit_count[(vertical, transfered)]
            logger.info('    new max partial score: %g with %d direct_losses', ancsplit_partial[sorted_partial[-1]], len(ancsplit_direct_losses[sorted_partial[-1][0]]))

        return min_score, best_event_count, best_config, ancsplit_partial, ancsplit_count, ancsplit_direct_losses, ancsplit_configs


def best_taxa_or_resolvers(clustfeatures, anc_ids=None, split=None):
    """Return the overall best score OR partial_score"""
    if anc_ids is None:
        anc_ids = range(len(clustfeatures['score']))
    best_score = None
    best_taxa = []
    for a in anc_ids:
        if a in clustfeatures['partial']:
            #logger.info('partial[a=%d]: %d items', a, len(clustfeatures['partial'][a]))
            if not clustfeatures['partial'][a]:
                continue
            score_a, best_resolvers_a = multi_min(clustfeatures['partial'][a],
                                                  key=clustfeatures['partial'][a].__getitem__)
        else:
            score_a = clustfeatures['score'][a]
            best_resolvers_a = [None]

        if not np.isnan(score_a) and (best_score is None or score_a < best_score):
            best_taxa = [(a,resolver) for resolver in best_resolvers_a]
            best_score = score_a
        elif score_a == best_score:
            best_taxa += [(a,resolver) for resolver in best_resolvers_a]
        #if split is not None:
        # the problem is that only optimal subsplits are stored. TODO: exception for the rootclust
        #    features[rootclust]['resolver_best_subsplits'][a].items()
    return best_score, best_taxa


def EM_freq_weight_reconcile(species_tree, amalgam, get_sp, ancestors=None, time_compatible=None, dup=1, loss=0.5, transfer=3, freq=1, epsilon=0.001):
    """Expectation-Maximisation algorithm described in Scornavacca et al 2015 for TERA,
    equation (3).
    
    It finds the best weight parameter for the frequency cost VS the DTL costs,
    and return the corresponding reconciliation
    """
    #TODO
    import warnings
    warnings.warn('Unfinished/Untested function: EM_freq_weight_reconcile')

    combine = AmalgDLTCombiner(species_tree, amalgam, get_sp, ancestors, time_compatible, dup, loss, transfer, freq)

    amalgam.walk(combine, amalgam)  # triggers warning because of previous features
    #FIXME: obtain this for the best root id
    best_taxa_or_resolvers()
    events = np.nanmean(amalgam.features['count'][amalgam.full], axis=0)
    measured = np.log(events[:3].sum() / -events[3] + 1)

    n_iter = 0
    while abs(combine.freq - measured) > epsilon:
        n_iter += 1
        combine.freq = measured
        amalgam.walk(combine, amalgam)  # triggers warning because of previous features
        events = np.nanmean(amalgam.features['count'][amalgam.full], axis=0)
        measured = np.log(events.sum() / events[3])
        if n_iter > 1000:
            logger.warning('Could not converge in 1000 reconciliations, stop.')
            break
    return combine


def backtrack_best_rec(combine, root_anc_id=None, rootsplit=None):
    """Return all optimal trees.
    If root_anc or rootsplit are given, constrain the trees accordingly"""
    #FIXME: the combine input is only used to obtain amalgam, anc_range, and fill_missing_partials_adding_losses
    #best_logfreq = amalgam.max_tree_logfreq()  # Optional
    amalgam = combine.amalgam  # Only used in logging, though
    features = amalgam.features
    rootclust = amalgam.full
    if rootsplit is not None:
        raise NotImplementedError

    selected_anc_ids = combine.anc_range if root_anc_id is None else [root_anc_id]
    _, best_root_resolvers = best_taxa_or_resolvers(features[rootclust], selected_anc_ids)

    node_id = 0  # Each node & tree combination gets unique ID.

    backtrace = deque()  # (parent_id, [children_ids]), refering to ids of heads (= anc & clust).
                         # in preorder.
    heads = deque()      # Current clusters that are not terminal (not leaves).
                         # (node_id)
    data = {}            # node_id: (clust, anc_id, resolver, count, branch type)

    # Initialize each root possibility
    for a, resolver in best_root_resolvers:
        heads.append(node_id)
        count = (features[rootclust]['count'][a] if resolver is None
                 else features[rootclust]['partial_count'][a][resolver])
        data[node_id] = (rootclust, a, resolver, count, None)
        node_id += 1

    dup_splits = defaultdict(list)   # nodes that are duplications
    para_clades = defaultdict(list)  # chosen paralog to detach
    xeno_clades = defaultdict(list)

    def update_event_stats(events, split, a, vertical):
        #FIXME: just compute tree-specific event by comparing a with children species.
        nonlocal dup_splits, para_clades, xeno_clades
        #para, xeno, ortho = [], [], []  # mutually exclusive child event.
        branch_types = [None]*len(split)  # 'P' if paralog, 'X' if xenolog
        if events[0] >= events[2] and events[0] > 0:
            # duplication
            dup_splits[split].append(1)
            # select one child as the paralog subtree to detach: smallest
            # WARNING: without tuple sorting, the result is not deterministic when clusters have the same 'len'.
            #smallest, largest = sorted(sorted(split, key=tuple), key=len)
            smallest, largest = sorted(split, key=len)
            #FIXME if events[0] != 1.0, it must be taken into account
            para_clades[smallest].append(1)
            para_clades[largest].append(0)
            xeno_clades[smallest].append(0)
            xeno_clades[largest].append(0)
            #para.append(smallest)
            #ortho.append(largest)
            branch_types[list(split).index(smallest)] = 'P'
            if events[2]:
                logger.warning('    Dup of %r (%g) favored over transfer (%g)', smallest, events[0], events[2])
        elif events[2] > 0 and None in vertical:
            dup_splits[split].append(0)
            for clust, vertic in zip(split, vertical):
                para_clades[clust].append(0)
                if vertic is None:
                    logger.debug('    Map a transfer to %r (%g)', clust, events[2])
                    xeno_clades[clust].append(1)
                    branch_types[list(split).index(clust)] = 'X'
                else:
                    xeno_clades[clust].append(0)
                    #ortho.append(clust)
            #DEBUG
            #clust1, clust2 = split
            #assert (clust1 in xeno and clust2 in ortho) or (clust2 in xeno and clust1 in ortho), 'split=%r vertical=%r events=%s' % (tuple(split), vertical, events)
        else:
            if events[2]:
                #FIXME: we have a transfer count > 0 but the config has no transfered (=None) child
                logger.warning('    Spe at %r favored over transfer (%g)', split, events[2])
            dup_splits[split].append(0)
            for sub in split:
                para_clades[sub].append(0)
                xeno_clades[sub].append(0)
                #ortho.append(sub)
        #assert len(para + xeno + ortho) == len(split)
        #return para, xeno, ortho
        #assert None not in branch_types
        return branch_types

    while heads:
        parent_id = heads.pop()
        clust, a, resolver, count, _ = data[parent_id]

        if resolver is not None:
            # Resolver of a polytomy (one side of a config)
            try:
                subsplits = features[clust]['resolver_best_subsplit'][a][resolver]
            except KeyError:
                logger.error('KeyError a=%s on resolver_best_subsplit=%s / partial.keys: %s',
                             a, features[clust]['resolver_best_subsplit'],
                             list(features[clust]['partial'].keys()))
                raise
            logger.debug('  head %d %r resolver %s -> resolver_best_subsplit %s', parent_id, clust, resolver, subsplits)
            splits_configs = [(subsplit, resolver) for subsplit in subsplits]
        else:
            splits_configs = features[clust]['best_split_config'][a]
            #if not split_configs:
                #FIXME: a better implementation would store it in the partial
                # resolver of length 1: the best descendant, in the same clust (but don't add node in backtrace)
                #combine.fill_missing_partials_adding_losses()
                # Is it fixed?

        # find out the direct event, for post-hoc pruning of paralogs/xenologs
        logger.debug('* head %r -> %s ; anc %s %r', clust,
                     '/'.join(repr(set(amalgam.subsplits[clust][b])) for b,config in splits_configs),
                     a, combine.ancestors[a])

        assert splits_configs, 'Missing child reconciliation! resolver %s; all subsplits: %s; n_best(clust): %s resolver_best_subsplit[a=%s]=%s' % (resolver, amalgam.subsplits[clust], ','.join('%s:%d' % (ci,k) for ci,k in features[clust]['n_best'].items() if k), a, features[clust]['resolver_best_subsplit'].get(a))

        # Expand the current head into each possible subsplit x config.
        for b, config in splits_configs:
            subsplit = amalgam.subsplits[clust][b]
            subscores = []
            subcounts = []
            children_ids = []
            # !! if iter_configs uses 'sorted(subsplit)', apply it here as well!
            for sub, child, transfer in zip(subsplit, *config):  # !! I implicitly assume that subsplit order stayed consistent with the config
                children_ids.append(node_id)
                assert node_id not in data, "node_id=%d sub=%r a=%s" % (node_id, sub, a)
                if isinstance(child, tuple):
                    # resolver of a polytomy. We must unpack it recursively as well.
                    if len(child) > 1:
                        try:
                            subscores.append(features[sub]['partial'][a][child,transfer])
                        except KeyError as err:
                            logger.error('KeyError %s: features[sub=%r][partial][a=%s] '
                                         'keys: %s from subsplit %s config %s / parent partial keys: %s',
                                    err, sub, a, list(features[sub]['partial'][a].keys()), subsplit,
                                    config, list(features[clust]['partial'][a].keys()))
                            raise
                        subcounts.append(features[sub]['partial_count'][a][child,transfer])
                        data[node_id] = [sub, a, (child, transfer), subcounts[-1]]
                    elif child[0] is None:
                        subscores.append(features[sub]['score'][transfer[0]])
                        subcounts.append(features[sub]['count'][transfer[0]])
                        data[node_id] = [sub, transfer[0], None, subcounts[-1]]
                    else:
                        subscore = features[sub]['score'][child[0]]
                        if np.isnan(subscore):
                            # This direct child was not considered, but has vertical descendants scored.
                            best_desc, subscore, subcount = combine.fill_missing_partials_adding_losses(a, child[0], sub)
                        else:
                            subcount = features[sub]['count'][child[0]]
                            best_desc = child[0]
                        subscores.append(subscore)
                        subcounts.append(subcount)
                        data[node_id] = [sub, best_desc, None, subcounts[-1]]
                    logger.debug('  add resolver head %r %s', sub, data[node_id])
                else:
                    if child is None:
                        child = transfer
                    subscores.append(features[sub]['score'][child])
                    subcounts.append(features[sub]['count'][child])
                    data[node_id] = [sub, child, None, subcounts[-1]]
                assert not np.isnan(subcounts[-1]).any(), "%s sub %r child %s" % (subcounts[-1], sub, child)
                if len(sub) > 1:
                    heads.append(node_id)
                node_id += 1

            assert not np.isnan(subscores).any() and len(subscores), 'subsplit %s sub %r child %s' % (subsplit, sub, child)

            backtrace.append((parent_id, children_ids))

            logger.debug('  subsplit %d: %s -> parental %s', b, ', '.join('%s %s' % (data[ch][1], sc) for ch,sc in zip(children_ids, subcounts)), count)
            #FIXME:
            # When count was obtained by averaging equally good configs, it may result in lower counts than its children.
            # Example: test case dup_oneloss_or_transfer (DTL = 2,1,3)
            # parent counts = average of [1,1,0] & [0,0,1] = [.5, .5, .5]
            # produces negative events when substracting.
            branch_types = update_event_stats(count - np.sum(subcounts, axis=0), subsplit, a, config[0])
            for sub, child_id, btype in zip(subsplit, children_ids, branch_types):
                assert len(data[child_id]) == 4, "sub=%r child id=%s" % (sub, child_id)
                data[child_id].append(btype)

    #for k,v in dup_splits.items():
    #    assert len(v) == len(tree_traversals), "dup[%s] %d != %d trees" % (k, len(v), len(tree_traversals))
    #for k,v in para_clades.items():
    #    assert len(v) == len(tree_traversals), "para[%r] %d != %d trees" % (k, len(v), len(tree_traversals))
    #for k,v in xeno_clades.items():
    #    assert len(v) == len(tree_traversals), "xeno[%r] %d != %d trees" % (k, len(v), len(tree_traversals))

    # Probability of being a paralog/xenolog subtree in a most parsimonious reconciliation:
    dup_splits = {split: sum(is_dup)/len(is_dup) for split,is_dup in dup_splits.items()}
    para_clades = {clust: sum(is_para)/len(is_para) for clust,is_para in para_clades.items()}
    xeno_clades = {clust: sum(is_xeno)/len(is_xeno) for clust,is_xeno in xeno_clades.items()}

    assert tuple(sorted(data)) == tuple(range(node_id)), "%s" % tuple(sorted(data))
    return backtrace, data, dup_splits, para_clades, xeno_clades


def rectrees_from_backtrace(combine, backtrace, data, dup_splits, para_clades, xeno_clades,
                            **feature_codes):
    """Most parsimonious reconciled trees. Run on 'backtrack_best_rec' output."""
    from ete3 import TreeNode
    amalgam = combine.amalgam
    rootsize = len(amalgam.full)

    trees = []
    subtrees = {}  # node_id: [subtrees]

    while backtrace:
        node_id, children_ids = backtrace.pop()
        clust, anc_id, _, count, _ = data[node_id]

        split = []
        for child_id in children_ids:
            sub, subanc, *_ = data[child_id]
            split.append(sub)
            if len(sub) == 1:
                child = TreeNode(name=amalgam.labels[list(sub)[0]])
                child.add_feature('id', list(sub)[0])
                subtrees[child_id] = [child]
                child.add_features(**{feature_codes.get(ft, ft): value
                    for ft, value in zip(
                        ('taxon_id', 'taxon'),
                        (subanc, combine.ancestors[subanc]))}
                    )

        split = frozenset(split)
        assert len(split)>0, "Node for %r at anc %s has no children!" % (clust, combine.ancestors[anc_id])
        assert amalgam.unions[split] == clust, "clust %r not parent of %s" % (clust, split)

        for children in product(*(subtrees[c] for c in children_ids)):
            node = TreeNode()
            try:
                node.support = amalgam.freq[clust]
            except KeyError as err:
                if len(clust) != 1 and clust != amalgam.full:
                    err.args += (" clust=%r (len=%d)" % (clust, len(clust)),)
                    raise
            node.add_features(**{
                feature_codes.get(ft, ft): value for ft, value in [
                                        ('taxon_id', anc_id),
                                        ('taxon', combine.ancestors[anc_id]),
                                        ('count', count if count.any() else 0),
                                        ('dup_fraction', dup_splits[split])]
                })

            for child_id, child in zip(children_ids, children):
                sub, _, _, _, evt = data[child_id]
                node.add_child(child, dist=amalgam.dist.get(sub, 0))
                child.add_features(**{feature_codes.get(ft, ft): value
                    for ft, value in zip(
                        ('xeno_fraction', 'para_fraction'),
                        (xeno_clades[sub], para_clades[sub]))}
                    )
                if evt:
                    child.add_feature(feature_codes.get('event', 'event'), evt)
                if len(sub) > 1:
                    #DEBUG
                    child.add_feature('clust', sub)

            if len(clust) == rootsize:
                trees.append(node)
            else:
                try:
                    subtrees[node_id].append(node)
                except KeyError:
                    subtrees[node_id] = [node]

    return trees
