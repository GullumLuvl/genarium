#!/usr/bin/env python3

# (C) Copyright Guillaume Louvel 2023
#
# This file is licensed under the GNU General Public License GPL version 3


"""
Reconciliation of amalgamated gene trees
(trees that can be built from a sample of tripartitions).

Similar to TERA (Scornavacca et al. 2015) and ecceTERA (Jacox et al. 2016).

This aims to extend the algorithm to allow polytomic species trees.
"""


import re
from sys import stderr
import datetime as dt
from functools import partial
from collections import Counter
import argparse as ap
import numpy as np
from base64 import b85encode
import ete3
from myriphy.newick import iter_multinewick
from myriphy.compactset import BitSet
from myriphy.MCMC import multi_chain_burnin
from myriphy.CCP import build_amalgam
from .seq2clust import load_seq2sp, first_match_capture
from .amalgrec import AmalgDLTCombiner, backtrack_best_rec, rectrees_from_backtrace, multi_min, best_taxa_or_resolvers
import logging
logger = logging.getLogger(__name__)


def amalgam_rec_postorder(treelists, species_tree, seq2sp_regexes=[re.compile(r'(.*)')],
                          burnin=0.05, dlt_string='2,1,5', freq_weight=0.7,
                          thin_low_freq=None, memlimit=None):
    ntrees = []
    for treefile in treelists:
        if treefile == '-':
            stdin_trees = list(iter_multinewick(treefile))
            ntrees.append(len(stdin_trees))
        else:
            ntrees.append(sum(1 for _ in iter_multinewick(treefile)))
    burnins = multi_chain_burnin(ntrees, burnin)
    logger.info('burnins = %s; treefiles = %s', burnins, ', '.join(treelists))

    treelist = []
    for burnin, treefile in zip(burnins, treelists):
        next_trees = stdin_trees if treefile == '-' else list(iter_multinewick(treefile))
        treelist.extend(next_trees[burnin:])

    amalgam = build_amalgam(treelist)
    try:
        thin_params = [float(x) for x in thin_low_freq.split(',')]
    except AttributeError:
        thin_params = None
    if thin_params:
        amalgam.thin_low_freq(*thin_params)
        logger.info('Thinned %d+%d clusters and %d subsplits', *amalgam.thinned)

    seq2sp = {seqname: first_match_capture(seqname, seq2sp_regexes)
              for seqname in amalgam.labels.values()}

    species_counts = Counter(seq2sp.values())
    species_dups = [sp for sp,count in species_counts.items() if count>1]
    if species_dups:
        print('%d species have duplicate genes' % (len(species_dups)), file=stderr)
    else:
        print('No duplicate genes', file=stderr)
        #return
    if dlt_string == 'auto':
        # Not a logical rule, just a poor guess
        dlt = [2, 1, 5]
        r_species = len(species_counts) / len(species_tree)
        if r_species <= 0.2:
            dlt[1] = 0.2
        elif r_species <= 0.8:
            dlt[1] = 0.5
        print('Auto weights D,L,T =', ', '.join(map(str, dlt)),
              '. %.2f %% species represented' % (r_species*100), file=stderr)
    else:
        dlt = [float(x) for x in dlt_string.split(',')]

    print('dlt = %r, freq_weight = %r' % (dlt, freq_weight), file=stderr)
    combine = AmalgDLTCombiner(species_tree, amalgam, seq2sp.__getitem__)
    combine.set_weights(*dlt, freq_weight)

    amalgam.init_leaves(partial(combine, []))
    print('Leaves initialized, start recursion', file=stderr)

    amalgam.walk(combine, checkmem=memlimit)
    return amalgam, combine, seq2sp


def amalgam_rec_backtrack(amalgam, species_tree, combine, seq2sp):
    species_counts = Counter(seq2sp.values())
    max_log_freq = amalgam.max_tree_logfreq()

    root_id = combine.anc_index[species_tree.name]
    selected_root_constraints = [None]
    best_score, best_root_resolvers = best_taxa_or_resolvers(amalgam.features[amalgam.full], combine.anc_range)
    for best_root_id, best_resolver in best_root_resolvers:
        if best_root_id == root_id:
            break
    else:
        if root_id in combine.polytomies:
            measured_count = amalgam.features[amalgam.full]['best_partial_count'][root_id]
            logger.info('measured_count (poly root=%d) = %s', root_id, measured_count)
            if np.isnan(measured_count).all():
                logger.warning('All NaN measured counts! (count=%s)' % amalgam.features[amalgam.full]['count'][root_id])
                measured_count = amalgam.features[amalgam.full]['count'][root_id]
        else:
            measured_count = amalgam.features[amalgam.full]['count'][root_id]
            logger.info('measured_count (dicho root=%d) = %s', root_id, measured_count)
        freq_delta = max_log_freq - measured_count[3]
        expected_freq_weight = np.log(measured_count[:3].sum()/freq_delta + 1)
        print('#', species_tree.name,
              'score:', amalgam.features[amalgam.full]['score'][root_id],
              'count:', amalgam.features[amalgam.full]['count'][root_id],
              '\n# partial score: %g' % amalgam.features[amalgam.full]['best_partial'].get(root_id, np.NaN),
              ' partial count:', amalgam.features[amalgam.full]['best_partial_count'].get(root_id, np.NaN),
              ' output freq weight:', expected_freq_weight)

        # The optimal score is not obtained at the root taxon, so print it.
        print('# Best score:', best_score)
        selected_root_constraints.append(root_id)

    for best_root_id, best_resolver in best_root_resolvers:
        if best_root_id in combine.polytomies:
            measured_count = amalgam.features[amalgam.full]['best_partial_count'][best_root_id]
            if np.isnan(measured_count).all():
                logger.error('All NaN measured counts! (count=%s)' % amalgam.features[amalgam.full]['count'][root_id])
        else:
            measured_count = amalgam.features[amalgam.full]['count'][best_root_id]
        freq_delta = max_log_freq - measured_count[3]
        expected_freq_weight = np.log(measured_count[:3].sum()/freq_delta + 1)
        print('# Optimal root taxon: %s,' % combine.ancestors[best_root_id],
              'resolver:%s' % (best_resolver,),
              '\n# score: %g' % amalgam.features[amalgam.full]['score'][best_root_id],
              'count:', amalgam.features[amalgam.full]['count'][best_root_id],
              '\n# partial score:%g' % amalgam.features[amalgam.full]['best_partial'].get(best_root_id, np.NaN),
              'partial count:', amalgam.features[amalgam.full]['best_partial_count'].get(best_root_id, np.NaN),
              'expected freq weight:', expected_freq_weight)

    #logging.getLogger('grec.amalgrec').setLevel(logging.DEBUG)

    for root_anc_constraint in selected_root_constraints:
        if root_anc_constraint is not None:
            print('## Constraint on the root taxon:', combine.ancestors[root_anc_constraint])

        backtrace, data, dup_splits, para_clades, xeno_clades\
                = backtrack_best_rec(combine, root_anc_id=root_anc_constraint)

        if not backtrace:
            print('# ZERO tree',
                  ('root taxon constraint:'+combine.ancestors[root_anc_constraint]
                   if root_anc_constraint is not None else ''))
            break

        rectrees = rectrees_from_backtrace(combine, backtrace, data, dup_splits, para_clades, xeno_clades, taxon='S', count='C', dup_fraction='D', para_fraction='P', xeno_fraction='X', event='E')

        print('#',
              ('root taxon constraint = %s :' % combine.ancestors[root_anc_constraint]
               if root_anc_constraint is not None else ''),
               '%d optimal tree%s' % (len(rectrees), 's' if len(rectrees)>1 else ''))

        # TODO: select one of the roots based on minimal Mean Path Length difference.
        #logger.info('traversal lengths: %s', [len(t) for t in tree_traversals])

        #TODO: find events that are mutually exclusive
        #clade_events = {k: [None]*len(tree_traversals) for k in para_clades}
        for rectree in rectrees:
            #clades = {}
            assert all('id' in leaf.features for leaf in rectree.iter_leaves())
            #for node in rectree.iter_descendants('postorder'):
            #    if node.is_leaf():
            #        try:
            #            clades[node] = BitSet((node.id,))
            #        except AttributeError as err:
            #            err.args = err.args + ("leaf #%d %s" % (rectree.get_leaves().index(node), ' '.join('%s=%r' % (ft, getattr(node, ft)) for ft in node.features)),)
            #            #import ipdb; ipdb.set_trace();

            #            raise
            #    else:
            #        subclust = clades[node.children[0]]
            #        clades[node] = subclust.union(*(clades[ch] for ch in node.children[1:]))
            #    #clade_events[clades[node]][i] = getattr(node, 'E', 'S')
            print(rectree.write(['S', 'D', 'P', 'X', 'C', 'E'], format=0, format_root_node=True))

        # Decide where to prune to obtain trees of orthologs only.
        #pruned = BitSet(())
        pruned_species = Counter()

        candidates_xeno = sorted(xeno_clades.items(), key=lambda item: item[1], reverse=True)
        candidates_para = sorted(para_clades.items(), key=lambda item: item[1])

        clades_pruned = []
        leaves_pruned = np.zeros(len(amalgam.full))  # addition frequencies
        print('# Sizes and frequencies of xeno clusters')
        for pruned, freq in candidates_xeno:
            if freq == 0:
                break
            for k in pruned:
                leaves_pruned[k] += freq
            if freq >= 0.2:
                print('%d\t%.6f' % (len(pruned), freq), b85encode(pruned.asbytes()).decode(), sep='\t')
                clades_pruned.append(pruned)

        if not leaves_pruned.any():
            print('No HGT found.', file=stderr)
        else:
            print('Infered %d xenologs from %d transfers / downweighted transfers: %d' % (
                  (leaves_pruned>=0.2).sum(), len(clades_pruned), sum(xeno_clades[cl] for cl in clades_pruned)), file=stderr)

        pruned_species = Counter(seq2sp[amalgam.labels[i]] for i in np.flatnonzero(leaves_pruned>=0.2))

        print('# Sizes and frequencies of para clusters')
        while any(v>1 for v in (species_counts - pruned_species).values()):
            pruned, freq = candidates_para.pop()
            if freq == 0:
                break
            for k in pruned:
                #FIXME: cannot count a leaf more than once per tree (take max event freq)
                # occurs if it descends from multiple non-orthologous events.
                leaves_pruned[0] += freq
            clades_pruned.append(pruned)
            print('%d\t%.5f' % (len(pruned), freq), b85encode(pruned.asbytes()).decode(), sep='\t')
            pruned_species = Counter(seq2sp[amalgam.labels[i]] for i in np.flatnonzero(leaves_pruned>=0.2))

        print('# %d sequences to prune' % (leaves_pruned>=0.2).sum())
        for i in leaves_pruned.argsort()[::-1]:
            freq = leaves_pruned[i]
            if freq < 0.2:
                break
            seqname = amalgam.labels[i]
            print('%.5f\t%s\t%s' % (freq, seqname, seq2sp[seqname]))


def main():
    start_time = dt.datetime.now()
    loggers = [logger, logging.getLogger('grec.amalgrec'), logging.getLogger('tools.CCP')] #, logging.getLogger('tools.newick')]
    try:
        from UItools.colorlog import ColoredFormatter
        ColoredFormatter.install(*loggers)
    except ImportError:
        logging.basicConfig(level=logging.INFO)
    for logr in loggers:
        logr.setLevel(logging.INFO)
    parser = ap.ArgumentParser(description=__doc__)
    parser.add_argument('treelists', nargs='+')
    parser.add_argument('species_tree')
    parser.add_argument('-i', '--get-taxon-name', help="Simply return the taxon names for the given ids (comma-separated).")
    parser.add_argument('-b', '--burnin', default=0.05, type=float, help='[%(default)s]')
    parser.add_argument('--dlt', default='2,1,5',
                        help=('comma-separated list of the D,L,T costs. '
                              '"auto" tunes the loss rate based on the species representation. [%(default)s]'))
    parser.add_argument('-w', '--freq-weight', type=float, default=0.7,
                        help='Weight of the penalty for CCP (-log(CCP/best tree CCP)) [%(default)s]')
    parser.add_argument('-t', '--thin-low-freq', help='Reduce the input amalgam')
    parser.add_argument('-m', '--memlimit', help='Human readable RAM limit')

    seq2sp_parser = parser.add_mutually_exclusive_group()
    seq2sp_parser.add_argument('-r', '--seq2sp_regex', metavar='REGEX', default=r'(.*)',
                       help=('Regular expression to capture the species name in the sequence label'
                             ' (a parenthesized group is required) [%(default)r].'))
    seq2sp_parser.add_argument('-f', '--seq2sp_file', metavar='REGEX_FILE',
                       help=('One regular expression per line, stop at the first matching one.'
                             ' Lines starting with "#" are ignored.'))

    args = parser.parse_args()

    seq2sp_regexes = load_seq2sp(args.seq2sp_file) if args.seq2sp_file else [re.compile(args.seq2sp_regex)]

    species_tree = ete3.Tree(args.species_tree, format=1)
    if args.get_taxon_name:
        taxon_ids = [int(x) for x in args.get_taxon_name.split(',')]
        ancestors = [n.name for n in species_tree.traverse()]
        for tid in taxon_ids:
            print('%d\t%s' % (tid, ancestors[tid]))
        return

    amalgam, combine, seq2sp = amalgam_rec_postorder(args.treelists, species_tree, seq2sp_regexes, args.burnin, args.dlt, args.freq_weight, args.thin_low_freq, args.memlimit)

    np.set_printoptions(formatter={'float_kind': lambda x: '%g' % x})

    amalgam_rec_backtrack(amalgam, species_tree, combine, seq2sp)
    end_time = dt.datetime.now()
    logger.info('Run time: ' + str(end_time - start_time).rsplit('.', 1)[0])


if __name__ == '__main__':
    main()
