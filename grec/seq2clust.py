#!/usr/bin/env python3

# (C) Copyright Guillaume Louvel 2023
#
# This file is licensed under the GNU General Public License GPL version 3


"""
Convert sequence names into `species`,`clust`.

`clust` would usually mean a higher order taxon.
"""


from sys import stdin
import re
import argparse as ap
from collections import OrderedDict
from ete3 import TreeNode

import logging
logger = logging.getLogger(__name__)


def read_sp_cluster_hierarchy(lines):
    """Iterate over (cluster name, list of members)"""
    unnamed = 1
    for line in lines:
        if line.rstrip() and not line.startswith('#'):
            cl, *species = line.rstrip().split('\t')
            if (not species and '+' in cl):  # Old syntax
                # this cluster is a fusion of multiple precedent clusters
                subcl = cl.split('+')
                cl = 'Cluster%d=' % unnamed
                unnamed += 1
                yield cl, subcl
            else:
                yield cl, species


def read_sp_clusters(lines):
    clusts = OrderedDict()          # cluster name -> set of species
    species_clusts = {}  # species name -> [nested clusters names] (inner to outer)
    for cl, species in read_sp_cluster_hierarchy(lines):
        denested = set()
        if cl.endswith('='):
            # This cluster is a only container of existing clusters
            for sp in species:
                if sp == cl:
                    raise ValueError('Infinite recursion: sp == clust == %r' % sp)
                denested.update(clusts[sp])
        elif cl.endswith('~'):
            # A member of such cluster is recognized by pattern matching (species are patterns).
            denested.update(species)
            # Or build the PatternDict directly:
            #species_clusts.set_pattern_item(r'|'.join(species))
            # We let the regex populate the sp_clusts dictionary, as if one regex is one species.
        else:
            if all(sp.startswith('*') for sp in species):
                logger.warning('Container cluster %r is defined implicitly, and will not be treated specially. The notation "Container= member1 member2" is the explicit alternative.', cl)
            for sp in species:
                if sp.startswith('*'):
                    denested.update(clusts[sp[1:]])
                #elif sp == cl:
                #    raise ValueError('Infinite recursion: species == cluster == %r' % sp)
                else:
                    if sp in clusts:
                        logger.warning('In cluster %r, member species %r is also a cluster name', cl, sp)
                    denested.add(sp)

        for sp in denested:
            sp_clusts = species_clusts.setdefault(sp, [])
            sp_clusts.append(cl)
        clusts[cl] = denested
    return clusts, species_clusts


def load_sp_clusters(filename):
    with open(filename) as f:
        return read_sp_clusters(f)


def clusters_to_tree(cluster_hierarchy):
    subtrees = {clust: TreeNode(name=clust.rstrip('=~')) for clust in cluster_hierarchy}
    for clust, members in cluster_hierarchy.items():
        node = subtrees[clust]
        for member in members:
            try:
                child = subtrees[member]
            except KeyError:
                try:
                    child = subtrees[member + '=']
                except KeyError:
                    child = TreeNode(name=member)
            if child.up is not None:
                raise RuntimeError('node %r already has a parent node %r. Cannot make a child of %r' % (member, child.up.name, clust))
            node.add_child(child)
    roots = set(node.get_tree_root() for clust, node in subtrees.items())
    if len(roots) > 1:
        logger.warning('Clusters are not connected into a single tree: joining roots = %s', ' '.join(r.name for r in roots))
        clust = 'ROOT'
        root = subtrees[clust] = TreeNode(name=clust)
        for node in roots:
            root.add_child(node)

    return subtrees[clust].get_tree_root()


def load_seq2sp(filename):
    """Construct a list of regex to be tried sequentially.

    Each regex should capture the taxon identifier in the first group.
    This taxon identifier is then converted using a dictionary.
    re.match is used.
    """
    seq2sp = []  # list of regex to try in order
    with open(filename) as f:
        for line in f:
            if not line.startswith('#'):
                seq2sp.append(re.compile(line.rstrip()))
    return seq2sp


def load_conv_file(filename, sep='\t'):
    """
    Read tab separated file with 2 columns into dict

    The key '*' can store the default value
    """
    conv = {}
    with open(filename) as f:
        for line in f:
            if not line.startswith('#'):
                fields = line.rstrip().split(sep)
                conv[fields[0]] = fields[1]
    return conv


def apply_leaf_features(tree, get_key, save_key='', **attr_maps):
    """if save_key is given, store the result of get_key as attribute save_key"""
    for leaf in tree.iter_leaves():
        try:
            key = get_key(leaf.name)
        except BaseException as err:
            if not isinstance(err, KeyboardInterrupt):
                err.args += ('with leaf name %r' % leaf.name,)
            raise
        if key is None:
            continue
        if save_key:
            leaf.add_feature(save_key, key)
        for attrname, attrs in attr_maps.items():
            try:
                value = attrs[key]
            except KeyError as err:
                try:
                    value = attrs['*']
                except KeyError:
                    err.args = ('%r at leaf %r (%s, no default defined)' % (err.args[0], leaf.name, attrname),) + err.args[1:]
                    raise err
            except TypeError:
                # Accept functions as well:
                value = attrs(key)
            leaf.add_feature(attrname, value)


def mark_paralogs(tree, get_sp):
    species_log = {}
    for leaf in tree.iter_leaves():
        sp = get_sp(leaf.name)
        leaf.add_feature('sp', sp)
        if sp in species_log:
            leaf.add_features(paralog=species_log[sp][0].name,
                              n_paralog=len(species_log[sp]))
            if len(species_log[sp])==1:
                species_log[sp][0].add_feature('n_paralog', 0)
            species_log[sp].append(leaf)
        elif sp is not None:
            species_log[sp] = [leaf]
    return species_log


# Yep, a global variable (In order to print a single warning instead of 500)
TOTAL_UNMATCHED = set()


def first_match_capture(string, regexes, allow_unmatched=True):
    """Return the captured group of the first matching regex"""
    for i in range(len(regexes)):
        try:
            m = regexes[i].match(string)
        except TypeError as err:
            err.args += ("on %s: %r" % (type(string), string),)
            raise
        if m:
            key = m.group(1)
            if key is None:
                raise ValueError('%r capture is empty with regex %s' % (string, regexes[i].pattern))
            break
    else:
        if allow_unmatched:
            TOTAL_UNMATCHED.add(string)
            key = None
        else:
            raise ValueError('%r not matched by any regex (%d)' % (string, i))

    return key


CONV_DOC = """
CONVERSIONS SUMMARY:

    node.name > species code [ > species > cluster ]

EXAMPLE:

1. Input names have suffixes '_cyano', '_gloeo' or '_euk';
2. The conversion regex just need to capture this suffix, using
       --seq2sp_regex '.*_([a-z]+)$' 
    or --seq2sp_regex '.*_(cyano|gloeo|euk)$' to be more specific
3. Call:
    seq2clust --seq2sp_regex '.*_([a-z]+)' inputnames.txt
"""


def build_seq2clust_args(parser):
    """Add command-line options to the parser required for the sequence name-to-cluster conversion"""
    parser.add_argument('-k', '--spcluster_file',
                        help=('Optional. Contain one tab-separated row for each a priori clustered species:'
                            ' "clustername sp1 sp2 ...". Additionally a supercluster is defined by '
                            'combining several other clusters: "cluster1+cluster2". If no file given, '
                            'the `seq2sp` regex is assumed to capture the clustername.'))

    parser.add_argument('-s', '--spcode_file', metavar='SCFILE',
                        help='Convert codename used in the sequence label into the species name (2 columns) [TO REMOVE]')

    seq2sp_parser = parser.add_mutually_exclusive_group()
    seq2sp_parser.add_argument('-r', '--seq2sp_regex', metavar='REGEX', default=r'(.*)',
                       help=('Regular expression to capture the species name in the sequence label'
                             ' (a parenthesized group is required) [%(default)r].'))
    seq2sp_parser.add_argument('-f', '--seq2sp_file', metavar='REGEX_FILE',
                       help=('One regular expression per line, stop at the first matching one.'
                             ' Lines starting with "#" are ignored.'))
    parser.add_argument('-u', '--unmatched', action='store_true', help='Skip regex that fail')


class PatternDict(dict):
    """A dictionary where some keys can be regex patterns.

    Accessing elements can fallback on the patterns, by order of insertion into the dict.
    """
    def __init__(self, *args, fuzzy=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.fuzzy = OrderedDict()  # pattern -> value
        if fuzzy is not None:
            self.update_patterns(fuzzy)

    def __getitem__(self, key):
        try:
            return super().__getitem__(key)
        except KeyError:
            for regex, value in self.fuzzy.items():
                if regex.search(key):
                    return value
            raise

    def get(self, key, default=None):
        try:
            return self.__getitem__(key)
        except KeyError:
            return default

    def values(self):
        return list(super().values()) + list(self.fuzzy.values())

    def set_pattern_item(self, pattern, value, flags=0):
        self.fuzzy[re.compile(pattern, flags=flags)] = value

    def update_patterns(self, fuzzy, flags=0):
        try:
            fuzzy_items = fuzzy.items()
        except AttributeError:
            fuzzy_items = iter(fuzzy)
        for pattern, value in fuzzy_items:
            self.fuzzy[re.compile(pattern, flags=flags)] = value

    def __repr__(self):
        r = super().__repr__()
        r += ' /' + repr({reg.pattern: val for reg,val in self.fuzzy.items()}).strip('{}') + '/'
        return r

    def setdefault(self, key, value):
        raise NotImplementedError


def build_seq2clust(args):
    #TODO: make a `Cluster` class, with access methods for hierarchy, members, get2sp(), etc
    clusters, species_clusts = load_sp_clusters(args.spcluster_file) if args.spcluster_file else (None,None)
    spcodes = load_conv_file(args.spcode_file) if args.spcode_file else {}

    seq2sp = load_seq2sp(args.seq2sp_file) if args.seq2sp_file else [re.compile(args.seq2sp_regex)]

    def get_sp(seqname):
        sp = first_match_capture(seqname, seq2sp, args.unmatched)
        return spcodes.get(sp, sp)

    if clusters is None:
        get_clust = get_sp
    else:
        # Identify clusters with a species/sequence name pattern
        fuzzy_clusters = {'|'.join(members): species_clusts[list(members)[0]] #FIXME convoluted
                              for clust, members in clusters.items() if clust[-1]=='~'}
        if fuzzy_clusters:
            species_clusts = PatternDict(species_clusts, fuzzy=fuzzy_clusters)
        species_clusts[None] = [None]

        def get_clust(seqname):
            sp = get_sp(seqname)
            #return species_clusts.get(sp, [None])[0]  # Get the first defined cluster
            return species_clusts[sp][0]  # Get the first defined cluster

    return clusters, species_clusts, get_sp, get_clust


def main():
    parser = ap.ArgumentParser(description=__doc__, epilog=CONV_DOC,
                               formatter_class=ap.RawDescriptionHelpFormatter)
    parser.add_argument('infile')
    parser.add_argument('-o', '--out-format', default='sp,clusts',
                        help=('comma separated list of output columns. Names are: '
                              'sp,clusts,seq. clust[0] or last select the '
                              'given level. [%(default)s]'))
    build_seq2clust_args(parser)

    args = parser.parse_args()
    columns = args.out_format.split(',')
    row_format = '\t'.join('{'+name+'}' for name in columns)

    if args.infile == '-':
        lines = [line.rstrip() for line in stdin]
    else:
        with open(args.infile) as f:
            lines = [line.rstrip() for line in f]

    clusters, species_clusts, get_sp, get_clust = build_seq2clust(args)

    try:
        for line in lines:
            sp = get_sp(line)
            clust = species_clusts[sp]
            print(row_format.format(seq=line.rstrip(), sp=sp, clust=clust, last=clust[-1], clusts='\t'.join(clust)))
    except BrokenPipeError:
        pass


if __name__ == '__main__':
    main()
