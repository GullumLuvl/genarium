#!/usr/bin/env python3

# (C) Copyright Guillaume Louvel 2023
#
# This file is licensed under the GNU General Public License GPL version 3


"""
Measure the Conditional Clade Probabilities (CCP) from a sample of trees.

See Höhna & Drummond 2012,
Larget 2013,
Szöllősi et al. 2013, Efficient exploration of the space of reconciled gene trees.

By default, output the tree of highest probability.
"""


from sys import stdout, stderr, argv
import sys
import argparse as ap
from queue import deque
from itertools import chain, combinations, islice
from collections import Counter, defaultdict
from math import log, log2, modf
try:
    from math import comb  # since Python 3.8
except ImportError:
    from scipy.special import comb
from statistics import mean, median
try:
    from ete3 import Tree, TreeNode
except ImportError:
    pass
from .newick import iter_multinewick, IterNewickClusters, IterNewickClustersPartial, rewrite_indexed_newick
from .MCMC import multi_treelist_burnin
# RAM Optimization
from .compactset import BitSet, bitset_pack, bitset_unpack, frozen_freeze

import logging
logger = logging.getLogger(__name__)


def sortuple(iterable):
    return tuple(sorted(iterable, key=tuple))

def frozen_pack_id(setgroup, get_id):
    return frozenset((frozenset(get_id(label) for label in clust) for clust in setgroup))


class FormatDistrib:
    @staticmethod
    def fmt_subsplit(subsplit, intrasep=',', conv=str):
        subsplit_txt = [intrasep.join(conv(x) for x in clust) for clust in bitset_unpack(subsplit)]
        subsplit_txt.sort()
        return '|'.join(subsplit_txt)

    @classmethod
    def fmt_distrib(cls, distrib, intrasep=',', conv=str, itemsep=' '):
        return itemsep.join('%s:%g' % (cls.fmt_subsplit(k, intrasep, conv), v) for k,v in distrib.items())

    @classmethod
    def pretty_distrib(cls, distrib, intrasep=',', conv=str):
        return Counter(dict((cls.fmt_subsplit(k, intrasep, conv), v) for k,v in distrib.items()))


class FrozenFormatDistrib(FormatDistrib):
    @staticmethod
    def fmt_subsplit(subsplit, intrasep=',', conv=str):
        subsplit_txt = [intrasep.join(conv(x) for x in sorted(clust, key=tuple)) for clust in subsplit]
        subsplit_txt.sort()
        return '|'.join(subsplit_txt)


def combine_most_frequent_subsplit(subsplits, clust, amalgam):
    if len(clust) == 2:
        assert len(subsplits) == 1
        return {'max_logfreq': 0, 'most_frequent': list(subsplits)}
    else:
        max_logfreq = None
        most_freq_sub = []
        for sub in subsplits:
            logfreq = log(amalgam.CCP[sub])
            for subclust in sub:
                try:
                    logfreq += amalgam.features[subclust]['max_logfreq']
                except KeyError:
                    assert len(subclust) == 1
            if max_logfreq is None or logfreq > max_logfreq:
                max_logfreq = logfreq
                most_freq_sub = [sub]
            elif logfreq == max_logfreq:
                most_freq_sub.append(sub)
        return {'max_logfreq': max_logfreq, 'most_frequent': most_freq_sub}


def combine_sorted_proba_subtrees(subsplits, clust, amalgam):
    """Sort each possible subtree by decreasing probability.
    Can be used to list possible trees by decreasing likelihood
    (pick the next closest freq)"""
    if len(clust) == 2:
        assert len(subsplits) == 1
        return {'logfreqs': [0], 'subtrees': list(subsplits)}
    else:
        logfreqs = []
        for split in subsplits:
            logfreq = log(amalgam.CCP[split])
            for subclust in split:
                try:
                    logfreq += amalgam.features[subclust]['logfreqs'][0]
                except KeyError:
                    assert len(subclust) == 1
            logfreqs.append(logfreq)
        order = sorted(range(len(subsplits)), key=lambda b: logfreqs[b], reverse=True)
        subtrees = [subsplits[b] for b in order]
        logfreqs = [logfreqs[b] for b in order]
        return {'logfreqs': logfreqs, 'subtrees': subtrees}


def max_logfreq_tree(amalgam):
    max_logfreq = amalgam.max_tree_logfreq()
    logger.info('Max log-proba = %s', max_logfreq)
    features = amalgam.features
    CCP = amalgam.CCP
    full = amalgam.full

    rootsplit = features[full]['most_frequent'][0]
    tree = TreeNode()
    tree.add_feature('ccp', amalgam.CCP[rootsplit])

    queue = deque([(full, tree, rootsplit)])
    while queue:
        clust, parent, split = queue.pop()
        for subclust in split:
            child = parent.add_child(dist=amalgam.dist[subclust])
            if len(subclust) == 1:
                child.name = amalgam.labels[list(subclust)[0]]
            elif len(subclust) == 2:
                child.name = '%.2f' % (amalgam.freq[subclust] * 100,)
                child.support = amalgam.freq[subclust] * 100
                for elem in subclust:
                    child.add_child(name=amalgam.labels[elem], dist=amalgam.dist[amalgam.makeset((elem,))])
            else:
                child.name = '%.2f' % (amalgam.freq[subclust] * 100,)
                child.support = amalgam.freq[subclust] * 100
                childsplit = features[subclust]['most_frequent'][0]
                child.add_feature('ccp', CCP[childsplit])
                queue.append((subclust, child, childsplit))
    return tree


def first_logfreq_tree(amalgam):
    """Use the sorted logfreqs of subsplits (combine_sorted_proba_subtrees)"""
    features = amalgam.features
    CCP = amalgam.CCP

    rootsplit = features[amalgam.full]['subtrees'][0]
    tree = TreeNode()
    tree.add_feature('ccp', amalgam.CCP[rootsplit])

    queue = deque([(amalgam.full, tree, rootsplit)])
    tree_clusts = []
    while queue:
        clust, parent, split = queue.pop()
        tree_clusts.append(clust)
        for subclust in split:
            child = parent.add_child(dist=amalgam.dist.get(subclust, 0))
            if len(subclust) == 1:
                child.name = amalgam.labels[list(subclust)[0]]
            elif len(subclust) == 2:
                child.name = '%.2f' % (amalgam.freq[subclust] * 100,)
                child.support = amalgam.freq[subclust] * 100
                for elem in subclust:
                    child.add_child(name=amalgam.labels[elem], dist=amalgam.dist[amalgam.makeset((elem,))])
            else:
                child.name = '%.2f' % (amalgam.freq[subclust] * 100,)
                child.support = amalgam.freq[subclust] * 100
                childsplit = features[subclust]['subtrees'][0]
                child.add_feature('ccp', CCP[childsplit])
                queue.append((subclust, child, childsplit))
    return tree, tree_clusts


def closest_subsplits(amalgam, clusts=None, base_diff=0, skip_equal=False):
    if skip_equal:
        raise NotImplementedError('skip_equal is True')
    if clusts is None:
        clusts = amalgam.increasing_clusters
    features = amalgam.features
    diff_splits = []
    for clust in clusts:
        logfreqs = features[clust]['logfreqs']
        subtrees = features[clust]['subtrees']
        if len(subtrees) > 1:
            sub_i = 1
            while skip_equal and logfreqs[sub_i] == logfreqs[0] and sub_i < len(subtrees)-1:
                sub_i += 1
            #NOTE: building a list of (clust, sub_i) is convenient for extending it in top_logfreqs_trees
            diff_splits.append(( [(clust, sub_i)], base_diff + 10**logfreqs[0] - 10**logfreqs[sub_i] ))

    diff_splits.sort(key=lambda item: item[1])
    return diff_splits


def top_logfreq_trees(amalgam, n=20):
    """Most likely trees in decreasing order of frequency"""
    amalgam.walk(combine_sorted_proba_subtrees, amalgam, progress=False)
    features = amalgam.features

    if n<1:
        raise ValueError('n < 1')

    tree, tree_clusts = first_logfreq_tree(amalgam)
    ref_logfreq = features[amalgam.full]['logfreqs'][0]
    logger.info('Best tree logfreq=%g, remaining: %s', ref_logfreq, features[amalgam.full]['logfreqs'][1:])
    yield ref_logfreq, tree

    queue = closest_subsplits(amalgam, tree_clusts)
    seen_skipped = {}

    i = 1
    while i < n:
        logger.debug('%d sorted diffs: %s', len(queue), ' '.join(str(d) for _,d in queue[:30]))
        skipped, diff = queue.pop(0)
        if tuple(skipped) in seen_skipped:
            logger.warning('Current skipped %s diff %s was already seen with diff %s',
                           skipped, diff, seen_skipped[tuple(skipped)])
            continue
        else:
            seen_skipped[tuple(skipped)] = diff

        revert_subtrees = []
        revert_logfreqs = []
        skipped_clusts = set()
        # 1. Remove the subsplits to skip
        for clust, sub_i in skipped:
            skipped_clusts.add(clust)
            subtrees = list(features[clust]['subtrees'])
            logfreqs = list(features[clust]['logfreqs'])
            logger.debug('Skip %d in %r; next logfreq=%g; %d remaining splits', sub_i, ('full' if clust==amalgam.full else clust), logfreqs[sub_i], len(logfreqs) - sub_i)
            revert_subtrees.insert(0, subtrees[:sub_i])
            revert_logfreqs.insert(0, logfreqs[:sub_i])
            features[clust]['subtrees'] = subtrees[sub_i:]
            features[clust]['logfreqs'] = logfreqs[sub_i:]
        #FIXME: Example on PF01112_n71.pb1: If commented assert, the trees 26-41 are repeated.
        if not 10**features[clust]['logfreqs'][0] == 10**ref_logfreq - diff:
            logger.warning("Unexpected tree logfreq: logfreq, diff = %g, %g VS ref_logfreq=%g",
                           features[clust]['logfreqs'][0], diff, ref_logfreq)
        #else:

        i += 1
        # 2. recompute all parent clusters that depend from the changed clusters,
        #    and get the corresponding tree
        walked = amalgam.walk_from(skipped_clusts, combine_sorted_proba_subtrees, amalgam)
        logger.info('Walked %d clusters', len(walked))

        #amalgam.walk(combine_sorted_proba_subtrees, amalgam)
        tree, next_tree_clusts = first_logfreq_tree(amalgam)
        logger.info('Tree %d logfreq=%g skipped %s (from %d clusters)', i+2, features[amalgam.full]['logfreqs'][0], '+'.join(str(n) for _,n in skipped), len(skipped_clusts))
        yield features[clust]['logfreqs'][0], tree
        # compute the next differences:
        # first only investigate new clusters that appeared in the last tree:
        new_clusts = [cl for cl in next_tree_clusts if cl not in tree_clusts]
        new_clusts += list(skipped_clusts) + walked
        new_diff_splits = closest_subsplits(amalgam, new_clusts)
        tree_clusts = next_tree_clusts

        # 3. Insert the next skips in the correct order
        logger.debug(' first of next: %s, %g', *new_diff_splits[0])
        if new_diff_splits[0][1] + diff < queue[0][1]:
            logger.debug(' first next is smaller: %s < %s => extend %s', new_diff_splits[0][1] + diff, queue[0][1], skipped)
        for clust_subs, d in new_diff_splits:
            # clust_subs is something like: [(clust_0, sub_i_0), ...]
            clust0, sub_i_0 = clust_subs[0]
            if skipped[-1][0] == clust0:
                # Increment the discarding index. Not necessary, but makes the log easier to read.
                extended_skipped = skipped[:-1] + [(clust0, skipped[-1][1]+sub_i_0)] + clust_subs[1:]
            else:
                #logger.debug('Extend skipped with %s, next diff %s', clust_subs, d)
                extended_skipped = skipped + clust_subs
            queue.append((extended_skipped, diff + d))
        queue.sort(key=lambda item: item[1])

        # Revert back all clusters to the original state
        for (clust, _), first_subtrees, first_logfreqs in zip(reversed(skipped), revert_subtrees, revert_logfreqs):
            logger.debug(' - reinsert %d skipped before the %d retained', len(first_subtrees), len(features[clust]['subtrees']))
            features[clust]['subtrees'] = first_subtrees + features[clust]['subtrees']
            features[clust]['logfreqs'] = first_logfreqs + features[clust]['logfreqs']



class Amalgam:
    """
    Hierarchical structure representing nested splits.
    The child of a given clade are all alternative subsplits found in the original treelist
    """
    makeset = BitSet
    fmt_subsplit = staticmethod(FormatDistrib.fmt_subsplit)

    def __init__(self, bi, tri, ids=None, n=1):
        self.bi = bi
        self.tri = tri
        self.n = n  # Number of trees sampled.

        # Do not use Counter here: better raise KeyError if clust not found.
        self.clusts = {self.makeset(clust): count for split, count in bi.items() for clust in split if (count>0 and len(clust)>1)}

        # In tree language: 'root'
        for split, count in bi.items():
            if count > 0:
                self.full = self.makeset().union(*split)
                break
        else:
            # if tree is of size 3, there are no non-trivial splits.
            self.full = self.makeset().union(*(sub for subclust in tri for sub in subclust))
            logger.warning('Zero non-trivial bipartition, number of leaves inferred from tripartitions: %d ?', len(self.full))

        if not self.full:
            logger.error('No leaf found.')
        # Include trivial splits in inverted orientation (all-but-one member), because they have children subsplits
        #self.clusts.update({self.full.difference(self.makeset((k,))): n for k in self.full})

        self.ids = {k: k for k in self.full} if ids is None else ids
        self.labels = self.ids if ids is None else {k: label for label,k in ids.items()}

        if self.full in self.clusts:
            raise ValueError('The full set cannot represent a bipartition')

        self._compute_freqs()
        self.features = {clust: {} for clust in self.clusts}  # cluster features

    def aggregate_edge_lengths(self, dists, aggfunc=median):
        self._dist_agg_func = aggfunc
        self.dist = {}
        if dists is not None:
            for clust, dist in dists.items():
                if clust in self.clusts or (len(clust)==1 and self.full.difference(clust) in self.clusts):
                    validdist = [x for x in dist if x is not None]
                    self.dist[clust] = aggfunc(validdist) if validdist else None
                elif dist:
                    logger.error('cluster has %d edge lengths but not found: %r', len(dist), clust)
                try:
                    assert len(dist) == self.clusts.get(clust, 0), '%d dists != %d occurrences of %r' % (len(dist), self.clusts.get(clust, 0), clust)
                except AssertionError:
                    assert len(clust) == 1 and len(dist) == self.clusts.get(self.full.difference(clust), 0)

    def translate(self, clust):
        return tuple(self.labels[elem] for elem in clust)

    def encode_labeled_clust(self, clust):
        return self.makeset(map(self.ids.__getitem__, clust))

    def encode_labeled_split(self, split):
        return frozenset((self.makeset(map(self.ids.__getitem__, clust))
                          for clust in split))

    def _compute_freqs(self):
        #TODO: Discard low frequency splits and their tripartitions
        self.freq = {clust: count/self.n for clust, count in self.clusts.items()}
        self.terminals = []  # All clusters that can be used to start a postorder traversal
                             # (so all clusters of size 2)
        self.subsplits = {clust: [] for clust in self.clusts}  # 'children'
        self.subsplits[self.full] = []
        self.unions = {}                                       # 'parent'
        #self.joins = {} clusters that can be split to produce a given clust
        for split,count in self.bi.items():
            if count:
                self.subsplits[self.full].append(split)
                self.unions[split] = self.full

        self.CCP = {split: count/self.n for split, count in self.bi.items() if count}

        for subsplit, count in self.tri.items():
            clust = self.makeset().union(*subsplit)
            if len(clust) < 2:
                raise ValueError('Clade size must be > 1')
            elif len(clust) == 2:
                self.terminals.append(clust)
            try:
                self.subsplits[clust].append(subsplit)
            except KeyError:
                if clust.difference(self.full):
                    raise ValueError('Invalid elements contained in subsplit: %r', clust.difference(self.full))
                raise ValueError('Missing parent cluster of size %d for subsplit %s with count=%d' % (len(clust), subsplit, count))
            self.unions[subsplit] = clust
            #for sub in subsplit:
            #    self.joins[sub].append(clust)
            try:
                self.CCP[subsplit] = count / self.clusts[clust]
            except ZeroDivisionError as err:
                err.args = (err.args[0] + 'at clust=%s' % ','.join(str(x) for x in clust),)
                raise

    def verify_freqs(self):
        """Verify that the total count of subsplits equals the count of the parent cluster"""
        for i, clust in enumerate(self.increasing_clusters):
            if len(clust) == 1:
                raise ValueError('Invalid: clusters of size 1 cannot have subsplits')
            elif len(clust) == 2:
                if not self.subsplits[clust]:
                    # It's tolerated to omit this frequency, as it must be equal to the number of trees anyway.
                    continue
                elif len(self.subsplits[clust]) != 1:
                    raise ValueError('There can only be one subsplits for a cluster of size 1')
                #FIXME: should it be ensured that the trivial subsplit is present?
            s = sum(self.tri[subsplit] for subsplit in self.subsplits[clust])
            if s != self.clusts[clust]:
                logger.error('Invalid sum at %s -> subsplits %s; labels: %s',
                             ','.join(map(str, clust)),
                             ' '.join(map(self.fmt_subsplit, self.subsplits[clust])),
                             ', '.join(str(x) for x in self.translate(clust)))
                # if len(clust) == n - 1
                msg = 'clust %i : %g subsplits counts != %g counts' % (i, s, self.clusts[clust])
                raise ValueError(msg)

    def thin_low_freq(self, split_lim=0.1, sub_lim=0.5, min_subsplits=2):
        """Discard low frequency splits and subsplits:
        - discard splits under split_lim;
        - keep the most likely subsplits that account for at least sub_lim of the parent cluster.
        """
        n_discarded_subsplits = 0
        # First the splits to discard
        to_discard = [clust for clust, freq in self.freq.items() if freq<split_lim]
        initial_discard = to_discard
        coincident_discards = []
        discard_round = 0
        while to_discard:
            discard_round += 1
            logger.info('Split discard round #%d (%d to discard)', discard_round, len(to_discard))
            next_coincident = set()
            for clust in to_discard:
                # To delete: any subsplit containing clust on one side
                for nextclust in [self.full] + sorted(set(self.clusts).difference(initial_discard+coincident_discards), key=len, reverse=True):
                    if len(nextclust) <= len(clust):
                        break  # cannot contain clust.
                    discard_subsplits = [split for split in self.subsplits[nextclust] if clust in split]
                    # If it would remove the only subsplit of a cluster,
                    # also remove that cluster
                    if len(discard_subsplits) == len(self.subsplits[nextclust]):
                        next_coincident.add(nextclust)
                        #FIXME: add self.full.difference(nextclust)
                    else:
                        for split in discard_subsplits:
                            del self.CCP[split]
                            del self.unions[split]
                            self.subsplits[nextclust].remove(split)
                        n_discarded_subsplits += len(discard_subsplits)
                        #TODO: check the CCP[discarded_subsplit]
            to_discard = list(next_coincident)
            coincident_discards += to_discard
            redundant_discard = set(initial_discard).intersection(coincident_discards)
            if redundant_discard:
                first_match = list(redundant_discard)[0]
                raise RuntimeError("%d redundant discard at round %d: %r, size %d, at index %d, complement: %r" % (
                    len(redundant_discard), discard_round,
                    first_match, len(first_match),
                    coincident_discards.index(first_match),
                    self.full.difference(first_match)))

        for clust in initial_discard + coincident_discards:
            try:
                del self.freq[clust]
                del self.clusts[clust]
                del self.features[clust]
                del self.subsplits[clust]
            except KeyError as err:
                msg = repr(err.args[0])
                if clust in coincident_discards:
                    msg += ' (coindident)'
                else:
                    msg += ' (%d initial)' % initial_discard.index(clust)
                err.args = (msg,)
                raise
            if len(clust) == 2:
                self.terminals.remove(clust)
        for clust in sorted(self.clusts, key=len, reverse=True):
            if len(clust) <= 2:
                break
            # Filter the subsplits
            sorted_subsplits = sorted(self.subsplits[clust], key=self.CCP.__getitem__)
            retained_subsplits = []
            retained_CCP = 0
            while retained_CCP < sub_lim or len(retained_subsplits) < min_subsplits:
                try:
                    subsplit = sorted_subsplits.pop()
                except IndexError:
                    break
                retained_subsplits.append(subsplit)
                retained_CCP += self.CCP[subsplit]
            self.subsplits[clust] = retained_subsplits
            for discarded_sub in sorted_subsplits:
                del self.CCP[discarded_sub]
                del self.unions[discarded_sub]
            n_discarded_subsplits += len(sorted_subsplits)
        self.thinned = len(initial_discard), len(coincident_discards), n_discarded_subsplits
        try:
            delattr(self, '_increasing_clusters')
        except AttributeError:
            pass
        # Verify that postorder is possible
        for clust, subsplits in self.subsplits.items():
            if not subsplits and len(clust)>1:
                raise RuntimeError('Removed all subsplits from cluster %r' % clust)
            for split in subsplits:
                for subclust in split:
                    if len(subclust)>1:
                        errmsg = "missing subclust %r (clust=%r)" % (subclust, clust)
                        if subclust in coincident_discards:
                            errmsg += ' (coincident del)'
                        assert subclust in self.clusts, errmsg
                        assert subclust in self.freq, errmsg
                        assert subclust in self.features, errmsg


    @property
    def increasing_clusters(self):
        try:
            return self._increasing_clusters
        except AttributeError:
            self._increasing_clusters = sorted(self.clusts, key=len)
            return self._increasing_clusters

    def postorder(self):
        """"""
        for clust in self.increasing_clusters:
            for subclust in self.subsplits[clust]:
                yield clust, subclust


    def max_tree_logfreq(self):
        """Return the amalgamated tree that maximizes its clades frequency"""
        try:
            return self.features[self.full]['max_logfreq']
        except KeyError:
            self.walk(combine_most_frequent_subsplit, self, progress=False)
        return self.features[self.full]['max_logfreq']


    def init_leaves(self, init, *args, **kwargs):
        for k in self.full:
            leaf = self.makeset((k,))
            leaffeatures = self.features.setdefault(leaf, dict())
            leaffeatures.update(init(leaf, *args, **kwargs))

    def walk(self, combine, *args, checkmem=False, progress=True, **kwargs):
        if progress:
            logprogress = logger.info
        else:
            def logprogress(*a): pass
        if checkmem:
            # Measure how much memory the features are using
            from .object_memory import total_size
            current_mem = 0

            def monitor_mem(clustfeatures):
                nonlocal current_mem
                current_mem += total_size(clustfeatures)
                gigs, remain = divmod(current_mem, (1024**3))
                megs, remain = divmod(remain, 1024**2)
                return gigs, megs, round(remain/1024)

            def log_mem(mem_units):
                logger.info('Features Memory Usage: %dG %dM %dK', *mem_units)

            if checkmem is True:
                # No memory limit
                memlimit = (1000, 0)  # 1000G
            elif checkmem.endswith('M'):
                memlimit = (0, int(checkmem[:-1]))
            elif checkmem.endswith('G'):
                memlimit = (int(checkmem[:-1]), 0)
            else:
                raise ValueError('Invalid checkmem argument: %r', checkmem)

            def limit_mem(mem_units):
                """stop the program if memory usage is above limit"""
                gigs, megs, _ = mem_units
                if gigs > memlimit[0] or (gigs==memlimit[0] and (megs+1) > memlimit[1]):
                    raise RuntimeError('Memory limit %sG %sM exceeded' % memlimit)
        else:
            def monitor_mem(a): pass
            def log_mem(a): pass
            def limit_mem(a): pass

        n_clusts = len(self.increasing_clusters) + 1  # +1 for the root cluster
        for i, clust in enumerate(self.increasing_clusters):
            # Note: the combine function requires access to self.features
            estimated = combine(self.subsplits[clust], clust, *args, **kwargs)
            oldkeys = set(self.features[clust])
            if oldkeys.intersection(estimated):
                logger.warning('Overwritten variables in features[clust]')
            self.features[clust].update(estimated)
            mem_units = monitor_mem(self.features[clust])
            if round(i/n_clusts * 100) % 5 == 0:
                logprogress('Walked %2d %% - clust size %d', (i/n_clusts)*100, len(clust))
                log_mem(mem_units)
            limit_mem(mem_units)

        # Combine all rootings
        #for k in self.full:
        #    if (self.makeset((k,)), self.full.difference(self.makeset((k,)))) not in splits:
        #       logger.warning('Missing trivial split')
        #TODO: verify
        estimated = combine(self.subsplits[self.full], self.full, *args, **kwargs)
        rootfeatures = self.features.setdefault(self.full, {})
        rootfeatures.update(estimated)
        log_mem(monitor_mem(rootfeatures))

    def walk_from(self, start, combine, *args, **kwargs):
        """
        Rerun the pruning algorithm from the start point (excluded).

        param 'start':
            - if integer, recompute all clusters of larger size;
            - if a cluster, recompute all of them and clusters that depend from them.
        """
        if isinstance(start, int) and not isinstance(start, BitSet):
            def retain(clust):
                return len(clust) >= start
        else:
            # start is an upstream cluster. We search for dependent parents
            updated = list(start) if isinstance(start, (list, set, tuple)) else [start]
            start_size = min(updated, key=len)

            def retain(clust):
                nonlocal updated
                if len(clust) <= start_size or clust in updated:
                    return False
                for up in updated:
                    for split in self.subsplits[clust]:
                        if up in split:
                            updated.append(clust)
                            return True
                return False

        walked = []
        for clust in self.increasing_clusters + [self.full]:
            if not retain(clust):
                continue
            # Note: the combine function requires access to self.features
            estimated = combine(self.subsplits[clust], clust, *args, **kwargs)
            self.features[clust].update(estimated)
            walked.append(clust)

        return walked

    def write(self, filename=None, prec=4):
        out = stdout if filename is None else open(filename, 'w')
        try:
            out.write('# translate\n')
            for k, name in sorted(self.labels.items()):
                if name != k:
                    out.write('%s\t%s\n' % (name, k))

            out.write('# bipartitions\n')
            for clust in self.increasing_clusters:
                out.write('%s\t%.*f\n' % (','.join(str(k) for k in clust), prec, self.freq[clust]))

            out.write('# tripartitions\n')
            for clust in self.increasing_clusters:
                for subsplit in self.subsplits[clust]:
                    out.write('%s\t%.*f\n' % (self.fmt_subsplit(subsplit), prec, self.CCP[subsplit]))
        finally:
            if filename is not None:
                out.close()


class FrozenAmalgam(Amalgam):
    makeset = staticmethod(frozenset)
    fmt_subsplit = staticmethod(FrozenFormatDistrib.fmt_subsplit)


def clusters_from_ete3(newick, format=1):
    tree = Tree(newick, format=format)
    nodeleaves = {}

    for node in tree.traverse('postorder'):
        if node.is_leaf():
            nodeleaves[node] = [node.name]
        else:
            nodeleaves[node] = []
            for child in node.children:
                nodeleaves[node] += nodeleaves[child]
            yield nodeleaves[node]


def index_elements(*clusters, ids=None):
    if ids is None:
        ids = {}
        next_id = 0
    else:
        next_id = max(ids.values())

    indexed_clusters = []
    for clust in clusters:
        clust_ids = []
        for leafname in clust:
            try:
                k = ids[leafname]
            except KeyError:
                k = ids[leafname] = next_id
                next_id += 1
            clust_ids.append(k)
        indexed_clusters.append(clust_ids)
    return ids, indexed_clusters


def include_trivial_subsplits(distrib_sub, distrib, unrooted=True, setgroup=bitset_pack):
    """
    Add the subsplits of clusters of size 2. #FIXME

    This is not needed if you are using ClusterPool to build the distribs.
    It's mostly useful if you are declaring the distrib by hand, and want to
    automatically complete obvious subsplits.
    """
    for split, count in distrib.items():
        for clust in split:
            if len(clust) == 2:
                distrib_sub[setgroup(((k,) for k in clust))] = count

    #trivialcounter = Counter()
    #for split, count in distrib_sub.items():
    #    for subclust in split:
    #        if len(subclust) == 2:
    #            trivialcounter[setgroup(((k,) for k in subclust))] += count
    #factor = 0.5 if unrooted else 1
    #for trivialsplit, count in trivialcounter.items():
    #    if trivialsplit in distrib_sub:
    #        assert distrib_sub[trivialsplit] == count * factor, '%s split count %s != %s subsplit sum' % (trivialsplit, distrib_sub[trivialsplit], count*factor)
    #    else:
    #        distrib_sub[trivialsplit] = count*factor


def include_trivial_splits(distrib, rootclust, n, setgroup=bitset_pack):
    for k in rootclust:
        distrib[setgroup(((k,), rootclust.difference((k,))))] = n


def unrooted_complement_tripartitions(distrib_sub: Counter, rootclust, keywrap=frozenset):
    """keywrap defines the way iterable keys are built.

    if keywrap=frozenset, it ensures that the order of subelements inside the key does not matter.
    if keywrap=sortuple, it ensures that the key subelements are always sorted consistently.
    """
    polytomic_root_subsplits = False  # Logging
    for subclust, count in list(distrib_sub.items()):
        try:
            outgroup = rootclust.difference(*subclust)
        except TypeError as err:
            err.args = (err.args[0] + ' with value %r', *err.args[1:])
            raise
        if outgroup:
            for sub in subclust:
                distrib_sub[keywrap((sub, outgroup))] += count
        else:
            # The root partition has no outgroup, but possibly defines
            # additional tripartitions
            polytomic_root_subsplits = True
            if len(subclust) == 2:
                # This is a root bipartition, therefore not a tripartition. Remove.
                logger.error('Invalid tripartition %r (rootclust=%r)', subclust, rootclust)
                raise RuntimeError("dichotomic root partition is not expected in distrib_sub", subclust)
            elif len(subclust) > 2:
                for subroot_pair in combinations(subclust, 2):
                    distrib_sub[keywrap(subroot_pair)] += count
                if len(subclust) > 3:
                    logger.warning('Root is partitioned into %d > 3 clusters. Expected: tripartition or bipartition.',
                                   len(subclust))
                    for subroot_pair in combinations(subclust, 2):
                        #NOTE: By symmetry, this changes nothing if len(subclust) == 4
                        distrib_sub[subclust.difference(subroot_pair)] += count
            del distrib_sub[subclust]
    if polytomic_root_subsplits:
        logger.info('Fixed polytomic root subsplits.')


def unrooted_complement_dists(distrib_dist: Counter, rootclust):
    """
    Expand the edge lengths of each cluster from its complement.
    """
    #NOTE: rather inefficient storage (twice for each split, but it's convenient to index by each clust)
    seen_outgroups = set()
    for subclust in list(distrib_dist):
        if subclust in seen_outgroups:
            continue
        try:
            outgroup = rootclust.difference(subclust)
        except TypeError as err:
            err.args = (err.args[0] + ' with value %r', *err.args[1:])
            raise
        if not outgroup:
            raise ValueError('Unexpected: the full cluster cannot be have a distance.')
        distrib_dist[subclust] += distrib_dist[outgroup]
        distrib_dist[outgroup] = distrib_dist[subclust]
        seen_outgroups.add(outgroup)
        #TODO: verify: len(dists) == distrib[subclust]


class ClusterPool:
    """Count bipartitions and tripartitions from a sample of trees"""
    setgroup = staticmethod(bitset_pack)  # This uses much less RAM than 'frozen_pack' but is slower
    keywrap = staticmethod(frozenset)
    makeset = BitSet

    def __init__(self, treelist, burnin=0, ids=None, nsamples=None, thin=1, allow_polytomies=False):
        self.burnin = burnin
        self.ids = ids
        self.nsamples = nsamples

        # Load the first tree to initialize the distribution and check input
        if isinstance(treelist, (str,bytes)):
            raise ValueError('treelist should be an iterable of strings')
        itertrees = iter(treelist)
        n = -1
        try:
            if burnin:
                first_tree = next(itertrees).strip()  # check input validity
                if not (first_tree.startswith('(') and first_tree.endswith(';')
                        and first_tree.count('(') == first_tree.count(')')):
                    raise ValueError('Invalid newick string: %r' % first_tree)
            for n in range(burnin-1):
                next(itertrees)
            # Process the first tree to build the index
            first_newick_clusters = IterNewickClusters(next(itertrees), ids)
        except StopIteration:
            raise ValueError('Not enough trees (%d) for burnin %d' % (n+1, burnin))

        clusters, subclustdists = tuple(zip(*first_newick_clusters))
        if len(first_newick_clusters.root_level) > 2:
            subclustdists += (first_newick_clusters.root_level,)

        distrib_sub = Counter()
        distrib_dist = defaultdict(list)

        if ids is None:
            ids, (rootclust,) = index_elements(first_newick_clusters.root_clust)
            rootclust = frozenset(rootclust)
            clusters = [tuple(map(ids.__getitem__, clust)) for clust in clusters]
            # Also reindex the cluster element in subclustdists and root_level
            subclustdists_indexed = []
            for subclustdist in subclustdists:
                subclustdists_indexed.append([(frozenset(map(ids.__getitem__, cl)), d) for cl,d in subclustdist])
            subclustdists = subclustdists_indexed
            root_level = [(frozenset(map(ids.__getitem__, cl)), d) for cl,d in first_newick_clusters.root_level]
            self.ids = ids
        else:
            rootclust = frozenset(first_newick_clusters.root_clust)
            root_level = first_newick_clusters.root_level

        for subclustdist in subclustdists:
            if self.setgroup == bitset_pack and not all(isinstance(k, int) for item in subclustdist for k in item[0]):
                raise TypeError('Integer IDs are required with bitset_pack')

            subsplit = []

            for subclust, subdist in subclustdist:
                try:
                    distrib_dist[self.makeset(subclust)].append(subdist if subdist is None else float(subdist))
                except ValueError as err:
                    err.args = (err.args[0], 'At subclust=%r' % self.makeset(subclust))
                    raise
                subsplit.append(subclust)

            logger.debug('Increment subsplit %s', subsplit)
            distrib_sub[self.setgroup(subsplit)] += 1
        if len(root_level) == 2:
            # For a dichotomic root, subroot edges must also be included.
            rootsplit = []
            for clust, dist in root_level:
                distrib_dist[self.makeset(clust)].append(None if dist is None else float(dist))
                rootsplit.append(clust)
            logger.debug('rooted dichotomic root edges added %s', rootsplit)
            self.first_rootsplit = self.setgroup(rootsplit)
        else:
            self.first_rootsplit = self.setgroup([clust for clust,_ in root_level])

        distrib = Counter()
        for clust in clusters:
            # Each split can only exist once in a tree:
            # Note: if the root is a dichotomy, each root child identifies the same split;
            # it will only be counted once below.
            if len(clust) < len(rootclust) - 1:
                distrib[self.setgroup((clust, rootclust.difference(clust)))] = 1
            # Otherwise, it defines a trivial split (one-leaf clade), so skip it.

        n_splits = sum(distrib.values())  # Remember, clusters of size 1 (trivial split) are skipped
        n_subsplits = sum(distrib_sub.values())  # only contains rooted subsplits
        if not allow_polytomies:
            assert n_splits == (len(rootclust) - 3), "%d splits found, expected (%d - 3) non-trivial, assuming a dichotomic tree" % (n_splits, len(rootclust))
            #if len(first_newick_clusters.root_level) == 2:
            assert n_subsplits == (len(rootclust) - 2), "%d subsplits found, expected (%d - 2) rooted" % (n_subsplits, len(rootclust))
            #else:
            #    assert n_subsplits == (len(rootclust) - 3), "%d subsplits found, expected (%d - 3) rooted" % (n_subsplits, len(rootclust))
        self.rootclust = rootclust
        self.distrib = distrib
        self.distrib_sub = distrib_sub
        self.distrib_dist = distrib_dist
        self.thin = thin
        self.enumtrees = enumerate(itertrees, start=2) if nsamples is None else zip(range(2, 1+nsamples), itertrees)
        if thin != 1:
            self.enumtrees = islice(self.enumtrees, 0, None, thin)
        self.n = 1  # Only here to avoid NameError in case there are no more trees.
        # All the other trees

    def count_subsplits(self, unrooted=True, trivial_splits=False, ignorebad=False, drop_ids=None):
        n = self.n
        # Fix the subroot edge lengths when unrooted=True and the root was dichotomic:
        if unrooted:
            # Find the rootsplit:
            # FIXME: do that in unroot_complement_dist (problem: finding the corresponding rootsplits)
            rootsplit = self.first_rootsplit
            if len(rootsplit) == 2:
                assert rootsplit not in self.distrib_sub
                rootdists = []
                for cl in rootsplit:
                    dist = self.distrib_dist[cl].pop()
                    if dist is not None:
                        rootdists.append(float(dist))
                joined_dist = sum(rootdists) if rootdists else None
                #logger.debug('Dichotomic root edge update %r dist=%s current: %s %s', rootsplit, joined_dist, *(distrib_dist[cl] for cl in rootsplit))
                self.distrib_dist[list(rootsplit)[0]].append(joined_dist)
                # The dist of the outgroup will be extended in unrooted_complement_dists

        if drop_ids:
            # Fix the counts of the first parsed tree
            self.rootclust = self.rootclust.difference(drop_ids)
            new_distrib = Counter()
            new_distrib_sub = Counter()
            new_distrib_dist = defaultdict(list)  #TODO
            minsize = 1 if trivial_splits else 2
            for split in self.distrib:
                newsplit = self.setgroup([cl.difference(drop_ids) for cl in split])
                if all(len(cl) >= minsize for cl in newsplit):
                    if newsplit not in new_distrib:
                        new_distrib[newsplit] = 1
            for subsplit in self.distrib_sub:
                newsubsplit = self.setgroup([cl.difference(drop_ids) for cl in subsplit])
                outgroup = self.rootclust.difference(*newsubsplit)
                if all(len(cl)>0 for cl in newsubsplit) and len(outgroup)>0:
                    if newsubsplit not in new_distrib_sub:
                        new_distrib_sub[newsubsplit] = 1
            for clust, dists in self.distrib_dist.items():
                # In a single tree, if several clusters reduce to the same new cluster,
                # their branch length should be *additionned*.
                if len(dists) > 1:
                    # Should not occur, except at rootclust(?)
                    logger.error('Unexpected: more than one branch lengths (%d) at clust %r', len(dists), clust)
                newdists = new_distrib_dist.setdefault(clust.difference(drop_ids), [0])
                newdists[0] += dists[0]
            self.distrib = new_distrib
            self.distrib_sub = new_distrib_sub
            self.distrib_dist = new_distrib_dist

            def setup_iternewick(newick, ids=None):
                return IterNewickClustersPartial(newick, drop_ids, ids=ids)
        else:
            setup_iternewick = IterNewickClusters

        distrib = self.distrib
        distrib_sub = self.distrib_sub
        distrib_dist = self.distrib_dist

        ignored = 0

        for n, newick in self.enumtrees:
            if not n % 5000:
                logger.info('Processed %d trees', n)
            newick_clusters = setup_iternewick(newick, self.ids)
            try:
                iterated = list(newick_clusters)
                # Interestingly, loading it into a list beforehand does not slow the execution (even accelerates)
            except BaseException as err:
                err.args = ((err.args[0]+" " if err.args else '') + "at newick #%d" % n,) + err.args[1:]
                if not ignorebad or isinstance(err, KeyboardInterrupt):
                    raise
                ignored += 1
                logger.error(str(err))
                continue
            for clust, subclust in iterated:
                distrib[self.setgroup((clust, self.rootclust.difference(clust)))] += 1
                subsplit = self.setgroup([item[0] for item in subclust])
                #logger.debug('Increment subsplit %s', subsplit)
                distrib_sub[subsplit] += 1  # set of sets.
                for sub, (_, subdist) in zip(subsplit, subclust):
                    distrib_dist[sub].append(subdist if subdist is None else float(subdist))
            if len(newick_clusters.root_level) == 2:
                # dichotomic root
                rootsplit = self.setgroup([item[0] for item in newick_clusters.root_level])
                #logger.debug('Dichotomic root: the root bipartition should be added only once. rootsplit = %s', rootsplit)
                if newick_clusters.has_full_clust:
                    distrib[rootsplit] -= 1
                if unrooted:
                    rootdists = [float(dist) for _,dist in newick_clusters.root_level if dist is not None]
                    joined_dist = sum(rootdists) if rootdists else None
                    distrib_dist[list(rootsplit)[0]].append(joined_dist)
                else:
                    rootsplit = []
                    rootdists = []
                    for item in sorted(newick_clusters.root_level, key=lambda item: tuple(item[0])):
                        rootsplit.append(item[0])
                        rootdists.append(item[1])
                    rootsplit = self.setgroup(rootsplit)
                    #logger.debug('rooted dichotomic root edges added %s : %s', rootsplit, rootdists)
                    for rcl, dist in zip(sorted(rootsplit, key=tuple), rootdists):
                        distrib_dist[rcl].append(None if dist is None else float(dist))

            else:
                #assert frozensplit(subclust) != frozensplit(newick_clusters.root_level)
                #if not newick_clusters.has_full_clust:
                #logger.debug('Polytomic root: the root partition is added as subcluster. root_level = %s', newick_clusters.root_level)
                rootsplit = self.setgroup([item[0] for item in newick_clusters.root_level])
                distrib_sub[rootsplit] += 1
                for sub, (_, subdist) in zip(rootsplit, newick_clusters.root_level):
                    distrib_dist[sub].append(subdist if subdist is None else float(subdist))
                # this is updated below

        self.n = n - ignored

        # Complement the tripartitions. Unnecessary if the tree is considered rooted.
        if unrooted:
            self.unrooted_complement_tripartitions()
            #if self.withdist:
            self.unrooted_complement_dists()
        #else: Warning: the edge length of some subroots are missing.

        if trivial_splits:
            self.add_trivial_splits()

        return distrib, distrib_sub, distrib_dist

    def add_trivial_splits(self):
        for k in self.rootclust:
            self.distrib[self.setgroup(((k,), self.rootclust.difference((k,))))] = self.n

    def unrooted_complement_tripartitions(self):
        try:
            unrooted_complement_tripartitions(self.distrib_sub, self.makeset(self.rootclust), self.keywrap)
        except RuntimeError as err:
            subclust = err.args[1]
            err.args += ('tri count=%g ; bi count=%g' % (self.distrib_sub[subclust], self.distrib[subclust]),)
            raise

    def unrooted_complement_dists(self):
        unrooted_complement_dists(self.distrib_dist, self.makeset(self.rootclust))

    def count_bipartitions(self, trivial_splits=False, ignorebad=False):
        n = self.n
        distrib = self.distrib
        ignored = 0
        for n, newick in self.enumtrees:
            if not n % 5000:
                logger.info('Processed %d trees', n)
            newick_clusters = IterNewickClusters(newick, self.ids)
            try:
                for clust, _ in newick_clusters:
                    distrib[self.setgroup((clust, self.rootclust.difference(clust)))] += 1
                if len(newick_clusters.root_level) == 2:
                    # dichotomic root
                    #logger.debug('Dichotomic root: the root bipartition should be added only once. root_level = %s', newick_clusters.root_level)
                    if newick_clusters.has_full_clust:
                        distrib[self.setgroup([item[0] for item in newick_clusters.root_level])] -= 1
            except BaseException as err:
                err.args = ((err.args[0]+" " if err.args else '') + "at newick #%d" % n,) + err.args[1:]
                if not ignorebad or isinstance(err, KeyboardInterrupt):
                    raise
                ignored += 1
                logger.error(str(err))
        self.n = n - ignored
        if trivial_splits:
            for k in self.rootclust:
                distrib[self.setgroup(((k,), self.rootclust.difference((k,))))] = n

        return distrib


class FrozenClusterPool(ClusterPool):
    setgroup = staticmethod(frozen_freeze)
    keywrap = staticmethod(frozenset)
    makeset = staticmethod(frozenset)

    def unrooted_complement_dists(self):
        unrooted_complement_dists(self.distrib_dist, self.rootclust)


def repr_large_num(k: int):
    """Avoid OverflowError when trying to represent large int as float.
    Convert to approximate scientific notation e.g 1.2e300"""
    if k==0:
        return '0'
    elif k < 0:
        negative = True
        k = -k
    else:
        negative = False
    unit, exponent = modf(log(k, 10))
    if exponent < 6:
        return ('-%d' % k) if negative else ('%d' % k)
    outformat = '%.4fe%+d'
    if negative:
        outformat = '-' + outformat
    return outformat % (10**unit, int(exponent))


def number_of_possible_subsplits(N, given_split_counts=None, nsamples=None):
    """Number of possibilities to select two non-empty disjoint sets from N ids"""
    possibilities = 0
    if given_split_counts is None:
        # Alternative method
        for k in range(2,N):
            # iterate over all sizes of parent cluster;
            # Number of such clusters:
            nclusts = comb(N,k)
            # Number of bipartitions into non-empty sets of the parent:
            nsubsplits = sum(comb(k,i) for i in range(1,k//2))
            if k % 2 == 0:
                nsubsplits += comb(k, k//2) // 2
            else:
                nsubsplits += comb(k, k//2)
            possibilities += nclusts * nsubsplits
    else:
        tot_count = 0  # number of valid splits
        trivial = 0
        trivial_count = 0
        for split, count in given_split_counts.items():
            if len(split) != 2:
                logger.warning('Not a dichotomic split, skip')
                continue
            if sum(map(len, split)) != N:
                logger.warning('Not a partition of the full set, skip')
                continue
            tot_count += count
            for clust in split:
                k = len(clust)
                if k == 1:
                    if count:
                        if nsamples is not None and count != nsamples:
                            logger.error('trivial split has to be found in every tree but count = %d', count)
                        trivial += 1
                        trivial_count += count
                else:
                    nsubsplits = sum(comb(k,i) for i in range(1,k//2))
                    nsubsplits += comb(k, k//2) // (2 if k%2==0 else 1)
                    possibilities += min(count, nsubsplits)
        # Include trivial splits (single-leaf):
        if trivial > N:
            raise ValueError('trivial count %d > N' % trivial)
        elif trivial < N:
            if trivial:
                logger.warning('%d trivial splits referenced but expected N (%d)', trivial, N)
            k = N-1
            nsubsplits = sum(comb(k,i) for i in range(1, k//2))
            nsubsplits += comb(k, k//2) // (2 if k%2==0 else 1)
            if nsamples is not None:
                nsubsplits = min(nsubsplits, nsamples)
            possibilities += (N - trivial) * nsubsplits
        else:
            logger.warning('%d trivial splits are included' % trivial)

        if nsamples is not None and tot_count - trivial_count != nsamples * (N-3):
            raise ValueError('total split counts %d != %d nsamples x (N-3)' % (
                             tot_count - trivial_count, nsamples * (N-3)))
    return possibilities


def check_expected_split_counts(n_trees, rootclust, count_bi, count_tri):
    n = n_trees
    idlist = list(rootclust)
    N = len(rootclust)
    total_splits = sum(count_bi.values())
    expected_nontrivial = (N-3) * n
    (most_common, max_count), = count_bi.most_common(1)
    logger.info('Total: %d splits in %d trees of %d elements (expect %d non-trivial). Maximum id: %d',
                total_splits, n, N, expected_nontrivial, max(idlist))
    logger.info('Unique clusters: %d. (< %s)', len(count_bi), repr_large_num(2**N - N - 1))  # maximum possible is 2^n - n - 1
    logger.info('Most common cluster: count=%d size=%d', max_count, min(map(len, most_common)))
    assert max_count <= n, 'max_count = %d > number of trees %d, split=%r' % (max_count, n, most_common)

    total_subsplits = sum(count_tri.values())
    expected_total = 3 * (N-2) * n
    max_uniq = number_of_possible_subsplits(N)
    max_uniq_from_givensplits = number_of_possible_subsplits(N, count_bi, n)
    (most_common, max_count), = count_tri.most_common(1)
    logger.info('\nSubsplits: %d subsplits (expected total rooted = %d, unrooted = %d).', total_subsplits, expected_total/3, expected_total)
    logger.info('Unique subsplits: %d. Possibilities = %s, of which %s realisable from splits',
            len(count_tri), repr_large_num(max_uniq), repr_large_num(max_uniq_from_givensplits))
    logger.info('Most common subsplit: count=%d clust size=%d', max_count, sum(map(len, most_common)))
    assert max_count <= n, 'max_count = %d > number of trees' % max_count


def build_amalgam(treelist, **kwargs):
    if isinstance(treelist, (tuple,list)):
        iter_newicks = iter(treelist)
    else:
        iter_newicks = iter_multinewick(treelist)
    clusterpool = ClusterPool(iter_newicks, **kwargs)
    count_bi, count_tri, dists = clusterpool.count_subsplits(trivial_splits=True, ignorebad=True)
    n = clusterpool.n
    ids = clusterpool.ids

    #{clust_union = set().union(*((s1 | s2) for s1,s2 in count_bi.keys()))
    #if not all(isinstance(k, int) for k in clust_union):
    #    logger.warning('Non integer ids as members: %s', ', '.join(str(k) for k in clust_union if not isinstance(k, int)))
    check_expected_split_counts(n, clusterpool.rootclust, count_bi, count_tri)

    amalgam = Amalgam(count_bi, count_tri, ids, n)
    amalgam.aggregate_edge_lengths(dists)
    del dists
    # Checks: the total sum should equal: number of splits × number of trees
    return amalgam


def bpcomp(treefiles, burnin=0.05, min_freq=0.5, ignorebad=False):
    ntrees, burnins = multi_treelist_burnin(treefiles, burnin)
    logger.info('burnins = %s; treefiles = %s', burnins, ', '.join(treefiles))
    # Obtain the list of ids
    clusterpool = ClusterPool(list(iter_multinewick(treefiles[0])), burnins[0])
    distrib = clusterpool.count_bipartitions(ignorebad=ignorebad)
    ids = clusterpool.ids

    distribs = [distrib]
    maxdiffs, meandiffs = [], []

    logger.info('Reading %d remaining treelists', len(treefiles)-1)
    for b, treefile in zip(burnins[1:], treefiles[1:]):
        distrib = ClusterPool(list(iter_multinewick(treefile)), b, ids=ids).count_bipartitions(ignorebad=ignorebad)
        distribs.append(distrib)

    # Compare all pairs of chains. phylobayes bpcomp does differently: it compares each split across all chains. I don't bother because I will only ever compare 2 chains anyway
    logger.info('Compare all pairs')
    for i, j in combinations(range(len(distribs)), 2):
        d1, d2 = distribs[i], distribs[j]
        n1, n2 = ntrees[i] - burnins[i], ntrees[j] - burnins[j]
        diffs = []
        weights = []  # average frequency of a split across chains
        for split in set(d1).union(d2):
            f1, f2 = d1[split] / n1, d2[split] / n2
            if f1 >= min_freq or f2 >= min_freq:
                diffs.append(abs(f1 - f2))
                weights.append((f1 + f2) / 2)
        try:
            maxdiffs.append(max(diffs))
        except ValueError:
            # fails if diffs is empty
            if diffs:
                raise
            assert len(d1) and len(d2)
            assert sum(d1.values()) and sum(d2.values()), "sum=%s for %d splits (1); sum=%s for %d splits (2)" % (sum(d1.values()), len(d1), sum(d2.values()), len(d2))
            logger.warning('No frequency found above %g (retry without cutoff)', min_freq)
            for split in set(d1).union(d2):
                f1, f2 = d1[split] / n1, d2[split] / n2
                diffs.append(abs(f1 - f2))
                weights.append((f1 + f2) / 2)
            maxdiffs.append(max(diffs))

        # phylobayes bpcomp mean is weighted by the average frequency of the split across chains
        # FIXME: can't reproduce the same meandiff using PF01020 chains
        meandiffs.append(sum(d*w for d,w in zip(diffs, weights))/sum(weights))

    return maxdiffs, meandiffs


def load_multi_clusterpool(treefiles, burnin=0.05, poolclass=ClusterPool):
    ntrees, burnins = multi_treelist_burnin(treefiles, burnin)
    logger.info('burnins = %s; treefiles = %s', burnins, ', '.join(treefiles))

    all_trees = chain(*(islice(iter_multinewick(treefile), b, None)
                        for treefile,b in zip(treefiles, burnins)))
    clusterpool = poolclass(all_trees)
    return clusterpool


def MREfromtreelist(treefiles, burnin=0.05, min_freq=0.5, aggfunc=mean, #makeset=BitSet, keywrap=sortuple,
                    support_as_percent=False):
    """Majority Rule Consensus tree"""
    support_factor = 100 if support_as_percent else 1
    clusterpool = load_multi_clusterpool(treefiles, burnin)
    distrib, _, distrib_dist = clusterpool.count_subsplits(unrooted=False)
    #clusterpool.unrooted_complement_dists()
    n, ids = clusterpool.n, clusterpool.ids
    labels = {k: name for name, k in ids.items()}
    rootclust = BitSet(clusterpool.rootclust)

    cutoff = n * max(0.5, min_freq)  # For the first pass, we cannot have a frequency less than 0.5

    # Tree building
    nodes = {}  # bipartition: TreeNode object

    # Sort splits, based on the side of the split that does not contain leaf 0:
    # If we sort this side by increasing size, this should ensure that the next
    # cluster that contains a subcluster is the immediate parent node in the tree.
    # At the end, don't forget to add the smallest cluster containing leaf 0
    nesting_ordered_splits = sorted([split for split, count in distrib.items() if count > cutoff],
                                     key=lambda split: len(max(split, key=tuple)))
    basal = []  # clusters without non-trivial parent
    logger.debug('Tree of %d elements, %d retained splits', len(rootclust), len(nesting_ordered_splits))
    for i, split in enumerate(nesting_ordered_splits):
        count = distrib.pop(split)
        split = tuple(sorted(split, key=tuple))
        parent = None  # Safeguard to make sure the previous iteration parent is not reused.
        try:
            node = nodes[split]
            logger.debug('#%d split %r, existing node with %d children.', i, split[1], len(node.children))
        except KeyError:
            node = nodes[split] = TreeNode(dist=aggfunc(distrib_dist.pop(split[1])),
                                           support=count/n * support_factor)
            node.add_features(idx=i)
            logger.debug('#%d split %r, new node.', i, split[1])
        for j, larger in enumerate(nesting_ordered_splits[i+1:], start=i+1):
            s_larger = tuple(sorted(larger, key=tuple))
            if split[1] <= s_larger[1]:
                try:
                    parent = nodes[s_larger]
                except KeyError:
                    parent = nodes[s_larger] = TreeNode(dist=aggfunc(distrib_dist.pop(s_larger[1])),
                                                      support=distrib[larger]/n * support_factor)
                    parent.add_features(idx=j)
                parent.add_child(node)

                break
        else:
            s_larger = None
            j = None
            # Its parent is a trivial split/rootclust
            basal.append(node)
            #logger.debug('  basal clust #%d %r of size %d' % (i, split[1], len(split[1])))

        # Now, build leaves for all the elements that are not in any child clade
        child_clusts = []
        for child in node.children:
            try:
                child_clusts.append(max(nesting_ordered_splits[child.idx], key=tuple))
            except AttributeError as err:
                logger.error('%s: split #%d, child number %d %r leaf=%s', err.args, i, len(child_clusts), child.name, child.is_leaf())
                raise

        logger.debug('  parent: %r  child clusts: [%s]', None if j is None else s_larger[1], ', '.join(map(repr, child_clusts)))
        unclustered = split[1].difference(*child_clusts)
        for elem in unclustered:
            child = node.add_child(name=labels[elem],
                           dist=aggfunc(distrib_dist.pop(BitSet((elem,)))))
            child.add_feature('elem', elem)
        if len(unclustered):
            logger.debug('  Branching unclustered leaves: %s', tuple(unclustered))

    # The basal nodes can be joined with the other side of the last split.
    lastsplit = tuple(sorted(nesting_ordered_splits[-1], key=tuple))
    assert basal[-1] == nodes[lastsplit]

    if len(basal) == 1:
        parent = basal[-1]
        last_elements = lastsplit[0]
        # We *know* that lastclust has more than one element, because trivial splits are not stored.
        lastnode = parent.add_child(dist=parent.dist, support=parent.support)
    else:
        # The most basal node represents a trivial cluster: rootclust.difference((0,))
        parent = TreeNode(dist=aggfunc(distrib_dist[BitSet((0,))]),
                          support=support_factor)
        baseclusts = []
        for base in basal:
            parent.add_child(base)
            baseclusts.append(max(nesting_ordered_splits[base.idx], key=tuple))

        lastnode = parent
        last_elements = rootclust.difference(*baseclusts)
    logger.debug('%d basal nodes. Last unclustered: %r', len(basal), last_elements)

    # Following the unrooted convention, we make the root node polytomic
    if len(parent.children) <= 2:
        logger.error('Root node is not a polytomy, this is unexpected.')

    parent.dist = 0
    #parent.support = 1
    for elem in last_elements:
        child = lastnode.add_child(name=labels[elem],
                           dist=aggfunc(distrib_dist.pop(BitSet((elem,)))))
        child.add_feature('elem', elem)

    if len(parent) != len(rootclust):
        forgotten = rootclust.difference(BitSet(leaf.elem for leaf in parent.iter_leaves()))
        raise RuntimeError('%d tree leaves (root:%s) != %d elements: forgot %r' % (
                           len(parent), parent.is_root(), len(rootclust), forgotten))
    return parent


def resolver_compatibility(clust, alt_clusts, rootclust=None):
    """Test if alternative clusters can coexist with clust.
    if rootclust is given, compatibility is assumed in an unrooted way, so
    a cluster is also valid if its complement is compatible with clust.
    """
    compat = ''
    for prev in alt_clusts:
        if prev < clust or prev > clust:
            compat += '1'
        elif prev & clust:
            if rootclust is not None:
                # the complement of prev must be contained or containing as well
                compl = rootclust.difference(prev)
                compat += '1' if compl < clust else '0'
            else:
                compat += '0'
        else:
            compat += '1'
    return compat


def polysplits(targetnewick, distrib, nodeid, ids, n, key='name', split_dist=None, distrib_sub=None,
               support_as_percent=False):
    """Rank single splits resolving the given polytomy 'nodeid' by their support,
    and yield the corresponding trees.

    If distrib_sub (Conditional Clade Probabilities) are given,
    rank resolutions by CCP instead of bipartition frequency"""
    if split_dist is None:
        split_dist = {}

    if support_as_percent:
        support_factor = 100
        support_format = '{:.2f}'.format
    else:
        support_factor = 1
        support_format = '{:.4f}'.format

    targettree = Tree(targetnewick, format=1)  # if labels are support, they are stored as "name"
    original_nnodes = sum(1 for _ in targettree.traverse())

    if nodeid == 'ROOT':
        node = targettree
    else:
        node, = targettree.search_nodes(**{key: nodeid})

    targetclust = BitSet((ids[leafname] for leafname in node.iter_leaf_names()))

    subtargets = []
    original_subdists = {}
    original_children = node.get_children()  # preserve the ordering
    if len(original_children) <= 2:
        logger.error('Node is already resolved. Quit.')
        return
    logger.info('Target polytomy size: %d', len(original_children))
    for child in node.children:
        subtargets.append(BitSet((ids[leafname] for leafname in child.iter_leaf_names())))
        original_subdists[child] = child.dist

    targetclusterpool = ClusterPool([targetnewick], ids=ids, allow_polytomies=True)
    targetsplits, targetsubsplits, _ = targetclusterpool.count_subsplits(unrooted=False)

    if distrib_sub is not None:
        # Use the conditional clades.
        # Drawback: each resolution is necessarily dichotomic
        # (otherwise I'll have to include subsplits from strictly smaller clades)
        if node is targettree:
            logger.warning('Root subsplit resolving is not different from split resolving.')
        amalgam = Amalgam(distrib, distrib_sub, ids, n)
        sorted_subsplits = sorted(amalgam.subsplits[targetclust], key=amalgam.CCP.__getitem__)
        # Why sorting with distrib_sub.__getitem__ does not reproduce CCP sorting ???
        logger.info('%d subsplits to test', len(sorted_subsplits))
        inserted = []  # pairs of inserted nodes
        while sorted_subsplits:
            subsplit = sorted_subsplits.pop()
            sub0, sub1 = subsplit
            grouped = [{}, {}]
            for child, sub in zip(original_children, subtargets):
                if sub <= sub0:
                    grouped[0][child] = sub
                elif sub <= sub1:
                    grouped[1][child] = sub
                else:
                    # this is not a compatible partition. Discard.
                    break
            else:
                # All target subclusters are compatible. Insert resolution.
                if node is targettree and any(len(group)==1 for group in grouped):
                    # Given that this is a root split, this one already exists.
                    continue
                for previous in inserted:
                    previous.delete(prevent_nondicotomic=False)
                inserted = []
                for newclust, group in zip((sub0, sub1), grouped):
                    if len(group) == 1:
                        child, sub = group.popitem()
                        node.add_child(child.detach()) #FIXME: dist should not have to be updated,
                                                       # because unchanged anyway.
                    else:
                        inserted.append(node.add_child(dist=split_dist.get(newclust,1),
                                        support=amalgam.freq[newclust] * support_factor,
                                        name=support_format(amalgam.freq[newclust]*support_factor)))
                        inserted[-1].add_feature(key, 'new%d' % len(inserted))
                        # Note: for later:
                        # this provides a way to improve branch lengths under polytomies:
                        # by adding the average length of inserted edges (weighted by probability)
                        for child, sub in group.items():
                            inserted[-1].add_child(child.detach())  # dist unchanged.
                nnodes = sum(1 for _ in targettree.traverse())
                assert nnodes > original_nnodes
                yield amalgam.CCP[subsplit], inserted, targettree

    else:
        # We are not interested in these splits, because already included.
        for split in targetsplits:
            del distrib[split]

        splitcounts = sorted(distrib.items(), key=lambda item: item[1])  # most frequent at the end (use pop)

        inserted = None

        n_inserted = 0

        while splitcounts:
            split, count = splitcounts.pop()
            assert split not in targetsplits  # DEBUG
            if node is targettree:
                clust = min(split, key=len)
                # Not necessary, but its prettier to group the smallest set under the root.
            else:
                for clust in split:
                    if clust < targetclust:
                        break
                else:
                    continue  # Neither side is contained: incompatible
            assert clust != targetclust      # DEBUG
            assert clust < targetclust
            nested = {}
            for child, sub in zip(original_children, subtargets):
                if sub < clust:
                    nested[child] = sub
                elif sub & clust:
                    # This split is incompatible. Skip.
                    nested.clear()
                    break
            if nested:
                n_inserted += 1
                # only valid if its boundaries match existing child (not splitting any child)
                if BitSet.union(*nested.values()) != clust:
                    raise RuntimeError('Incompatible split #%d support=%.4f: %r', n_inserted, count/n, clust)
                if len(nested) == 1:
                    logger.error('Single child resolution (number %d): wrong', n_inserted)
                elif node is targettree and len(nested) == len(original_children)-1:
                    logger.error('all-but-one child root resolution #%d support=%.4f: wrong %d nested VS %d children',
                                   n_inserted, count/n, len(nested), len(node.children))
                # Delete the previously inserted resolver
                try:
                    if inserted.is_root():
                        logger.error('Inserted node #%d-1 is root! cannot delete.', n_inserted)
                    inserted.delete(prevent_nondicotomic=False)
                except AttributeError:
                    # this is the first time we insert a node, previous is None.
                    pass
                # Insert the resolver node
                inserted = node.add_child(dist=split_dist.get(clust, 1),
                                          support=count/n*support_factor,
                                          name=support_format(count/n*support_factor))
                inserted.add_feature(key, 'new')
                for child in original_children:
                    if child not in nested:
                        # Restore the original ordering and branch length
                        node.add_child(child.detach(), dist=original_subdists[child])
                    else:
                        inserted.add_child(child.detach(), dist=original_subdists[child])
                nnodes = sum(1 for _ in targettree.traverse())
                assert nnodes == original_nnodes + 1  # DEBUG

                #TODO: check compatibility with previous resolver splits?
                #compat = resolver_compatibility(clust, previously_inserted_clusts,
                #                      (targetclust if node is targettree else None))
                #previous_inserted_clusts.append(clust)

                yield count/n, inserted, targettree  # this is NOT a copy.
        logger.info('Created %d alternative resolutions', n_inserted)

        #
        # Note: multiple resolving splits might co-occur
        # Then, there are nested in each other (choose one level)


def conflicts(split1, split2):
    """Whether split1 and 2 are incompatible (cannot exist in the same tree)"""
    x1, y1 = split1
    x2, y2 = split2
    return x1 & x2 and x1 & y2 and y1 & x2 and y1 & y2


def find_tree_splits(tree, clusterpool):
    ids = clusterpool.ids
    leaves = set(ids[name] for name in tree.iter_leaf_names())
    for node in tree.iter_descendants():
        if node.is_leaf():
            continue
        ingroup = [ids[name] for name in node.iter_leaf_names()]
        outgroup = leaves.difference(ingroup)
        nodesplit = clusterpool.setgroup((ingroup, outgroup))
        try:
            refcount = clusterpool.distrib[nodesplit]
        except KeyError:
            raise ValueError('target tree split is not contained in the sample')
        yield node, nodesplit, refcount


def map_split_supports(tree, clusterpool, attr='support', percent=False):
    factor = 100 if percent else 1
    for node, nodesplit, refcount in find_tree_splits(tree, clusterpool):
        node.add_feature(attr, refcount/clusterpool.n * factor)


def internode_certainty(tree, clusterpool, attr='IC', percent=False, ICA=False, save_ref_support=None):
    if save_ref_support is True: save_ref_support = 'support'
    sorted_splits = sorted(clusterpool.distrib.items(), key=lambda item: item[1], reverse=True)
    factor = 100 if percent else 1
    TC = 0  # Tree certainty (total)
    for node, nodesplit, refcount in find_tree_splits(tree, clusterpool):
        #conflicting = []
        IC = 1
        for split, count in sorted_splits:
            if conflicts(split, nodesplit):
                #conflicting.append(split)
                if refcount > 0:
                    relfreq = refcount/(refcount+count)
                    IC += relfreq * log2(relfreq) + (1-relfreq) * log2(1-relfreq)
                else:
                    logger.warning('Conflicting split with freq=0 (node=%s %s)', node.name, getattr(node, 'nodeid', None))
                break
        TC += IC * factor
        node.add_feature(attr, IC * factor)
        if save_ref_support:
            node.add_feature(save_ref_support, refcount/clusterpool.n * factor)

    if len(tree.children) == 2:
        # Count the root split only once
        TC -= getattr(tree.children[0], attr, factor)
    return TC


def main():
    logging.basicConfig()
    logger.setLevel(logging.INFO)
    parser = ap.ArgumentParser(description=__doc__)
    parser.add_argument('treelists', nargs='+')
    parser.add_argument('-b', '--burnin', type=int, default=0, help='[%(default)s]')
    parser.add_argument('-n', '--nsamples', type=int, help='[%(default)s]')
    parser.add_argument('-i', '--get-seq-label', help='comma separated list of integer ids to label, then quit.')
    parser.add_argument('-e', '--evaluate', metavar='NEWICKFILE', help='Score the given tree and exit.')
    args = parser.parse_args()

    _, burnins = multi_treelist_burnin(args.treelists, args.burnin)
    logger.info('burnins = %s; treefiles = %s', burnins, ', '.join(args.treelists))

    treelist = []
    for burnin, treefile in zip(burnins, args.treelists):
        treelist.extend(list(iter_multinewick(treefile))[burnin:])

    nsamples = 1 if args.get_seq_label else args.nsamples

    amalgam = build_amalgam(treelist, nsamples=nsamples)
    try:
        amalgam.verify_freqs()
    except ValueError as err:
        print('ValueError:', err, file=stderr)
        itertrees = iter_multinewick(args.treefile)
        for i in range(args.burnin):
            next(itertrees)
        print('Some input trees:', file=stderr)
        for n, newick in enumerate(itertrees, start=1):
            if n > args.nsamples or n > 30:
                break
            print(rewrite_indexed_newick(newick, amalgam.ids), file=stderr)
        #amalgam.write()
    if args.get_seq_label:
        seq_ids = [int(k) for k in args.get_seq_label.split(',')]
        for i in seq_ids:
            print('%d\t%s' % (i, amalgam.labels[i]))
        return

    if not args.evaluate:
        #for logfreq, toptree in top_logfreq_trees(amalgam, n=50):
        #    print('%g' % logfreq, toptree.write(format=2), sep='\t')
        print(max_logfreq_tree(amalgam).write(format=2))  # all branches + leaf names + internal supports
    else:
        for i, newick in enumerate(iter_multinewick(args.evaluate)):
            clusterpool = ClusterPool([newick], ids=amalgam.ids, allow_polytomies=True)
            if len(clusterpool.rootclust) < len(amalgam.full):
                # This tree does not have all leaves
                missing_leaves = amalgam.full.difference(clusterpool.rootclust)
                logger.warning('Missing %d leaves in evaluated tree #%d: match the amalgam.', len(missing_leaves), i)
                raise NotImplementedError
                #amalgam = Amalgam()  # If each input tree has a different set of leaves, I will have to recompute a new amalgam for each one.
            #count_bi, count_tri, _ = clusterpool.count_subsplits(trivial_splits=False, unrooted=True)
            print('Tree #%d with %d leaves and %d subsplits' % (i, len(clusterpool.rootclust), len(clusterpool.distrib_sub)))
            lCCPscore = 0
            lBPscore = 0
            n_unfound_sub = 0  # number subsplits not found: approximated frequency
            for subsplit in clusterpool.distrib_sub:
                try:
                    lCCPscore += log(amalgam.CCP[subsplit])
                except KeyError:
                    if len(subsplit)==3:
                        # root split of an unrooted tree
                        assert set().union(*subsplit) == clusterpool.rootclust
                        cl1, cl2, cl3 = subsplit
                        lCCPscore += log(amalgam.CCP[clusterpool.setgroup((cl1, cl2))])
                        clusterpool.first_rootsplit = clusterpool.setgroup((cl1.union(cl2), cl3))
                    else:
                        # The sample does not contain this tripartition. Its freq might be computed from the bipartition freqs?
                        cl1, cl2 = subsplit
                        joined = cl1.union(cl2)
                        outgroup = amalgam.makeset(clusterpool.rootclust).difference(joined)
                        assert clusterpool.setgroup((cl1, outgroup)) not in amalgam.CCP, "invalid amalgam (not unrooted)"
                        assert clusterpool.setgroup((cl2, outgroup)) not in amalgam.CCP, "invalid amalgam (not unrooted)"
                        # Approximate
                        split_n = amalgam.bi[clusterpool.setgroup((joined, outgroup))]
                        if split_n == 0:
                            split_n = 2
                        # The expected CCP of subsplit is less than 1/split_n.
                        lCCPscore += log(1./split_n)  # Not sure how bad is my approx.
                        # This could be tuned based on the known downsampling frequency.
                        # For example, downsamping 1 every 2 of 2n trees: a bad luck case
                        # could eliminate a subsplit with original count n.
                        n_unfound_sub += 1

            if len(clusterpool.first_rootsplit) == 2:
                lCCPscore += log(amalgam.freq[list(clusterpool.first_rootsplit)[0]])

            n_unfound = 0
            for split in clusterpool.distrib:
                cl1, cl2 = split
                assert cl1.union(cl2) == amalgam.makeset(clusterpool.rootclust)
                try:
                    lBPscore += log(amalgam.freq[cl1])
                except KeyError:
                    if len(cl1) in (1, amalgam.n - 1):
                        continue  # log(1) = 0
                    lBPscore += log(1./amalgam.n)
                    n_unfound += 1

            if n_unfound_sub:
                logger.warning('%d subsplits not found (approximated).', n_unfound_sub)
            if n_unfound:
                logger.warning('%d splits not found (approximated).', n_unfound)
            print('log(CCPscore) =', lCCPscore, 'log(BPscore) =', lBPscore)


def main_bpcomp():
    doc="""Measure the bipartition frequency differences between MCMC chains,
    as in Phylobayes' bpcomp.
    """
    logging.basicConfig()
    logger.setLevel(logging.INFO)
    parser = ap.ArgumentParser(description=doc)
    parser.add_argument('treefiles', nargs='+')
    parser.add_argument('-b', '--burnin', type=float, default=0, help='[%(default)s]')
    parser.add_argument('-c', '--cutoff', type=float, default=0.5, help='[%(default)s]')
    parser.add_argument('-i', '--ignore-bad', action='store_true', help='Do not fail on badly formatted newick lines')
    args = parser.parse_args()

    maxdiffs, meandiffs = bpcomp(args.treefiles, args.burnin, args.cutoff, args.ignore_bad)

    print('Max diff: ', *('%.5f' % x for x in maxdiffs), sep='\t')
    print('Mean diff:', *('%.5f' % x for x in meandiffs), sep='\t')


def main_MREtree():
    logging.basicConfig()
    logger.setLevel(logging.INFO)
    parser = ap.ArgumentParser(description="Majority Rule Consensus tree")
    parser.add_argument('treefiles', nargs='+')
    parser.add_argument('-b', '--burnin', type=float, default=0, help='[%(default)s]')
    parser.add_argument('-c', '--cutoff', type=float, default=0.5, help='[%(default)s]')
    args = parser.parse_args()

    tree = MREfromtreelist(args.treefiles, args.burnin, args.cutoff)

    print(tree.write(format=0))


def main_polysplits():
    sys.setrecursionlimit(5000)  # Allow more recursive __next__ calls in newick.IterNewickClustersPartial. FIXME.
    logging.basicConfig()
    logger.setLevel(logging.INFO)
    parser = ap.ArgumentParser(description=("Given an input polytomic node, "
                                    "list the different resolutions by order "
                                    "of frequency."))
    parser.add_argument('targettree', help='The tree to resolve')
    parser.add_argument('treefiles', nargs='+', help='Sample of trees (e.g. MCMC)')
    parser.add_argument('-n', '--node', required=True, help='Node to resolve (required). "ROOT" selects the root.')
    parser.add_argument('-k', '--key', default='name', help='Node attribute used as query identifier')
    parser.add_argument('-b', '--burnin', type=float, default=0, help='[%(default)s]')
    parser.add_argument('-c', '--conditional', action="store_true",
                        help='Rank resolutions by CCP instead of bipartition frequency')
    parser.add_argument('-p', '--support-as-percent', action="store_true")
    args = parser.parse_args()
    conditional = args.conditional

    clusterpool = load_multi_clusterpool(args.treefiles, args.burnin)
    # Delay full reading after the target tree was loaded, in case some leaves should be ignored.
    ids = clusterpool.ids

    for targetnewick in iter_multinewick(args.targettree):
        break  # Read a single tree.

    # Remove the missing leaves from the MCMC clusterpool
    targetclusterpool = ClusterPool([targetnewick], ids=ids, allow_polytomies=True)
    if targetclusterpool.rootclust < clusterpool.rootclust:
        missing_leaves = BitSet(clusterpool.rootclust.difference(targetclusterpool.rootclust))
        logger.info('Match the reference sample by dropping %d unused leaves: %r', len(missing_leaves), missing_leaves)
        #distrib, distrib_sub, distrib_dist = clusterpool.drop_leaves(BitSet(missing_leaves))
    else:
        missing_leaves = set()
    distrib, distrib_sub, distrib_dist = clusterpool.count_subsplits(unrooted=False, trivial_splits=False, drop_ids=missing_leaves)
    logger.info('Max cluster size: %d; rootclust size %d', max(map(len, distrib)), len(clusterpool.rootclust))
    n = clusterpool.n

    # Verification (especially if removing some leaves):
    check_expected_split_counts(n, clusterpool.rootclust, distrib, distrib_sub)

    for label, k in list(ids.items()):
        if k in missing_leaves:
            del ids[label]
    logger.info('Sample of %d trees, %d elements', n, len(ids))
    logger.info('Reference splits: %d (non-trivial)', len(distrib))

    clusterpool.unrooted_complement_dists()

    #if conditional:
    clusterpool.add_trivial_splits()  # Required for Amalgam
    clusterpool.unrooted_complement_tripartitions()
    # verification 2
    testamalgam = Amalgam(distrib, distrib_sub, clusterpool.ids, n)
    testamalgam.verify_freqs()

    split_dist = {clust: median(dists) for clust,dists in distrib_dist.items()}
    del distrib_dist

    out_features = None if args.key=='name' else [args.key]
    for score, inserted, resolved in polysplits(targetnewick, distrib, args.node, ids, n, args.key,
                                      split_dist,
                                      (distrib_sub if conditional else None),
                                      args.support_as_percent):
        print('%g\t%s' % (score, resolved.write(out_features, format=1)))
        if score < 0.02:
            break


def main_TC():
    """compute Tree Certainty and Internode Certainty values,
    and/or classical split supports"""
    sys.setrecursionlimit(5000)  # Allow more recursive __next__ calls in newick.IterNewickClustersPartial. FIXME.
    logging.basicConfig()
    logger.setLevel(logging.INFO)
    parser = ap.ArgumentParser(description="""Compute Tree Certainty and Internode Certainty values,
    and/or classical split supports""")
    parser.add_argument('targettree', help='The tree to score')
    parser.add_argument('treefiles', nargs='+', help='Sample of trees (e.g. MCMC)')
    parser.add_argument('-m', '--metrics', default='IC,support',
        help=("comma-separated list of metrics to add. Implemented: IC,support. "
              "In the form 'attr=metric', attr is used as the key in the NHX comment."
              " [%(default)s]"))
    parser.add_argument('-b', '--burnin', type=float, default=0, help='[%(default)s]')
    parser.add_argument('-p', '--score-as-percent', action="store_true")
    parser.add_argument('-t', '--tree-format', default=0, type=int, help="ete3 newick format")
    args = parser.parse_args()

    metrics = {}  # metric -> target attribute
    for metric in args.metrics.split(','):
        try:
            key, val = metric.split('=', maxsplit=1)
        except ValueError:
            key = val = metric
        if val not in ('IC', 'support'):
            raise ValueError('Unsupported support metric %r' % val)
        metrics[val] = key

    clusterpool = load_multi_clusterpool(args.treefiles, args.burnin)
    # Delay full reading after the target tree was loaded, in case some leaves should be ignored.
    ids = clusterpool.ids

    itertargetnewicks = iter_multinewick(args.targettree)
    targetnewick = next(itertargetnewicks)  # Read a single tree for now
    targettree = Tree(targetnewick, format=args.tree_format)

    # Remove the missing leaves from the MCMC clusterpool
    targetclusterpool = ClusterPool([targetnewick], ids=ids, allow_polytomies=True)
    if targetclusterpool.rootclust < clusterpool.rootclust:
        missing_leaves = BitSet(clusterpool.rootclust.difference(targetclusterpool.rootclust))
        logger.info('Match the reference sample by dropping %d unused leaves: %r', len(missing_leaves), missing_leaves)
        #distrib, distrib_sub, distrib_dist = clusterpool.drop_leaves(BitSet(missing_leaves))
    else:
        missing_leaves = set()
    distrib, distrib_sub, distrib_dist = clusterpool.count_subsplits(unrooted=False, trivial_splits=False, drop_ids=missing_leaves)
    logger.info('Max cluster size: %d; rootclust size %d', max(map(len, distrib)), len(clusterpool.rootclust))
    n = clusterpool.n

    # Verification (especially if removing some leaves):
    check_expected_split_counts(n, clusterpool.rootclust, distrib, distrib_sub)

    for label, k in list(ids.items()):
        if k in missing_leaves:
            del ids[label]
    logger.info('Sample of %d trees, %d elements', n, len(ids))
    logger.info('Reference splits: %d (non-trivial)', len(distrib))

    clusterpool.unrooted_complement_dists()

    clusterpool.add_trivial_splits()  # Required for Amalgam
    clusterpool.unrooted_complement_tripartitions()
    testamalgam = Amalgam(distrib, distrib_sub, clusterpool.ids, n)
    testamalgam.verify_freqs()

    del distrib_dist

    if 'IC' in metrics:
        TC = internode_certainty(targettree, clusterpool, metrics['IC'], args.score_as_percent,
                                 save_ref_support=metrics.get('support'))
        targettree.add_feature('TC', TC)
        allfeatures = set().union(*(n.features for n in targettree.traverse())).difference(('name', 'dist', 'support'))
        print('#TC\t%g' % TC)
    elif 'support' in metrics:
        map_split_supports(targettree, clusterpool, metrics['support'], args.score_as_percent)

    # Too many decimals are annoying
    scorefmt = '%.2f' if args.score_as_percent else '%.5g'
    for node in targettree.iter_descendants():
        if not node.is_leaf():
            for attr in set(metrics.values()):
                if attr != 'support':
                    node.add_feature(attr, scorefmt % getattr(node, attr))

    print(targettree.write(allfeatures, format=args.tree_format, format_root_node=True,
                           support_formatter=(scorefmt if metrics.get('support')=='support' else None)))


if __name__ == '__main__':
    if argv[1] in ('bp', 'bpcomp'):
        argv.pop(1)
        main_bpcomp()
    elif argv[1] == 'MR':
        argv.pop(1)
        main_MREtree()
    elif argv[1] in ('res', 'resolve'):
        argv.pop(1)
        main_polysplits()
    #TODO: rebranch: move a clade to alternative supported locations.
    elif argv[1] == 'TC':
        argv.pop(1)
        main_TC()
    else:
        main()
