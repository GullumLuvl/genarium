#!/usr/bin/env python3

# (C) Copyright Guillaume Louvel 2023
#
# This file is licensed under the GNU General Public License GPL version 3


"""
Adjust burnins and lengths of multiple MCMC chains.
"""


from sys import stderr
import argparse as ap
from itertools import islice
from .newick import iter_multinewick


def multi_chain_burnin(samplesizes, burnin=0.05):
    """Compute the burnin for each chain, so as to have all chain lengths equal
    to the shortest trimmed chain. This selects tails of equal length."""
    if 0 < burnin < 1:
        # Interpret burnin as a percentage
        max_n = max(samplesizes)
        burnin = int(round(burnin * max_n))

    # Ensure that we keep the same number of trees in all chains.
    # (we keep the last ones, so increase burnin when needed)
    min_n = min(samplesizes)
    return [int(burnin + n - min_n) for n in samplesizes]


def multi_treelist_burnin(treelists, burnin=0.05):
    ntrees = []
    for treefile in treelists:
        ntrees.append(sum(1 for _ in iter_multinewick(treefile)))
    return ntrees, multi_chain_burnin(ntrees, burnin)


def main_equalized_chains():
    desc = "Produce trimmed chains of equal lengths"
    parser = ap.ArgumentParser(description=desc)
    parser.add_argument('samplefiles', nargs='+')
    parser.add_argument('-b', '--burnin', metavar='X', default=0.05, type=float,
            help=('integer >= 1: absolute number of samples; '
                  '0 < float < 1: fraction of samples. [%(default)s]'))
    parser.add_argument('-t', '--thin', metavar='K', default=1, type=int,
            help='thinning: keep one sample every K')
    parser.add_argument('-o', '--outfile', help="Filename for the trimmed chain. Use '-' for stdout. [default: no output chain]")
    #TODO: if outchain contains string {#}, one file is created for each input, with an integer id.
    args = parser.parse_args()

    #TODO: input checking: distinguish trees from arrays of numbers. Check for header line.
    nsamples, burnins = multi_treelist_burnin(args.samplefiles, args.burnin)

    print('burnins\t%s\nsizes\t%s' % (' '.join(map(str, burnins)), ' '.join(map(str, nsamples))), file=stderr)
    if args.outfile:
        out = stdout if args.outfile == '-' else open(args.outfile, 'w')
        try:
            for samplefile, b in zip(args.samplefiles, burnins):
                for line in islice(iter_multinewick(samplefile), b, None, args.thin):
                    out.write(line.rstrip() + '\n')
        finally:
            if args.outfile != '-':
                out.close()

