
# (C) Copyright Guillaume Louvel 2023
#
# This file is licensed under the GNU General Public License GPL version 3


from sys import getsizeof
from functools import reduce


class BitSet(int):
    def __new__(cls, intlist=()):
        #NOTE: using reduce is slower.
        val = 0
        for k in intlist:
            val |= (1<<k)
        return super().__new__(cls, val)
    #TODO: read from a list of single characters in arbitrary base

    @classmethod
    def fromint(cls, k):
        if k < 0:
            raise ValueError('Negative integer not allowed')
        return super().__new__(cls, k)

    @property
    def highest(self):
        """Highest bit set to 1"""
        return len(bin(self)) - 3

    def __len__(self):
        """Number of bits set to 1"""
        return bin(self).count('1')  # in Python 3.10, there is int.bit_count()

    def __bool__(self):
        """True when the set is not empty"""
        return self != 0

    def __iter__(self):
        yield from (k for k,bit in enumerate(bin(self)[:1:-1]) if bit=='1')

    def __repr__(self):
        strlist = list(map(str, self))
        if len(strlist) > 25:
            strlist = strlist[:25] + ['..', strlist[-1]]
        return "B(" + ','.join(strlist) + ")"

    def __str__(self):
        return "Bx" + bin(self)[:1:-1]

    def complement(self, N):
        """Complement the set composition. N should be greater or equal to self.highest"""
        return BitSet.fromint((2**N - 1) ^ self)

    def union(self, *others):
        joined = self
        for other in others:
            joined |= other
        return BitSet.fromint(joined)

    def difference(self, *others):
        diff = self
        for other in others:
            diff &= ~other
        return BitSet.fromint(diff)

    def asbytes(self):
        # A compact ascii representation could make use of base64 or base85
        # on top of that
        return self.to_bytes((self.bit_length()+7) // 8, byteorder='big')

    def __contains__(self, element):
        return bool(1<<element & self)

    def __sub__(self, other):
        return BitSet.fromint(self & ~other)

    def __gt__(self, other):
        """self contains all elements of other"""
        return (self | other) == self and (self & other) != self

    def __ge__(self, other):
        return (self | other) == self

    def __lt__(self, other):
        return (self | other) == other and (self & other) != other

    def __le__(self, other):
        return (self | other == other)

    # For safety, the following operations should not be used, because bitset is conceptually a set:
    def __float__(self):
        raise TypeError("cannot convert 'BitSet' to float")
    def __neg__(self):
        raise TypeError("bad operand type for unary -: 'BitSet'")
    def __pow__(self, value, mod):
        raise TypeError("unsupported operand type for ** or pow(): 'BitSet'")
    def __add__(self, other):
        raise TypeError("unsupported operand type for +: 'BitSet'")
    def __mul__(self, other):
        raise TypeError("unsupported operand type for *: 'BitSet'")
    def __truediv__(self, other):
        raise TypeError("unsupported operand type for /: 'BitSet'")
    def __floordiv__(self, other):
        raise TypeError("unsupported operand type for //: 'BitSet'")
    def __mod__(self, other):
        raise TypeError("unsupported operand type for %: 'BitSet'")
    def __divmod__(self, other):
        raise TypeError("unsupported operand type for divmod(): 'BitSet'")
    def __rpow__(self, value, mod):
        raise TypeError("unsupported operand type for ** or pow(): 'BitSet'")
    def __radd__(self, other):
        raise TypeError("unsupported operand type for +: 'BitSet'")
    def __rmul__(self, other):
        raise TypeError("unsupported operand type for *: 'BitSet'")
    def __rtruediv__(self, other):
        raise TypeError("unsupported operand type for /: 'BitSet'")
    def __rfloordiv__(self, other):
        raise TypeError("unsupported operand type for //: 'BitSet'")
    def __rmod__(self, other):
        raise TypeError("unsupported operand type for %: 'BitSet'")
    def __rdivmod__(self, other):
        raise TypeError("unsupported operand type for divmod(): 'BitSet'")


def compact_setgroup(setgroup):
    """Encode a group of set using a very compact notation as a tuple of integers

    (P, *subs)
    
    P represents the union of all sets in the group as a BitSet integer
    subs represents the subdivisions of P (BitSet), reindexed

    By symmetry, (P, q) is equivalent to (P, q.complement(len(P)))

    NOTE: to compute the complement of P, the total number of elements is needed.
    """

    subsets = list(setgroup)
    parent = frozenset(subsets[0]).union(*subsets[1:])
    parent_idx = dict((k,i) for i,k in enumerate(sorted(parent)))
    p = int(BitSet(parent))
    # reindex by reference to the parent elements
    qs = [int(BitSet([parent_idx[k] for k in sub])) for sub in subsets]
    qs.sort()
    return (p, *qs[:-1])


def decompact_setgroup(pq):
    p, *qs = pq
    if p < reduce(int.__or__, qs):
        raise ValueError('Expect the parent division to contain all subsets')
    parent = tuple(BitSet.fromint(p))
    subsets = []
    for q in qs:
        subsets.append(frozenset(parent[i] for i in tuple(BitSet.fromint(q))))
    # Last set contains all the non selected elements from P
    subsets.append(frozenset(parent).difference(*subsets))

    return tuple(subsets)


def bitset_pack(setgroup):
    return frozenset(map(BitSet, setgroup))

def bitset_unpack(bitsetgroup):
    return tuple(map(tuple, bitsetgroup))

def frozen_freeze(setgroup):
    return frozenset(map(frozenset, setgroup))

def sizeof_2D_container(container):
    s = getsizeof(container)
    for sub in container:
        s += getsizeof(sub) + sum(map(getsizeof, sub))
    return s
