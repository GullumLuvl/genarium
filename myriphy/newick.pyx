# cython: language_level=3

# (C) Copyright Guillaume Louvel 2023
#
# This file is licensed under the GNU General Public License GPL version 3


import re
from sys import stdin
import bz2
from itertools import chain
from queue import deque


_MAX_DEPTH = 10000
NUM_PATTERN = r'[0-9]*(?:\.[0-9]+)?(?:[eE]-?[0-9]+)?'
#DELIM_REGEX = re.compile(r'[(,;]|\)(:' + NUM_PATTERN + ')?')

# This works but is convoluted:
#DELIM_REGEX = re.compile(r'(\(|,|;|\)[^(),;\[\]:]*)(\[.*\])?:?(' + NUM_PATTERN + ')?')
# This splits the element into 3 groups:
# 1. one of : ','  or  '('  or  ';'  or  ')nodename'
# 2. and 3. are empty if the first character is not a closing parenthesis;
# otherwise: 2. the node comment;  3. the branch length.

DELIM_REGEX = re.compile(r'[(),;]')

#TODO: allow internal node labels
AFTERLABEL_REGEX = re.compile(r'[:\[]')

LABEL_BRANCH_REGEX = re.compile(r'([^;,()\[\]:]*)(\[.*\]|):?(' + NUM_PATTERN + '|)')




def iter_multinewick(treefile):
    f = stdin if treefile == '-' else bz2.open(treefile, 'rt') if treefile.endswith('.bz2') else open(treefile)
    try:
        for line in f:
            yield line.rstrip()
    finally:
        if treefile != '-':
            f.close()


def decompose_newick(newick):
    """Iterate over control elements of a newick: ( ) , leaf label."""
    position = 0
    previous = ','
    for match in DELIM_REGEX.finditer(newick.rstrip(';') + ';'):
        delim = match.group()
        if previous[0] != ')' and delim != '(':
            # Previous text is a leaf. Fill the current clade (stack[-1])
            clade = AFTERLABEL_REGEX.split(newick[position:match.start()], maxsplit=1)[0]
            #logger.debug('    append %r', clade)
            yield position, clade

        yield position, match.group()

        position = match.end()
        previous = match.group()


def decompose_newick_withdist(newick):
    """Iterate over control elements of a newick: ( ) , leaf label."""
    position = 0
    #previous_pos
    previous = ','
    for match in DELIM_REGEX.finditer(newick.rstrip(';') + ';'):
        delim = match.group()
        start, end = match.span()
        if previous[0] != ')' and delim != '(':
            # Previous text is a leaf. Fill the current clade (stack[-1])
            clade, comment, length = LABEL_BRANCH_REGEX.match(newick[position:start]).groups()
            #FIXME: output internal labels as well
            #if remaining characters after branch length: branch comment
            yield position, clade, comment, length
        if delim == ')':
            label_match = LABEL_BRANCH_REGEX.match(newick[end:])
            clade, comment, length = label_match.groups()
            delim += clade
            end += label_match.end()
        else:
            comment, length = '', ''
        yield start, delim, comment, length

        position = end
        previous = delim
        #if not previous:  # This is old version which matched end of string.
        #    break


#cdef char[4] DELIMS = ['(', ',', ')', ';']

cdef inline parse_label_branch(str element):
    """The element should be strictly limited to the 'label[comment]:branch', 
    because this function does not check for "(" "," or ")".
    """
    clade, comment, length = '', '', ''
    cdef Py_ssize_t end = len(element)

    if end == 0:
        return clade, comment, length
    cdef Py_ssize_t i = 0
    cdef Py_ssize_t prev_i = 0

    # 'current' is the character we check, and 'i' the corresponding index

    while i < end:
        current = element[i]
        if current=='[' or current==':':
            break
        i += 1

    clade = element[0:i]
    #print('    i=%d clade=%r' % (i, clade))

    if i < end and element[i] == '[':
        i += 1
        prev_i = i
        while i < end:
            if element[i] == ']':
                break
            i += 1
        comment = element[prev_i:i]
        if i+1 < end:
            i += 1
            # The only character allowed to follow should be ':' (unless we tolerate multiple consecutive comments)
            if element[i] != ':':
                raise ValueError("Unexpected character #%d %r != ':' following comment in %r" % (i, element[i], element))

    #print('    i=%d comment=%r' % (i, comment))

    if i < end and element[i] == ':':
        i += 1
        prev_i = i
        while i < end:
            for numchar in '0123456789.-eE':
                if element[i] == numchar:
                    break
            else:
                # This is not a valid character for a number, stop parsing.
                break
            i += 1
        length = element[prev_i:i]
    # Check that the next characters are valid:
    #if i < end:
    #    nextchar = element[i]
    #    if nextchar != ',' and nextchar != ')' and nextchar != ';':
    #        raise ValueError("position %d: got %r but expect ',);' after a leaf branch" % (i, nextchar))

    return clade, comment, length


cdef class NewickDecomposer:
    cdef readonly:
        str newick
        Py_ssize_t newicklength
        Py_ssize_t pos
        #char retained = ','
        #char current
    cdef readonly str retained
    cdef bint _emitted_pre_leaf

    def __cinit__(self, str newick):
        self.newick = newick
        self.newicklength = len(newick)
        self.retained = '' # Character being hold until next iteration, where we emit it.
        self.pos = 0  # One character past the retained one.
                      # The position to start on at the next call
        self._emitted_pre_leaf = False
        #print('Initial position=%r' % self.position)

    def __next__(self):
        i, *elements = self.next_elem()
        if i==-1:
            raise StopIteration
        else:
            return i, *elements

    def __iter__(self):
        return self

    cdef next_elem(self):
        clade, comment, length = '', '', ''

        #current = ';'
        cdef Py_ssize_t i = self.pos

        while i < self.newicklength:
            current = self.newick[i]

            if current=='(':
                #TODO: check that retained in '(,' and that i == pos
                if self.retained:
                    #print('Opening: i=%d, position=%d, retained=%s' % (i, self.pos, self.retained))
                    self.pos = i+1
                    emitted = self.retained
                    self.retained = current
                    return i-1, emitted, '', ''
                else:
                    # There is no retained, because we are at the beginning
                    # Since we will anyway increment i, we must emit the *current* character now
                    #print('Beginning open: i=%d, position=%d' % (i, self.pos))
                    self.retained = current
                    if i:
                        # The first opening parenthesis is not the first character. Emit those before:
                        #print('WARNING: characters before first parenthesis: %r' % self.newick[:i])
                        # We can tolerate preceeding comments such as [&U]
                        clade, comment, length = parse_label_branch(self.newick[:i])
                        self.pos = i+1
                        return 0, clade, comment, length
                    else:
                        # Retain it for the next iteration
                        self.pos = 1
            elif current==')' or current==',' or current==';':
                if self.retained == ')':
                    # Internal node. Emit now the *retained* structure:
                    #print('node element: %r' % self.newick[self.pos:i])
                    clade, comment, length = parse_label_branch(self.newick[self.pos:i])
                    clade = ')' + clade
                    prev_pos = self.pos
                    self.pos = i+1
                    self.retained = current
                    return prev_pos-1, clade, comment, length
                # Otherwise, the preceding text is a leaf. We first must emit the retained character, if it wasn't already.
                #elif self.newick[self.position] != self.retained:
                elif not self._emitted_pre_leaf:
                    # We have not emitted it already:
                    if not self.retained:
                        # We are at the beginning
                        #print('Beginning %r: i=%d, pos=%d' % (current, i, self.pos))
                        self.retained = current
                        clade, comment, length = parse_label_branch(self.newick[:i])
                        self.pos = i+1
                        #self._emitted_pre_leaf = True
                        return 0, clade, comment, length

                    #print('pre-leaf element, pos=%d, i=%d' % (self.pos, i))
                    self._emitted_pre_leaf = True
                    return self.pos-1, self.retained, '', ''
                else:
                    # Emit the label now.
                    if self.pos!=i:
                        #print('leaf element: %r' % self.newick[self.pos:i])
                        clade, comment, length = parse_label_branch(self.newick[self.pos:i])
                    #else:
                        # !! Abnormal case: zero width leaf label !!
                        #print('Warning: EMPTY leaf element before %d' % self.pos)
                    prev_pos = self.pos
                    self.pos = i+1
                    self.retained = current
                    self._emitted_pre_leaf = False
                    return prev_pos, clade, comment, length

            i += 1
            # NOTE: this continues parsing after the semi-colon (bug or feature?)

        # The last element should normally be a semi-colon.
        if i == self.newicklength:
            if self.retained != ';':
                # Implicitly terminated newick
                clade, comment, length = parse_label_branch(self.newick[self.pos:self.newicklength])
                prev_pos = self.pos
                if self.retained == ')':
                    clade = ')' + clade
                    self.pos = self.newicklength  # to emit the semicolon afterwards.
                    self.retained = ';'
                    return prev_pos, clade, comment, length
                #elif self.retained != '(': # This should be invalid
                # leaf outside of any parentheses, this is weird but ok.
                elif not self._emitted_pre_leaf:
                    # We have not emitted retained yet
                    self._emitted_pre_leaf = True
                    return self.pos-1, self.retained, '', ''
                else:
                    clade, comment, length = parse_label_branch(self.newick[self.pos:self.newicklength])
                    prev_pos = self.pos
                    self.pos = self.newicklength
                    self.retained = ';'
                    return prev_pos, clade, comment, length
            else:
                # Return the terminating semicolon.
                # retained position should be newicklength - 1, unless there are extra characters...
                prev_pos = self.pos
                self.pos = self.newicklength+1
                return prev_pos-1, self.retained, '', ''

        return -1, '', '', ''


def rewrite_indexed_newick(newick, ids):
    new = ''
    for position, elem in decompose_newick_withdist(newick):
        if elem[0] in '(,);':
            new += elem
        else:
            new += str(ids[elem[0]]) + elem[1]
            try:
                new += ':' + elem[2]  # Branch length (not None)
            except TypeError:
                pass
    return new


class IterNewickClusters:
    """Parse a newick string (parenthesized hierarchical groups) and
    return the list of clusters."""
    def __init__(self, newick, ids=None):
        self.newick = newick
        #logger.debug('newick: %d characters', len(newick))
        if ids is None:
            def get_id(leafname): return leafname
            self.get_id = get_id
        else:
            self.get_id = ids.__getitem__

    def __iter__(self):
        self.stack = deque()  # cluster partitioning at each depth level (list of subclusters). Temporary.
        self.stack.append([])
        self.sublevel1 = []  # temporary: store the subclusters of level 1; needed to handle multiple root situations.
        #self.decomposing = decompose_newick_withdist(self.newick)
        self.decomposing = NewickDecomposer(self.newick)
        self.last_level = iter(())
        return self

    def __next__(self):
        stack, sublevel1 = self.stack, self.sublevel1
        try:
            position, element, _, dist = next(self.decomposing)
            #logger.debug('Depth %d; match %r; current clade: %s parts.', len(stack)-1, element, len(stack[-1]))
        except StopIteration:
            try:
                last_level_clust = next(self.last_level)
            except StopIteration as err:
                err.args = ("IterNewickClusters: end of root level clusters",)
                raise
            #logger.debug('    last clust (%s)', ','.join(map(str, last_level_clust)))
            return last_level_clust

        if element[0] == ')':
            # Save and close the current level
            # Flatten the inner clades into a single parent clade
            subclustdist = stack.pop()
            clust = tuple(chain(*(item[0] for item in subclustdist)))
            #clust = tuple(chain(*subclust))
            try:
                stack[-1].append((clust, dist or None))
            except IndexError:
                raise ValueError("Unmatched closing parenthesis at position %d" % position)
            if len(stack) == 1:
                # This avoids emitting the full cluster, when there is one single root cluster.
                # Handling the last informative level (0 or 1) is deferred to the end.
                # This level is zero if there are no englobing parentheses.
                sublevel1.append(subclustdist)
            elif len(clust) > 1:
                #logger.debug('    emit clust (%s); remaining levels: %d', ','.join(map(str, clust)), len(stack))
                return clust, subclustdist
        elif element == ';':
            root_level = stack.pop()
            self.root_clust = tuple(chain(*(item[0] for item in root_level)))
            if stack:
                raise ValueError('Missing %d closing parentheses!' % len(stack))
            if len(root_level) > 1:
                self.last_level = zip((item[0] for item in root_level), sublevel1)
                self.root_level = root_level
                self.has_full_clust = False
                #logger.debug('    root clust (%s)', ','.join(map(str, self.root_clust)))
            else:
                self.root_level = sublevel1[0]
                self.has_full_clust = True
                #logger.debug('    single root clust (%s)', ','.join(map(str, self.root_clust)))
                raise StopIteration("IterNewickClusters: end of clusters")
        elif element == '(':  # Open a new nested level
            stack.append([])
        elif element != ',':
            # It's a leaf
            stack[-1].append( ((self.get_id(element),), dist or None) )

        if len(stack) > _MAX_DEPTH:
            raise ValueError("Nested hierarchy above %d levels is not allowed" % _MAX_DEPTH)

        # If nothing was returned, we must continue the iteration
        return self.__next__()

    @property
    def root(self):
        """Return the last clusters, which is the full set of members."""
        try:
            return self.root_clust, self.root_level
        except AttributeError:
            list(self) # process the newick string first.
            return self.root_clust, self.root_level


class IterNewickClustersPartial(IterNewickClusters):
    """Discard some leaf elements on the fly, so only yield each reduced
    cluster once."""
    def __init__(self, newick, drop_ids, ids=None):
        super().__init__(newick, ids)
        self.drop_ids = drop_ids

    def __iter__(self):
        self.stack = deque()  # cluster partitioning at each depth level (list of subclusters). Temporary.
        self.stack.append([])
        self.decomposing = NewickDecomposer(self.newick)

        self.level_ready = deque()  # Only for informative subdivision (after removal of dropped ids)
        self.tmp_clust_dist = {}
        self.sublevels = {}

        return self

    def __next__(self):
        stack, level_ready, sublevels, tmp_clust_dist = self.stack, self.level_ready, self.sublevels, self.tmp_clust_dist
        # First, empty any informative subdivision:
        try:
            clust = level_ready.popleft()
            #logger.debug('-> cluster ready %r', clust)
            if len(clust) > 1:
                subclusters = sublevels.pop(clust)
                subclustdist = [(subclust, tmp_clust_dist.pop(subclust)) for subclust in subclusters]
                return clust, subclustdist
            else:
                return self.__next__()
        except IndexError:
            pass  # No cluster is ready, we must process the newick

        try:
            position, element, _, dist = next(self.decomposing)
        except StopIteration:
            # Check that we terminated normally:
            assert hasattr(self, 'root_clust')
            raise StopIteration('Newick fully decomposed.')

        #logger.debug('Depth %d; match %r; current clade: %s parts.', len(stack)-1, element, len(stack[-1]))

        if element[0] in (')', ';'):
            # Close current level
            # Flatten the inner clades into a single parent clade
            subclusters = stack.pop()
            if not stack and element != ';':
                raise ValueError("Unmatched closing parenthesis at position %d" % position)

            non_empty = [subclust for subclust in subclusters if subclust]

            if non_empty:
                clust = tuple(chain(*non_empty))
                dist = float(dist) if dist else None
                #logger.debug('clust of size %d, non-empty subclusters: %s', len(clust), non_empty)

                if len(non_empty) > 1:
                    level_ready.extend(non_empty)
                    tmp_clust_dist[clust] = dist
                    sublevels[clust] = non_empty  # if only one non_empty, we should not update this data.
                else:
                    # only one non empty subcluster.
                    # This subsplit is not retained, but its edge length is added.
                    try:
                        tmp_clust_dist[clust] += dist  # subclust == clust
                    except TypeError:
                        if tmp_clust_dist[clust] is None:
                            tmp_clust_dist[clust] = dist

                if element == ';':
                    if stack:
                        raise ValueError('Missing %d closing parentheses!' % len(stack))
                    self.root_clust = clust
                    self.root_level = [(subclust, tmp_clust_dist[subclust]) for subclust in sublevels[clust]]
                    if len(non_empty) > 1:
                        self.has_full_clust = False  # NOTE: the meaning differs from the one in the base class:
                                                     # It tells whether the first level is informative (= is a split),
                                                     # given dropped ids.
                        #logger.debug('    root clust (%s)', ','.join(map(str, self.root_clust)))
                    else:
                        self.has_full_clust = True
                        #logger.debug('    single root clust (%s)', ','.join(map(str, self.root_clust)))
                else:
                    stack[-1].append(clust)
            else:
                #logger.debug('no subclusters!')
                if element == ';':
                    self.root_clust = ()
                    self.root_level = []
        elif element == '(':  # Open a new nested level
            stack.append([])
        elif element != ',':
            # It's a leaf
            leaf_id = self.get_id(element)
            if leaf_id not in self.drop_ids:
                stack[-1].append( (leaf_id,) )
                tmp_clust_dist[(leaf_id,)] = float(dist) if dist else None

        if len(stack) > _MAX_DEPTH:
            raise ValueError("Nested hierarchy above %d levels is not allowed" % _MAX_DEPTH)

        # If nothing was returned, we must continue the iteration
        #FIXME: this might exceed Python recursion limit. Increase with sys.setrecursionlimit.
        return self.__next__()
