#!/usr/bin/env bash

# This program is licensed under the Unlicense (<http://unlicense.org/>)


help="Batch convert PhyloBayes trace files to a Tracer compatible format.

Automatic file naming:

  filename.trace -> filename.tracer
"

set -euo pipefail

[ "$#" -gt 0 ] || { echo "$help" >&2 ; exit 2; }

for pbtrace in $@; do
    outfile="${pbtrace%.trace}.tracer"
    # File content checking: first row should have '#cycle\t#treegen\ttime'
    if [ -s "$pbtrace" ] && sed -nr '/^#cycle\t#treegen\ttime/q0;1q1' "$pbtrace"; then
        # Remove columns 2 and 3, and the starting hash.
        sed -r 's/^(\S+\t)(\S+\t){2}/\1/; 1s/^#//' "$pbtrace" > "$outfile"
    else
        echo "INVALID trace file header: $pbtrace" >&2
    fi
done

