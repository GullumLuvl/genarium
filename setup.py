from setuptools import setup, find_packages, Extension

try:
    from Cython.Build import cythonize
    extensions = cythonize('myriphy/newick.pyx')
except ImportError:
    # Use the precompiled C file.
    extensions = [Extension('myriphy/newick', ['myriphy/newick.c'])]


setup(name='Genarium',
      version='0.0.0a0',
      description='Algorithms for phylogenetic gene tree amalgamation and reconciliation',
      classifiers=[
          'Development Status :: 3 - Alpha',
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
          'Programming Language :: Python :: 3',
          'Programming Language :: Python :: 3.6',
          'Programming Language :: Python :: 3.7',
          'Programming Language :: Python :: 3.8',
          'Programming Language :: Python :: 3.9',
          'Programming Language :: Python :: 3.10',
          'Environment :: Console',
          'Intended Audience :: Science/Research',
          'Topic :: Utilities',
          'Topic :: Scientific/Engineering :: Bio-Informatics',
      ],
      url='https://gitlab.com/GullumLuvl/Genarium',
      author='Guillaume Louvel',
      author_email='guillaume.louvel'+('@')+'normalesup.org',
      license='GPLv3',
      packages=['myriphy', 'grec'],
      ext_modules=extensions,
      install_requires=[
          'numpy',
          'ete3',
      ],
      #extras_require={},
      #tests_require=['pytest'],  # setuptools test is deprecated.
      entry_points = {
          'console_scripts': [
              'seq2clust=grec.seq2clust:main',
              # MCMC output tools
              'CCP=myriphy.CCP:main',
              'BPdiff=myriphy.CCP:main_bpcomp',
              'MCMCequaltails=myriphy.MCMC:main_equalized_chains',
              'MRtree=myriphy.CCP:main_MREtree',
              'polysplits=myriphy.CCP:main_polysplits',
              'internode_certainty=myriphy.CCP:main_TC',
              # Reconciliation
              'run_amalgam_rec=grec.run_amalgam_rec:main'
          ]
      },
      scripts=[
              'myriphy/summarytree.R',
              'myriphy/MCMCdiagnostics.R',
              'myriphy/pb2tracer.sh'
      ],
      include_package_data=True,
      zip_safe=False)
